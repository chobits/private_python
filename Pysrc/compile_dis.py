#!/usr/bin/python
import dis
import sys

def dis_consts(co):
    codes = []
    if co.co_consts:
        print ' [consts]'
        for (i, c) in enumerate(co.co_consts):
            print '  (%d) %s' %(i, repr(c))
            if type(c) == type(co):
                codes.append(c)
    return codes

def dis_names(co):
    if not co.co_names: return
    print ' [names]'
    for (i, name) in enumerate(co.co_names):
        print '  (%d) %s' %(i, repr(name))

def dis_local(co):
    codes = []
    if co.co_varnames:
        print ' [locals]'
        for (i, name) in enumerate(co.co_varnames):
            print '  (%d) %s' %(i, repr(name))
            if type(name) == type(co):
                codes.append(c)
    return codes

def dis_code(co):
    print '____ %s ____' %co.co_name
    codes = dis_consts(co)
    dis_names(co)
    codes.extend(dis_local(co))
    print ' [<disassembled code>]'
    dis.dis(co)
    print
    # recursion child code
    for c in codes:
        dis_code(c)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        file = 'a.py'
    elif len(sys.argv) == 2:
        file = sys.argv[1]
    else:
        print 'Usage: %s [pysrc]' %sys.argv[0]

    pysrc = open(file).read()
    co = compile(pysrc, file, 'exec')
    dis_code(co)

