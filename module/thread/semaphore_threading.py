#!/usr/bin/python
import threading
import time

sem = threading.Semaphore(5)

class MyThread(threading.Thread):
    def run(self):
        for i in range(0, 10):
            print '  %s %d: wait resource' %(self.getName(), i)
            sem.acquire()
            print '+ %s %d: get  resource' %(self.getName(), i)
            time.sleep(0.1)
            print '- %s %d: release resource' %(self.getName(), i)
            sem.release()
        print '\nX %s end\n' %self.getName()

for i in range(0, 10):
    sub = MyThread()
    sub.setName('[%d]' %i)
    sub.start()

while True:
    pass
