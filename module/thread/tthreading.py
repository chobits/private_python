#!/usr/bin/python
import threading
import time

def all_threads(all_list = threading.enumerate()):
    return '\n'.join([ repr(thread) for thread in all_list ])

def active_threads():
    threading._active_limbo_lock.acquire()
    active = threading._active.values()
    threading._active_limbo_lock.release()
    return active

def limbo_threads():
    threading._active_limbo_lock.acquire()
    limbo = threading._limbo.values()
    threading._active_limbo_lock.release()
    return limbo

class MyThread(threading.Thread):
    def run(self):
        while True:
            print 'I\'m', threading._get_ident()
            time.sleep(1)

mythread = MyThread()
print 'Mythread():\n<active %s>\n<limbo %s>' %(active_threads(), limbo_threads())
mythread.start()
print 'mythread.start():\n<active %s>\n<limbo %s>' %(active_threads(), limbo_threads())
while True:
    print 'main thread:', threading._get_ident()
    print 'all threads:\n', all_threads()
    time.sleep(1)

