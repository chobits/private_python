#!/usr/bin/python

import sys
import time
import threading

event = threading.Event()

class MyThread(threading.Thread):
    def run(self):
        while not event.isSet():
            print ' sub: waiting event to be set'
            event.wait()
        print ' sub: event is set!'

sub = MyThread()
sub.start()

time.sleep(1)
print 'main set event'
event.set()
time.sleep(1)

sys.exit(0)
