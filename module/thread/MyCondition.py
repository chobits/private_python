#!/usr/bin/python
# simple implemention to threading.Condition()
# NOTE: This condition is lockless _itself_, different from threading.Condition.
import thread
import sys

class MyCondition:
    def __init__(self):
        self.lock = thread.allocate_lock()
        self.notified = 0
        self.__owner = None
    def wait(self):
        if self.__owner:
            raise RuntimeError("cannot wait on other's condition")
        self.lock.acquire()
        self.__owner = thread.get_ident()
        # block
        while not self.notified:
            self.lock.acquire()
        self.notified = 0
        self.__owner = None
        self.lock.release()
    def wake(self):
        if not self.__owner:
            raise RuntimeError("cannot wake up masterless condition")
        if not self.notified:
            self.notified = 1
            self.lock.release()

class MyConditions:
    def __init__(self):
        self.queue = []
    def wakeone(self):  
        if self.queue:
            c = self.queue.pop()
            c.wake()
    def wakeall(self):  
        for c in self.queue:
            c.wake()
        self.queue = []
    def wait(self):
        try:
            if thread.get_ident() in [w.__owner for w in self.queue]:
                raise RuntimeError("wait again(Fuck! Should this happen?)")
        except AttributeError:
            pass
        w = MyCondition()
        self.queue.append(w)
        w.wait()
        del w

# test
def TestMyCondition():
    cond = MyCondition()
    def thread_proc():
        for i in range(0, 5):
            print '%d: wait' %i
            cond.wait()
            print '%d: I am awake' %i
    thread.start_new_thread(thread_proc, ())
    return cond

def TestMyConditions(n = 4):
    conds = MyConditions()
    def thread_proc(name):
        for i in range(0, 5):
            print '%s %d: wait' %(name, i)
            conds.wait()
            print '%s %d: I am awake' %(name, i)
        print '%s exit' %name
    while n > 0:
        thread.start_new_thread(thread_proc, ('[%d]' %n, ))
        n -= 1
    return conds

