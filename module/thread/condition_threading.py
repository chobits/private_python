#!/usr/bin/python
# see http://docs.python.org/library/threading.html#condition-objects
# Condition gives us 'notify' and 'wait' method!
import sys
import time
import threading

count = 0
condition = threading.Condition()

class MyThread(threading.Thread):
    def run(self):
        condition.acquire()
        while count < 5:
            print ' sub: count is %d, waiting' %count
            condition.wait()
        print ' sub: count is %d\n sub: awake!!!!!!!' %count
        condition.release()

sub = MyThread()
sub.start()

def main_notify():
    global count
    condition.acquire()
    count += 1
    print 'main: notify %d time(s)' %count
    condition.notify()
    condition.release()
    time.sleep(0.1)

main_notify()
main_notify()
main_notify()
main_notify()
main_notify()
main_notify()

sys.exit(0)
