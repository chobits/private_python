#!/usr/bin/python
import thread
import sys
import time

count = 0
lock = thread.allocate_lock()

def thread_proc(name):
    global count
    while True:
        lock.acquire()
        print '%s: %d' %(name, count)
        count += 1
        lock.release()

sys.setcheckinterval(1)
print 'interval tick: ', sys.getcheckinterval()
thread.start_new_thread(thread_proc, ('sub0',))
thread.start_new_thread(thread_proc, ('sub1',))
while True:
    time.sleep(1)
