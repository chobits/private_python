#!/usr/bin/python
# simple implemention to threading.Condition()
# NOTE: This condition is lockless _itself_, different from threading.Condition.
import sys
import time
import thread
import MyCondition

class MySemaphore:
    def __init__(self, value = 1):
        if value < 0:
            raise RuntimeError('inited %d semaphore vaule is invalid' %value)
        self.lock = thread.allocate_lock()
        self.conds = MyCondition.MyConditions()
        self.value = value
        self.owners = []
    def acquire(self):
        self.lock.acquire()
        while self.value <= 0:
            self.lock.release()
            self.conds.wait()
            # When get lock again, I should check whether there is resource!
            # Maybe the waker can get the resource again ahead of me!
            self.lock.acquire()
        # allow double semaphore.acquire()
        self.owners.append(thread.get_ident())
        self.value -= 1
        self.lock.release()
    def release(self):
        self.lock.acquire()
        if thread.get_ident() in self.owners:
            self.owners.remove(thread.get_ident())
            self.value += 1
            self.conds.wakeone()
        self.lock.release()

# test
def TestMySemaphore():
    sem = MySemaphore(5)
    def thread_proc(name):
        for i in range(0, 10):
            print '  %s %d: wait resource' %(name, i)
            sem.acquire()
            print '+ %s %d: get  resource' %(name, i)
            time.sleep(0.1)
            print '- %s %d: release resource' %(name, i)
            sem.release()
        print '\nX %s end\n' %name
    for i in range(0, 10):
        thread.start_new_thread(thread_proc, (' [%d]'%i,))

TestMySemaphore()
while True:
    pass
