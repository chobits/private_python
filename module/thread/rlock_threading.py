#!/usr/bin/python
import threading
import sys

rlock = threading.RLock()

class MyThread(threading.Thread):
    def run(self):
        print 'sub: get rlock'
        rlock.acquire()
        print 'sub: get rlock'
        rlock.acquire()
        print 'sub: double release rlock'
        rlock.release()
        rlock.release()
        while True:
            pass

sub = MyThread()
sub.start()
print 'main: get rlock'
rlock.acquire()
print 'main: got'
sys.exit(0)
