#!/usr/bin/python
import random

disk_capacity = 100
mem_capacity = 10

def fifo_replace(times = 1000):
    hit_times = 0
    queue = []
    for i in range(times):
        need_page = random.randrange(disk_capacity)
        if need_page not in queue:
            if len(queue) == mem_capacity:
                queue.pop()
            queue.insert(0, need_page)
        else:
            hit_times += 1
    return hit_times, times

def lru_replace(times = 1000):
    hit_times = 0
    queue = []
    need_page = random.randrange(disk_capacity)
    for i in range(times):
        #need_page = random.randrange(disk_capacity)
        need_page += random.randrange(mem_capacity) - mem_capacity/2
        if need_page in queue:
            index = queue.index(need_page)
            queue[0], queue[index] = queue[index], queue[0]
            hit_times += 1
        else:
            if len(queue) == mem_capacity:
                queue.pop()
            queue.insert(0, need_page)
    return hit_times, times
            
    
hits, total = fifo_replace()
print 'fifo hit rate: %d / %d' %(hits, total)
hits, total = lru_replace()
print 'lru hit rate: %d / %d' %(hits, total)
