import re
# 1)
pattern = re.compile('(http://)(.*)(/)(.*)')

def repl(m):
    return m.group(3) + m.group(4)

def url_absolute2relative(url):
    return pattern.sub(repl, url)

url = 'http://share.baidu.com/getsharenum?urls=4828071523070171226,12227450673634322083&callback=bds.se.like.giveData'
print url_absolute2relative(url)
print url_absolute2relative('http://www.baidu.com/')
print url_absolute2relative('http://www.baidu.com')     # cannot work

# 2)

# 'http://xx/yy' -> ''
def get_relative_url(url):
    pattern = re.compile('(http://)([^/]*)(.*)')
    m = pattern.match(url)
    return m.group(3) if m.group(3) else '/'

print get_relative_url('http://www.baidu.com')                # can work well
print get_relative_url('http://www.baidu.com/')
print get_relative_url(url)
