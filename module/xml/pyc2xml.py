#!/usr/bin/python
import sys
from struct import unpack

class RFILE:
    def __init__(self, name):
        self.strings = []
        self.file = open(name, 'r')
        self.magic = self.long()
        self.time = self.long()
        self.depth = 0

    def byte(self):
        return self.file.read(1)

    def long(self):
        return unpack('@i', self.file.read(4))[0]

    def long64(self):
        return unpack('@i', self.file.read(8))[0]

    def bytes(self, n):
        return self.file.read(n)

class pycode:
    pass


TYPE_NULL             = '0'
TYPE_NONE             = 'N'
TYPE_FALSE            = 'F'
TYPE_TRUE             = 'T'
TYPE_STOPITER         = 'S'
TYPE_ELLIPSIS         = '.'
TYPE_INT              = 'i'
TYPE_INT64            = 'I'
TYPE_FLOAT            = 'f'
TYPE_BINARY_FLOAT     = 'g'
TYPE_COMPLEX          = 'x'
TYPE_BINARY_COMPLEX   = 'y'
TYPE_LONG             = 'l'
TYPE_STRING           = 's'
TYPE_INTERNED         = 't'
TYPE_STRINGREF        = 'R'
TYPE_TUPLE            = '('
TYPE_LIST             = '['
TYPE_DICT             = '{'
TYPE_CODE             = 'c'
TYPE_UNICODE          = 'u'
TYPE_UNKNOWN          = '?'
TYPE_SET              = '<'
TYPE_FROZENSET        = '>'

def read_code_object(rf):
    code = pycode()
    code.argcount = rf.long()
    code.nlocals = rf.long()
    code.stacksize = rf.long()
    code.flags = rf.long()
    code.code = read_object(rf)
    code.consts = read_object(rf)
    code.names = read_object(rf)
    code.varnames = read_object(rf)
    code.freevars = read_object(rf)
    code.cellvars = read_object(rf)
    code.filename = read_object(rf)
    code.name = read_object(rf)
    code.firstlineno = rf.long()
    code.lnotab = read_object(rf)
    return code

def read_object(rf):
    ret = 'NORET'
    otype = rf.byte()
    if otype == '':
        return
    rf.depth += 1
    if otype == TYPE_NULL:
        ret = '__NULL__'
    elif otype == TYPE_NONE:
        ret = None
    elif otype == TYPE_FALSE:
        ret = False
    elif otype == TYPE_TRUE:
        ret = True
    elif otype == TYPE_STOPITER:
        pass
    elif otype == TYPE_ELLIPSIS:
        pass
    elif otype == TYPE_INT:
        ret = rf.long()
    elif otype == TYPE_INT64:
        ret = rf.long64()
    elif otype == TYPE_FLOAT:
        pass
    elif otype == TYPE_BINARY_FLOAT:
        pass
    elif otype == TYPE_COMPLEX:
        pass
    elif otype == TYPE_BINARY_COMPLEX:
        pass
    elif otype == TYPE_LONG:
        pass
    elif otype == TYPE_STRING or otype == TYPE_INTERNED:
        ret = rf.bytes(rf.long())
        if otype == TYPE_INTERNED:
            rf.strings.append(ret)
    elif otype == TYPE_STRINGREF:
        ret = rf.strings[rf.long()]
    elif otype == TYPE_TUPLE or otype == TYPE_LIST:
        ret = [read_object(rf) for i in range(rf.long())]
        if otype == TYPE_TUPLE: ret = tuple(ret)
    elif otype == TYPE_DICT:
        d = {}
        while True:
            key = read_object(rf)
            if key == '__NULL__': break
            val = read_object(rf)
            if val != '__NULL__': d[key] = val
        ret = d
    elif otype == TYPE_CODE:
        ret = read_code_object(rf)
    elif otype == TYPE_UNICODE:
        pass
    elif otype == TYPE_UNKNOWN:
        pass
    elif otype == TYPE_SET:
        pass
    elif otype == TYPE_FROZENSET:
        pass
    if ret == 'NORET':
        raise ValueError('unknown type %s %d' %(repr(otype), rf.file.tell()))
    rf.depth -= 1
    return ret

def pyc2code(fpyc):
    rf = RFILE(fpyc)
    return read_object(rf)

def create_element(dom, tagname, text):
    if type(text) != str:
        text = repr(text)
    elem = dom.createElement(tagname)
    tnode = dom.createTextNode(text)
    elem.appendChild(tnode)
    return elem

def create_tag(dom, tagname):
    if type(tagname) != str:
        tagname = repr(tagname)
    return dom.createElement(tagname)

def create_iter_element(dom, itertype, iterable):
    elem = create_tag(dom, itertype)
    for obj in iterable:
        write_object_to_dom(dom, elem, obj)
    return elem

def create_code_element(dom, code):
    def create_code_field(dom, pycelem, code, field):
        elem = create_tag(dom, field)
        fieldobj = getattr(code, field)
        if field == 'code':
            fieldobj = repr(fieldobj)
        elif field == 'name':
            fieldobj = fieldobj.replace('>', '')
            fieldobj = fieldobj.replace('<', '')
        elif field == 'lnotab':
            fieldobj = repr(fieldobj)
        write_object_to_dom(dom, elem, fieldobj)
        pycelem.appendChild(elem)

    elem = create_tag(dom, 'code')
    create_code_field(dom, elem, code, 'argcount')
    create_code_field(dom, elem, code, 'nlocals')
    create_code_field(dom, elem, code, 'stacksize')
    create_code_field(dom, elem, code, 'flags')
    create_code_field(dom, elem, code, 'code')
    create_code_field(dom, elem, code, 'consts')
    create_code_field(dom, elem, code, 'names')
    create_code_field(dom, elem, code, 'varnames')
    create_code_field(dom, elem, code, 'freevars')
    create_code_field(dom, elem, code, 'cellvars')
    create_code_field(dom, elem, code, 'filename')
    create_code_field(dom, elem, code, 'name')
    create_code_field(dom, elem, code, 'firstlineno')
    create_code_field(dom, elem, code, 'lnotab')
    return elem

def write_object_to_dom(dom, root, obj):
    if isinstance(obj, pycode):
        elem = create_code_element(dom, obj)
    elif type(obj) == list:
        elem = create_iter_element(dom, 'list', obj)
    elif type(obj) == tuple:
        elem = create_iter_element(dom, 'tuple', obj)
    elif type(obj) == str:
        elem = create_element(dom, 'string', obj)
    elif type(obj) == int:
        elem = create_element(dom, 'int', obj)
    elif type(obj) == long:
        elem = create_element(dom, 'long', obj)
    elif obj in [None, True, False]:
        elem = create_tag(dom, obj)
    else:
        raise ValueError('unknown object %s' %repr(obj))
    root.appendChild(elem)

def xmldom(name):
    import xml.dom.minidom
    dom = xml.dom.minidom.getDOMImplementation().createDocument(None, name, None)
    return dom

def code2dom(code):
    dom = xmldom(code.filename)
    root = dom.documentElement
    write_object_to_dom(dom, root, code)
    return dom

def savexml(dom, name):
    f = open(name, 'w')
    dom.writexml(f, '\n', ' ', '', 'UTF-8')
    f.close()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: %s pyc' %sys.argv[0]
        sys.exit(-1)
    code = pyc2code(sys.argv[1])
    dom = code2dom(code)
    savexml(dom, sys.argv[1] + '.xml')

