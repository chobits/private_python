# -*- encoding=utf-8 -*-
from xml.dom import minidom

#得到dom对象，test为根元素
imp = minidom.getDOMImplementation()
dom = imp.createDocument(None,'pyc',None)

#转化为element实例
root = dom.documentElement

#dom.version = '1.0'
#dom.encoding = 'UTF-8'

#创建一个元素并增加到根元素中
elem = dom.createElement('tagName')
text = dom.createTextNode('textdate')
comment = dom.createComment('comment')
elem.appendChild(text)
root.appendChild(elem)
root.appendChild(comment)

#在创建一个元素
elem1 = dom.createElement('test2')
elem1.appendChild(text)
elem.appendChild(elem1)

#读写文件的句柄
fileHandle = open('123.xml','w')
#写入操作，第二个参数为缩进（加在每行结束后），第三个为增量缩进（加在每行开始前并增量）
dom.writexml(fileHandle, '\n', ' ', '','UTF-8')

#fileHandle.write('\n')
#fileHandle.write(elem.toprettyxml())
fileHandle.close()
