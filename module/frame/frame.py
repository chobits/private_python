#!/usr/bin/python
import sys

global_variable = 1

def dump_type(object):
   print ', '.join(dir(object))

def dump_frame():
    # current thread -> frame -> f_back * 1
    frame = sys._getframe(1)
    n = 0
    print "global vars: %s" %', '.join(frame.f_globals.keys())
    while frame != None:
        print "[%d] " %n, 
        print "%s: " %frame.f_code.co_name,
        # output local vars
        print '\n\t',
        print '\n\t'.join([ i+'\t=\t'+repr(frame.f_locals[i]) for i in frame.f_locals.keys()])
        frame = frame.f_back
        n = n + 1

## test script ##
def aaa():
    local_aaa = 0
    local_aaa_2 = 1
    dump_frame()
def aa():
    local_aa = 0
    aaa()
def a():
    local_a = 0
    local_a = local_a + 1
    aa()

if __name__ == '__main__':
    a()
