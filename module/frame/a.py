#!/usr/bin/python

global_var = 0

def func1():
  print global_var

def func2():
  global global_var
  global_var = global_var + 1


if __name__ == '__main__':
  func2()
  func1()
