#!/usr/bin/python
import gc
import sys

gc.set_debug(gc.DEBUG_STATS | gc.DEBUG_LEAK | gc.DEBUG_OBJECTS)
L = []
print 'L = []: ', sys.getrefcount(L)        # real: 1
L.append(L)
print 'L.append(L): ', sys.getrefcount(L)   # real: 2
del L
gc.collect()
