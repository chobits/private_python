#!/usr/bin/python
import sys
import gc

class A(object):
    def __del__(self):
        pass
#gc.set_debug(gc.DEBUG_STATS | gc.DEBUG_LEAK | gc.DEBUG_UNCOLLECTABLE)

a = A()
a.myself = a

print '    a ref:', sys.getrefcount(a) 
del a

gc.set_debug(gc.DEBUG_SAVEALL)
gc.collect()
print gc.garbage
