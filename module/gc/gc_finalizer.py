#!/usr/bin/python
import gc

class A(object):
    def __del__(self):
        pass
class B(object):
    def __del__(self):
        pass

gc.set_debug(gc.DEBUG_STATS | gc.DEBUG_LEAK)

a = A()
b = B()

a.b = b
b.a = a

del a
del b

gc.collect()
