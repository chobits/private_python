#!/usr/bin/python
#Filename: netenv.py
import sys, socket

def getipaddrs(hostname):
	result = socket.getaddrinfo(hostname, None, 0, socket.SOCK_STREAM)
	return [ip[4][0] for ip in result]

hostname = socket.gethostname()
print 'Host name:', hostname
print 'Fully-qualified name:', socket.getfqdn(hostname)
try:
	print 'IP addresses:'
	for ip in getipaddrs(hostname):
		print ip
except socket.herror, e:
	print 'couldn\'t get ip addresses:', e
	
