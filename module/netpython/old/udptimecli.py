#!/usr/bin/python
#Filename: udptime.py
import socket, sys, struct, time

port = 51234
host = 'localhost'

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.sendto('', (host, port))

buf = s.recvfrom(2048)[0]
if len(buf) != 4:
	print 'wrong size:%s'%buf
	sys.exit(1)

# '!' stands for network-bit-order  
# 'I' stands for 'int'
secs = struct.unpack('!I', buf)[0]
print '[cli]', time.ctime(int(secs))
