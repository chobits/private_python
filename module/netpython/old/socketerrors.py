#!/usr/bin/python
#Filename:socketerrors.py
import sys, socket

host = sys.argv[1]
port = sys.argv[2]
file = sys.argv[3]

try:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error, e:
	print 'socket err: %s'%e
	sys.exit(1)

try:
	port = int(port)
except ValueError:
	try:
		port = socket.getservbyname(port)
	except socket.error, e:
		print 'no name: %s'%e
		sys.exit(1)

try:
	s.connect((host, port))
except socket.error, e:
	print 'cannot connect %s'%e
	sys.exit(1)
except socket.gaierror, e:
	print 'gaierror:%s'%e
	sys.exit(1)

try:
	s.sendall('GET %s HTTP/1.0\r\n\r\n'%file)
except socket.error, e:
	print 'senderr:%s'%e
	sys.exit(1)

while True:
	try:
		line = s.recv(1024)
	except socket.error, e:
		print 'recverr: %s'%e
		sys.exit(1)
	if not len(line):
		break
	sys.stdout.write(line)


s.close()
	
