#!/usr/bin/python
#Filename: serv2.py
import socket, traceback

host =''
port = 51234
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((host, port))

while 1:
	print '[serv]waiting message'
	try:
		message, cliaddr = s.recvfrom(1024)
		s.sendto(message, cliaddr)		
	except (KeyboardInterrupt, SystemExit):
		raise
	except:
		traceback.print_exc()
	
