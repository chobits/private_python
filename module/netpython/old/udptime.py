#!/usr/bin/python
#Filename: udptime.py
import socket, sys, struct, time

hostname = 'time.nist.gov'
port = 37
host = socket.gethostbyname(hostname)
print 'host:%s'%host

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.sendto(' ', (host, port))

buf = s.recvfrom(2048)[0]
if len(buf) != 4:
	print 'wrong size:%s'%buf
	sys.exit(1)

secs = struct.unpack('!I', buf)[0]
secs -= 220898880
print time.ctime(int(secs))
