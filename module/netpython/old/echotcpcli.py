#!/usr/bin/python
#Filename: echotcpcli.py
import socket, sys
if len(sys.argv) != 3:
	print '<suage> %s IP PORT'%sys.argv[0]
	sys.exit(1)

host = sys.argv[1]
port = sys.argv[2]

try:
	port = int(port)
except:
	port = getservbyname(port)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((host, port))

data = 'x' * 10485760
bytewrittern = 0
while bytewrittern < len(data):
	start = bytewrittern
	end = min(bytewrittern + 1024, len(data))
	bytewrittern += s.send(data[start:end])
	sys.stdout.write("[cli]writtern %d"%bytewrittern)
	sys.stdout.flush()

s.shutdown(1)
print "ALL DATA WRITTERN"
	
while 1:
	line = s.recv(1024)
	if not len(line):
		break
	print line,
