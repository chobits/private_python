#!/usr/bin/python
#Filename: udptimeserv.py
import socket, sys, struct, time

port = 51234
host = ''

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((host, port))

while 1:
	message, cliaddr = s.recvfrom(8192)
	secs = int(time.time())
	reply = struct.pack('!I', secs)
	s.sendto(reply, cliaddr)
