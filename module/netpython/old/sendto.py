#!/usr/bin/python
#Filename: sendto.py
import socket, sys
if len(sys.argv) != 3:
	print '<suage> %s IP PORT'%sys.argv[0]
	sys.exit(1)

host = sys.argv[1]
port = sys.argv[2]

try:
	port = int(port)
except:
	port = getservbyname(port)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect((host, port))

while 1:
	try:
		message = raw_input()
	except KeyboardInterrupt:
		print 'quit'
		sys.exit(1)
	s.sendto(message, (host, port))
	echo = s.recvfrom(1024)
	print echo[0]
