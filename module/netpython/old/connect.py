#!/usr/bin/python
#Filename:connect.py

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = socket.getservbyname('http', 'tcp')
tohost = 'www.google.com'

print 'connecting...%s, %d'%(tohost, port)
s.connect((tohost, port))
print 'done'

print 'connect form', s.getsockname()
print 'connect to', s.getpeername()
s.close()
