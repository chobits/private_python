#!/usr/bin/python
#Filename: host.py
import sys, socket

if len(sys.argv) != 2:
	print '<usage> %s HOSTNAME' % sys.argv[0]

counter = 0
result = socket.getaddrinfo(sys.argv[1], None, 0, socket.SOCK_STREAM)

#socket.AF_INET == 2
for item in result:
	print '%-2d:%s' % (counter, item)
	counter += 1
