#!/usr/bin/python
#Filename: addr.py
import sys, socket
def getipaddrs(hostname):
	result = socket.getaddrinfo(sys.argv[1], None, 0, socket.SOCK_STREAM)
	return [ip[4][0] for ip in result]

if len(sys.argv) != 2:
	print '<usage> %s IP' % sys.argv[0]
	sys.exit(1)

try:
	result = socket.gethostbyaddr(sys.argv[1])
	print 'HOST:\n   ', result[0]
	print 'ALIAS:'
	for item in result[1]:
		print '    ' + item
	print 'ADDRESSES:'
	for item in result[2]:
		print '    ' + item
except socket.herror, e:
	print 'couldn\'t look up name:', e

#sanity checks on reverse-lookup data
ips = getipaddrs(result[0])
if sys.argv[1] in ips:
	print 'valid host'
else:
	print 'F**k! We\'re tricked!'


