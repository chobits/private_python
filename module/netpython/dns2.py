#!/usr/bin/python
#Filename: dns2.py
import sys, DNS

def hierquery(name, qtype):
	reqobj = DNS.Request()
	try:
		ansobj = reqobj.req(name = name, qtype = qtype)
		ansdata = [x['data'] for x in ansobj.answers if x['type'] == qtype]	
	except DNS.Base.DNSError:
		ansdata =[]
	if len(ansdata):
		return ansdata
	else:
		secondname = name.split('.', 1)	
		if len(secondname) == 1:
			return None
		else:
			return hierquery(secondname[1], qtype)

def findnameservers(hostname):
	return hierquery(hostname, DNS.Type.NS)

#get records from name server:Authoritative name server(NS)
def getrecordsfromnameserver(name, qtype, nslist):
	answers = []
	for ns in nslist:
		reqobj = DNS.Request(server = ns)
		try:
			ansobj = reqobj.req(name = name, qtype = qtype)
			answers = ansobj.answers
			if len(ansobj.answers):
				return answers	
			#answers.extend(ansobj.answers)
		except DNS.Base.DNSError:
			pass
	return answers 
	
def nslookup(name, qtype, verbose = 1):
	nslist = findnameservers(name)
	if nslist == None:
		raise RuntimeError, "couldn't find name servers"
	if verbose == 1:
		print 'using nameservers:', ','.join(nslist)
	return getrecordsfromnameserver(name, qtype, nslist)


if __name__ == '__main__':
	queryname = sys.argv[1]
	DNS.DiscoverNameServers()

	answers = nslookup(queryname, DNS.Type.ANY)
	if not len(answers):
		print 'not found'
		sys.exit(0)
	for item in answers:
		print '%-5s %s'% (item['typename'], item['data'])
