#!/usr/bin/python
#Filename: dns.py

import sys, DNS

query = sys.argv[1]
#initializing the name servers
DNS.DiscoverNameServers()

#creating request object
reqobj = DNS.Request()

# @req(name, qtype): perform the actual lookup
# @name: the actual name to lookup
# @qtype: specify one of the record types
# return: answer object
# answerobj.answers: has a list of all answers 
answerobj = reqobj.req(name = query, qtype = DNS.Type.ANY)
#print '%s' % type(answerobj) 'instance'

if not len(answerobj.answers):
	print 'not found'
for item in answerobj.answers:
	print '%-5s %s' % (item['typename'], item['data'])

