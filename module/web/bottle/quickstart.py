#!/usr/bin/python
from bottle import route, run

@route('/')
def root():
    return 'This is a root directory!'

@route('/hello/<name>')
def greet(name='Stranger'):
    return 'Hello, how are you? %s' % name

@route('/object/<id:int>')
def callback(id):
    assert isinstance(id, int)

#@route('/object/<id:float>')
#def callback(id):
#    return 'float %s' % id

@route('/object/<name:re:[a-z]+>')
def callback(name):
    assert name.isalpha()
    return 'Your name is %s' % name

run(host='127.0.0.1', port=8080, debug=True)
