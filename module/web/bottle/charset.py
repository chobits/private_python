#!/usr/bin/python
from bottle import route, run, response

@route('/iso')
def callback():
    #response.charset = 'ISO-8859-15'
    response.content_type = 'text/html; charset=ISO-8859-15'
    return u'This will be sent with ISO-8859-15 encoding.'

@route('/latin9')
def callback():
    response.content_type = 'text/html; charset=latin9'
    return u'ISO-8859-15 is also known as latin9.'

run(host='127.0.0.1', port=8080, debug=True)
