#!/usr/bin/python
from bottle import run, get, post, request, static_file

@get('/login')
def login_form():
    return static_file('login.html', '.')

def check_login(name, password):
    return name == 'admin' and password == 'admin'
@post('login')
def login_submit():
    name = request.forms.get('name')
    password = request.forms.get('password')
    if check_login(name, password):
        return "<p> Your login was correct </p>"
    else:
        return "<p> Login Failed </p>"

run(host='127.0.0.1', port=8080, debug=True)
