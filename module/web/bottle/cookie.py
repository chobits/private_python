#!/usr/bin/python
from bottle import route, run, request, response

@route('/')
def root():
    return 'This is a root directory!'

@route('/hello')
def greet():
    v = request.get_cookie('visited')
    if v:
        print v
        response.set_cookie('visited', str(int(v) + 1))
        return 'Welcome back! %s' % v
    else:
        response.set_cookie('visited', '1')
        return 'Hello!'

run(host='127.0.0.1', port=8080, debug=True)
