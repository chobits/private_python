#!/usr/bin/python
from bottle import route, run, static_file

@route('/static/<filename:re:.*\.py>')
def server_static(filename):
    return static_file(filename, '.', mimetype='text')

@route('/static/<filename:path>')
def server_static(filename):
    return static_file(filename, '.')

@route('/images/<filename:re:.*\.png>')
def server_image(filename):
    return static_file(filename, '.', mimetype='image/png')

@route('/download/<filename:path>')
def server_download(filename):
    return static_file(filename, '.', download=filename)

run(host='127.0.0.1', port=8080, debug=True)
