#!/usr/bin/python
import pygame, sys, time

class Color:
    def __init__(self, x = 0, y = 0, z = 0):
        self.red = x
        self.green = y
        self.blue = z
        pygame.init()
        self.screen = pygame.display.set_mode((200, 200))
    def display(self):
        self.screen.fill((self.red, self.green, self.blue))
        print '(R=%d, G=%d, B=%d)' %(self.red, self.green, self.blue)
        pygame.display.flip()


color = Color()

# set(delay, interval) milliseconds(ms)
pulse = 10
pygame.key.set_repeat(pulse, pulse)

mainloop = True
while mainloop:
    color.display()
    event = pygame.event.wait()
    if event:
         if event.type == pygame.QUIT:
             mainloop = False
             break
         elif event.type == pygame.KEYDOWN:
             if event.key == pygame.K_q:
                 break
             elif event.key == pygame.K_a:
                 color.red += 10
                 if color.red > 255:
                     color.red = 255
             elif event.key == pygame.K_s:
                 color.green += 10
                 if color.green > 255:
                     color.green = 255
             elif event.key == pygame.K_d:
                 color.blue += 10
                 if color.blue > 255:
                     color.blue = 255
             elif event.key == pygame.K_z:
                 color.red -= 10
                 if color.red < 0:
                     color.red = 0
             elif event.key == pygame.K_x:
                 color.green -= 10
                 if color.green < 0:
                     color.green = 0
             elif event.key == pygame.K_c:
                 color.blue -= 10
                 if color.blue < 0:
                     color.blue = 0 

print 'display quit'
pygame.display.quit()
print 'python exiting...'
