#!/usr/bin/python
# example: how to draw one image on another
#       or how to draw a text on background

import pygame
screen = pygame.display.set_mode((100,100))
screen.fill((255,255,255)) # white background
red_block = pygame.Surface((50,50))
red_block.fill((255,0,0))

# draw (part of) red block onto white background
# red_block -- pygame.Surface
# (25, 25) -- position of screen: (top, left)
# (0, 0, 20, 20) -- part of red block: (top, left, width, height)
screen.blit(red_block, (25,25), (0, 0, 20, 20))
pygame.display.update()

