#!/usr/bin/python
import pygame, sys

class Cube:
    #  +-------> y
    #  |
    # \|/
    #  '
    #  x
    def changecolor(c):
        pass
    def moveto(self, move):
        (self.lastx, self.lasty) = (self.x, self.y)
        # left
        if move == 0:
            if self.y > 0:
                self.y -= 1
        # right
        elif move == 1:
            if self.y < self.width - 1:
                self.y += 1
        # up
        elif move == 2:
            if self.x > 0:
                self.x -= 1
        # down
        elif move == 3:
            if self.x < self.height - 1:
                self.x += 1
        print 'left:%d top:%d' %(self.y, self.x)
    def __init__(self, h = 5, w = 5):
        (self.height, self.width) = (h, w)
        (self.x, self.y) = (0, 0)
    	(self.lastx, self.lasty) = (0, 0)
    	self.len = 10
    	self.color = (0, 0, 0)

cube = Cube(50, 50)
print cube.width
print cube.len

pulse = 50
bgcolor = (0, 255, 255)

pygame.init()
# set(delay, interval) milliseconds(ms)
#hold down 'k' key: (k)delay(k)interval(k)interval(k)interval...interval(k)
pygame.key.set_repeat(pulse, pulse)
screen = pygame.display.set_mode((cube.width * cube.len, cube.height * cube.len))
screen.fill(bgcolor)

mainloop = True
while mainloop:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            mainloop = False
        elif event.type == pygame.KEYDOWN:
            # left
            if event.key == pygame.K_h:
                cube.moveto(0)
            # right 
            elif event.key == pygame.K_l:
                cube.moveto(1)
            # up
            elif event.key == pygame.K_k:
                cube.moveto(2)
            # down
            elif event.key == pygame.K_j:
                cube.moveto(3)
            elif event.key == pygame.K_q:
                mainloop = False
                break
            else:
                if event.key == pygame.K_w:
                    pulse -= 10
                    if pulse < 10:
                        pulse = 10
                    pygame.key.set_repeat(pulse, pulse)
                elif event.key == pygame.K_e:
                    pulse += 10
                    if pulse > 200:
                        pulse = 200
                    pygame.key.set_repeat(pulse, pulse)
                continue
        rectone = pygame.Rect(cube.lasty * cube.len, cube.lastx * cube.len, cube.len, cube.len)
        pygame.draw.rect(screen, bgcolor, rectone)
        rectone = pygame.Rect(cube.y * cube.len, cube.x * cube.len, cube.len, cube.len)
        pygame.draw.rect(screen, cube.color, rectone)
        pygame.display.flip()

print 'display quit'
pygame.display.quit()
print 'python exiting...'
