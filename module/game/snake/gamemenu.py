#!/usr/bin/python
from gamecolor import *

menulist = [ "   start", "   exit" ]
fontsize = 100
font = None
oselect = 0
select = 0

def displaystring(background, index, str, color = ColorBlack):
    background.screen.blit(font.render(str, 0, color, background.color), (0, index * fontsize))

def displayselect(background):
    print '%d %d', (oselect, select)
    displaystring(background, oselect, "  ", ColorRed)
    displaystring(background, select, "o", ColorRed)

def displayentry(background, index):
    displaystring(background, index, menulist[index])

def displaygm(background):
    background.displaybackground()
    i = 0
    while i < len(menulist):
        displayentry(background, i)
        i += 1

def GameMenu(background):
    print 'game menu!'
    global font, select, oselect
    font = background.pg.font.SysFont("None", fontsize)
    displaygm(background)
    displayselect(background)
    background.pg.display.flip()
    # key handling
    mainloop = True
    while mainloop:
        event = background.pg.event.wait()
        if event: 
            if event.type == background.pg.QUIT:
                mainloop = False
                break
            elif event.type == background.pg.KEYDOWN:
                if event.key == background.pg.K_RETURN:
                    print 'enter'
                    return select
                # handle up and down key
                oselect = select
                # up select entry
                if event.key == background.pg.K_k:
                    select -= 1
                    if select < 0:
                        select = 0
                # down select entry
                elif event.key == background.pg.K_j:
                    select += 1
                    if select >= len(menulist):
                        select = len(menulist) - 1
                if oselect != select:
                    displayselect(background)
                    background.pg.display.flip()
    print 'fuck!'
    sys.exit(-1)
