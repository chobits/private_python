#!/usr/bin/python
import time, sys


# Game Over
def displaygo(background):
    len = background.cube_length
    fontsize = len * 5
    color = (255,255,0)
    font = background.pg.font.SysFont("None", fontsize)
    background.screen.blit(font.render("Game Over", 0, (color)), (0, 0))
    background.pg.display.update()

def GameExit(background, ret = 0):
    print 'display quit'
    background.pg.display.quit()
    print 'python exiting...'
    sys.exit(ret)


def GameOver(background):
    displaygo(background)
    while True:
        event = background.pg.event.wait()
        if event.type == background.pg.KEYDOWN and event.key == background.pg.K_q:
            break

