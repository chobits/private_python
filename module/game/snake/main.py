#!/usr/bin/python

import pygame, time, random
from snake import BackGround, GameStart
from gameover import *
from gamemenu import GameMenu

# create background
bg = BackGround()
bg.displayinit()

while True:
    # display menu
    ret = GameMenu(bg)
    if ret == 0:
        ret = GameStart(bg)
        if ret < 0:
            GameOver(bg)
    else:
        break

GameExit(bg, ret)
