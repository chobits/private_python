#!/usr/bin/python
import pygame, time, random
from gamecolor import *

DirectVim = 0

if DirectVim:
    DirectLeft = pygame.K_h
    DirectRight = pygame.K_l
    DirectUp = pygame.K_k
    DirectDown = pygame.K_j
else:
    DirectLeft = pygame.K_LEFT
    DirectRight = pygame.K_RIGHT
    DirectUp = pygame.K_UP
    DirectDown = pygame.K_DOWN

ReverseDirects = { DirectLeft:DirectRight, DirectRight:DirectLeft,
                   DirectUp:DirectDown, DirectDown:DirectUp }

class Cube:
    def __init__(self, x = 0, y = 0, c = ColorLightBlue):
        self.left = x
        self.top = y
        self.color = c

class BackGround:
    def __init__(self, x = 20, y = 15, z = 20):
        self.width = x
        self.height = y
        self.color = ColorLightBlue
        self.cube_length = z
        self.pg = pygame
    def displaybackground(self):
        self.screen.fill(self.color)
        self.pg.display.flip()
    def displayinit(self):
        # display initing
        self.pg.init()
        self.screen = self.pg.display.set_mode((self.width * self.cube_length, self.height * self.cube_length))
        self.displaybackground()
    def killcube(self, cube):
        self.rectone = self.pg.Rect(cube.left * self.cube_length,
                                cube.top * self.cube_length,
                                self.cube_length,
                                self.cube_length)
        self.pg.draw.rect(self.screen, self.color, self.rectone)
    def drawcube(self, cube):
        self.rectone = self.pg.Rect(cube.left * self.cube_length,
                                cube.top * self.cube_length,
                                self.cube_length,
                                self.cube_length)
        print 'draw cube: (%d, %d)' %(cube.left, cube.top)
        self.pg.draw.rect(self.screen, cube.color, self.rectone)
    def display(self):
        self.pg.display.flip()

class Apple:
    def __init__(self):
        self.cube = Cube(1, 1, ColorRed)
        self.oldcube = Cube(1, 1)
    def __next(self, background):
        self.oldcube.left, self.oldcube.top = self.cube.left, self.cube.top
        while self.oldcube.left == self.cube.left:
            self.cube.left = random.randint(0, background.width - 1)
        while self.oldcube.top == self.cube.top:
            self.cube.top = random.randint(0, background.height - 1)
    def next(self, snake, background):
        self.__next(background)
        while (self.cube.left, self.cube.top) in [(c.left, c.top) for c in snake.body]:
            self.__next(background)
    def display(self, background):
        background.drawcube(self.cube)
        background.display()

class Snake:
    def __init__(self, length = 10):
        self.body = []
        i = 0
        while i < length:
            self.body.append(Cube(0, 0, ColorBlack))
            i += 1
        self.head = self.body[0]
        self.oldtail = Cube(0, 0, ColorBlack)
        # init body
        i = 0
        for c in reversed(self.body):
            print self.body.index(c)
            c.left = i
            i += 1
        # default move direction: left
        self.direct = DirectRight
        self.stop = 0
        self.doeatapple = 0
        # flush speed
        self.ticks = 0
        self.speed = 10
        self.fps = 80
        self.clock = pygame.time.Clock()
    def redirect(self, direct):
        if ReverseDirects[direct] == self.direct or direct == self.direct:
            return 0
        self.direct = direct
        self.stop = 0
        return 1
    def dead(self, background):
        if (self.direct == DirectRight and self.head.left >= background.width - 1) \
            or (self.direct == DirectLeft and self.head.left <= 0) \
            or (self.direct == DirectDown and self.head.top >= background.height - 1) \
            or (self.direct == DirectUp and self.head.top <= 0):
            print 'bang to wall'
            self.stop = 1
            return 1
        return 0
    def advance(self, background):
        # can it move?
        if self.dead(background):
            return
        self.head = self.body.pop()
        # save old tail
        (self.oldtail.left, self.oldtail.top) = (self.head.left, self.head.top)
        (self.head.left, self.head.top) = (self.body[0].left, self.body[0].top)
        # move head to new position
        if self.direct == DirectLeft:
            self.head.left -= 1
        elif self.direct == DirectRight:
            self.head.left += 1
        elif self.direct == DirectUp:
            self.head.top -= 1
        elif self.direct == DirectDown:
            self.head.top += 1
        # eat self?
        if (self.head.left, self.head.top) in [(c.left, c.top) for c in self.body]:
            print 'eat self'
            self.stop = 1
        self.body.insert(0, self.head)
    def update(self, background):
        if self.stop:
            return
        if self.doeatapple:
            self.body.append(Cube(self.oldtail.left, self.oldtail.top, ColorBlack))
            self.doeatapple = 0
        else:
            background.killcube(self.oldtail)
        background.drawcube(self.head)
        background.display()
    def display(self, background):
        for c in self.body:
            background.drawcube(c)
        background.display()
    def updatespeed(self, delta):
        self.speed += delta
        if self.speed > 100:
            self.speed = 100
        elif self.speed < 10:
            self.speed = 10
    def tick(self):
        self.clock.tick(self.fps)
        self.ticks += 1
    def eatapple(self, apple, background):
        if (apple.cube.left == self.head.left) \
            and (apple.cube.top == self.head.top):
            background.drawcube(apple.cube)
            self.doeatapple = 1
            return 1
        return 0
def GameStart(bg):
    bg.displaybackground()
    # create snake
    snake = Snake(2)
    snake.display(bg)

    # create apple
    apple = Apple()
    apple.display(bg)

    mainloop = True
    redirect = 0
    while mainloop:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                mainloop = False
            elif event.type == pygame.KEYDOWN:
                if event.key in ReverseDirects.keys():
                    redirect = snake.redirect(event.key)
                elif event.key == pygame.K_q:
                    mainloop = False
                    break
                else:
                    if event.key == pygame.K_w:
                        snake.updatespeed(10)
                    elif event.key == pygame.K_e:
                        snake.updatespeed(-10)
                    continue
        # Snake eat apple?
        if snake.eatapple(apple, bg):
            apple.next(snake, bg)
            apple.display(bg)
        # snake advance
        if redirect or snake.ticks % snake.speed == 0:
            snake.advance(bg)
            redirect = 0
        if snake.stop:
            print 'stop'
            return -1
        # flush
        snake.update(bg)
        snake.tick()
    return 0
