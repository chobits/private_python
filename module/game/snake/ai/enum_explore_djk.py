#!/usr/bin/python
import random
import time
import os

# basic elements
ROAD = ' '
NROAD = '?'     # not road (temp wall)
CORNER = '+'    # corner
VWALL = '|'     # vertical wall
LWALL = '-'     # level wall
HEAD = 'O'      # snake head
BODY = 'X'      # snake body
APPLE = '@'
road = [ROAD, APPLE]
delta = 0.07

debug = False
def dprint(str):
    if debug:
        print str

class Grid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.matrix = [[ROAD] * height + [LWALL] for i in range(width)] + \
                      [[VWALL] * height + [CORNER]]
    def display(self):
        # Althugh the display method is so fool, clear and flip all each time,
        # it can work well now.
        os.system('clear')
        for y in range(-1, self.height + 1):
            print ''.join([self.matrix[x][y] for x in range(-1, self.width + 1)])



# shortes path algorithm
def dijkstra(start, end, grid):
    infinity = 0xdeadbeef
    # paths[x][y] -> (path len from start to (x, y), previous path coordinate)
    paths = [[(infinity, None)] * grid.height for i in range(grid.width)]
    paths[start[0]][start[1]] = (0, None)
    # reachable paths for next loop
    next = [start]
    while next:
        (x, y) = next.pop()
        # This judgement makes it fast to break loop.
        if (x, y) == end:
            break
        # auxilary function, finding shorter path to (nx, ny)
        def __shorter((nx, ny), (x, y)):
            next_len = paths[x][y][0] + 1
            if (grid.matrix[nx][ny] in road) and paths[nx][ny][0] > next_len:
                paths[nx][ny] = (next_len, (x, y))
                next.insert(0, (nx, ny))
        __shorter((x, y - 1), (x, y))   # up
        __shorter((x + 1, y), (x, y))   # right
        __shorter((x, y + 1), (x, y))   # down
        __shorter((x - 1, y), (x, y))   # left
    if not paths[end[0]][end[1]][1]:
        dprint('\nNo road from (%d, %d) to (%d, %d)\n' \
            %(start[0], start[1], end[0], end[1]))
        return []
    dprint('\nFIND the road from (%d, %d) to (%d, %d)\n' \
        %(start[0], start[1], end[0], end[1]))
    # get complete path
    shortest_path = []
    (x, y) = end
    while (x, y) != start:
        shortest_path.insert(0, (x, y))
        (x, y) = paths[x][y][1]
    shortest_path.insert(0, start)
    return shortest_path

class Snake:
    def __init__(self, width = 10, height = 10):
        self.grid = Grid(width, height)
        self.body = [(0, 0)]
        self.paths = []
        self.grid.matrix[0][0] = HEAD
        self.apples = 0
        self.max_apples = width * height
    def display(self):
        self.grid.display()
        print 'Snake eats %d apples.' %self.apples
    def beg_apple(self):
        if self.apples >= self.max_apples:
            print 'Snake eats all apples!'
            return None
        while True:
            x = random.randrange(self.grid.width)
            y = random.randrange(self.grid.height)
            if self.grid.matrix[x][y] == ROAD:
                self.grid.matrix[x][y] = APPLE
                self.display()
                return (x, y)
    def __find_path(self, start, end):
        # find final path
        if start == end:
            return [start]
        # get adjacent paths around start
        def adjacent_paths((x, y)):
            return [(x, y - 1), (x + 1, y), (x, y + 1), (x - 1, y)]
        apaths = adjacent_paths(start)
        # Using recursion method to find next
        # path from adjacent point of start to end
        for (ax, ay) in apaths:
            if self.grid.matrix[ax][ay] in road:
                # save orignal state and mark it as wall(not road)
                orig = self.advance_one_step((ax, ay))
                # recursion to find next path
                npath = self.find_path((ax, ay), end)
                # restore orignal state
                self.back_one_step(orig)
                if npath:
                    npath.insert(0, start)
                    return npath
        return None

    def find_path(self, start, end):
        paths = dijkstra(start, end, self.grid)
        if paths:
            return paths
        return self.__find_path(start, end)
 
    def want_eat(self, apple):
        self.paths = self.find_path(self.body[0], apple)
        if self.paths:
            return self.paths
        return None
    # There is a reachable path to apple,
    # so snake follows self.paths and eat the apple.
    def advance_eat(self, (ax, ay)):
        for (x, y) in self.paths[1:]:
            # create new head
            (hx, hy) = self.body[0]
            self.grid.matrix[hx][hy] = BODY
            self.grid.matrix[x][y] = HEAD
            self.body.insert(0, (x, y))
            # drop old tail
            (ox, oy) = self.body.pop()
            if self.body[-1] != (ox, oy):
                self.grid.matrix[ox][oy] = ROAD
            time.sleep(delta)
            self.display()
        # eat apple
        self.body.append(self.body[-1])
        self.apples += 1
    def advance_one_step(self, (x, y)):
        # create new head
        (hx, hy) = self.body[0]
        self.grid.matrix[hx][hy] = BODY
        old_head_state = self.grid.matrix[x][y] # save orignal advanced path state
        self.grid.matrix[x][y] = HEAD
        self.body.insert(0, (x, y))
        # drop old tail
        (ox, oy) = self.body.pop()
        self.grid.matrix[ox][oy] = ROAD
        # return old tail
        return (old_head_state, (ox, oy))
    def back_one_step(self, (old_head_state, (x, y))):
        # add new tail
        self.body.append((x, y))
        self.grid.matrix[x][y] = BODY
        # drop head
        (hx, hy) = self.body.pop(0)
        self.grid.matrix[hx][hy] = old_head_state
        # reset head
        (nx, ny) = self.body[0]
        self.grid.matrix[nx][ny] = HEAD

def game_start():
    snake = Snake(10, 8)
    while True:
        apple = snake.beg_apple()   # generate an apple
        if not apple:
            break
        if not snake.want_eat(apple):
            break
        snake.advance_eat(apple)
    print 'Oooooops, game over!'

if __name__ == '__main__':
    while True:
        game_start()
