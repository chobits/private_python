#!/usr/bin/python
import random
import time
import os
import sys

# basic elements
ROAD = ' '
CORNER = '+'    # corner
VWALL = '|'     # vertical wall
LWALL = '-'     # level wall
HEAD = 'O'      # snake head
BODY = 'X'      # snake body
APPLE = '@'
road = [ROAD, APPLE]

def random_int(start, end):
    if start == end:
        return start
    return random.randint(start, end)

class Grid:
    def __init__(self, width, height):
        # [-->x][^y]
        self.width = width
        self.height = height
        self.matrix = [[ROAD] * height + [LWALL] for i in range(width)] + \
                      [[VWALL] * height + [CORNER]]
    def display(self):
        # Althugh the display method is so fool, clear and flip all each time,
        # it can work well now.
        os.system('clear')
        for y in range(-1, self.height + 1):
            print ''.join([self.matrix[x][y] for x in range(-1, self.width + 1)])

class Snake:
    def __init__(self, width = 10, height = 10):
        self.grid = Grid(width, height)
        self.body = [(0, 0)]
        self.path = []
        self.final_path = []
        self.path_len = width * height
        self.grid.matrix[0][0] = HEAD
        self.apples = 0

    def display(self):
        self.grid.display()
        print 'Snake eats %d apples.' %self.apples

    def _beg_apple_odd(self):
         while True:
            x = random.randrange(self.grid.width)
            y = random.randrange(self.grid.height)
            if self.grid.matrix[x][y] == ROAD:
                return (x, y)

    def _beg_apple_even(self):
        start = self.final_path.index(self.body[0]) + 1
        end = start + self.path_len - len(self.body) - 1
        return self.final_path[random.randint(start, end)]

    def beg_apple(self):
        if len(self.body) >= self.path_len:
            self.display()
            return None
        (ax, ay) = self._beg_apple_odd()
        self.grid.matrix[ax][ay] = APPLE
        self.display()
        return (ax, ay)

    def want_eat(self, apple):
        # check for odd * odd grid: have eaten special apple
        if self.path[-1] == (self.grid.width - 1, 0):
            start = self.path_len - 2
            end = self.final_path.index(apple, start)
            self.path = self.final_path[start:end]
            if not self.path:
                print 'fuck2'
                sys.exit(0)
            self.path[0] = (self.grid.width - 1, 0)
        # check for odd * odd grid: eat special apple
        elif apple == (self.grid.width - 1, 0):
            start = self.final_path.index(self.path[-1])
            end = self.path_len - 2
            self.path = self.final_path[start:end]
            if not self.path:
                print 'fuck'
                sys.exit(0)
            self.path[-1] = apple
        else:
            start = self.final_path.index(self.path[-1])
            end = self.final_path.index(apple, start)
            self.path = self.final_path[start:end + 1]

    # There is a reachable path to apple,
    # so snake follows self.path and eat the apple.
    def advance_eat(self, (ax, ay)):
        for (x, y) in self.path[1:]:
            # create new head
            (hx, hy) = self.body[0]
            self.grid.matrix[hx][hy] = BODY
            self.grid.matrix[x][y] = HEAD
            self.body.insert(0, (x, y))
            # drop old tail
            (ox, oy) = self.body.pop()
            if self.body[-1] != (ox, oy):
                self.grid.matrix[ox][oy] = ROAD
            time.sleep(0.05)
            self.display()
        # eat apple
        self.body.append(self.body[-1])
        self.apples += 1

    def search_path(self):
        width = self.grid.width
        height = self.grid.height
        # even * (odd|even)
        if width % 2 == 0:
            l = [ (x, 0) for x in range(width - 1, -1, -1) ]
            for x in range(0, width, 2):
                l += [ (x, y) for y in range(1, height) ]
                l += [ (x + 1, y) for y in range(height - 1, 0, -1) ]
        # odd * even
        elif height % 2 == 0:
            l = [ (0, y) for y in range(height - 1, -1, -1) ]
            for y in range(0, height, 2):
                l += [ (x, y) for x in range(1, width) ]
                l += [ (x, y + 1) for x in range(width - 1, 0, -1) ]
        # odd * odd
        else:
            l = [ (x, 0) for x in range(width - 2, -1, -1) ]
            for x in range(0, width - 3, 2):
                l += [ (x, y) for y in range(1, height) ]
                l += [ (x + 1, y) for y in range(height - 1, 0, -1) ]
            l += [ (width - 3, y) for y in range(1, height) ]
            for y in range(height - 1, 1, -2):
                l += [ (width - 2, y), (width -1, y) ]
                l += [ (width - 1, y - 1), (width - 2, y - 1) ]
            #l.append((width - 1, 1))

        self.final_path = l + l
        self.path.append(self.body[0])

if __name__ == '__main__':
    snake = Snake(5, 5)
    snake.search_path()
    while True:
        apple = snake.beg_apple()   # generate an apple
        if not apple:
            break
        snake.want_eat(apple)
        snake.advance_eat(apple)
    print 'Game Over!'
