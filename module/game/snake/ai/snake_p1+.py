#!/usr/bin/python
import random
import time
import os

# basic elements
ROAD = ' '
TEMP_ROAD = ROAD
NROAD = '?'     # not road (temporary wall)
CORNER = '+'    # corner
VWALL = '|'     # vertical wall
LWALL = '-'     # level wall
HEAD = 'O'      # snake head
BODY = 'X'      # snake body
APPLE = '@'
road = [ROAD, APPLE]
delta = 0.05

debug = False
def dprint(str):
    if debug:
        print str

class Grid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.matrix = [[ROAD] * height + [LWALL] for i in range(width)] + \
                      [[VWALL] * height + [CORNER]]
    def display(self):
        os.system('clear')
        for y in range(-1, self.height + 1):
            print ''.join([self.matrix[x][y] for x in range(-1, self.width + 1)])


def find_specified_path(start, end, nsteps, grid):
    # get adjacent paths around start
    def adjacent_paths((x, y)):
        return [(x, y - 1), (x + 1, y), (x, y + 1), (x - 1, y)]
    apaths = adjacent_paths(start)

    # if path from start to end is final
    if nsteps == 1:
        if end in apaths:
            return [start, end]
        return None

    # Using recursion method to find (n - 1 steps)
    # path from adjacent point of start to end
    for (ax, ay) in apaths:
        if grid.matrix[ax][ay] in road:
            # save orignal state and mark it as wall(not road)
            orig = grid.matrix[ax][ay]
            grid.matrix[ax][ay] = NROAD
            # recursion to (n - 1) steps
            npath = find_specified_path((ax, ay), end, nsteps - 1, grid)
            # restore orignal state
            grid.matrix[ax][ay] = orig
            if npath:
                npath.insert(0, start)
                return npath
    # no way to run
    return None

def dijkstra(start, end, grid):
    infinity = 0xdeadbeef
    paths = [[(infinity, None)] * grid.height for i in range(grid.width)]
    paths[start[0]][start[1]] = (0, None)
    next = [start]
    while next:
        (x, y) = next.pop()
        # This judgement makes it fast to break loop.
        if (x, y) == end:
            break
        # auxilary function, finding shorter path to (nx, ny)
        def __shorter((nx, ny), (x, y)):
            next_len = paths[x][y][0] + 1
            if (grid.matrix[nx][ny] in road) and paths[nx][ny][0] > next_len:
                paths[nx][ny] = (next_len, (x, y))
                next.insert(0, (nx, ny))
        __shorter((x, y - 1), (x, y))   # up
        __shorter((x + 1, y), (x, y))   # right
        __shorter((x, y + 1), (x, y))   # down
        __shorter((x - 1, y), (x, y))   # left
    if not paths[end[0]][end[1]][1]:
        dprint('\nNo road from (%d, %d) to (%d, %d)\n' \
            %(start[0], start[1], end[0], end[1]))
        return []
    dprint('\nFIND the road from (%d, %d) to (%d, %d)\n' \
        %(start[0], start[1], end[0], end[1]))
    # get complete path
    shortest_path = []
    (x, y) = end
    while (x, y) != start:
        shortest_path.insert(0, (x, y))
        (x, y) = paths[x][y][1]
    shortest_path.insert(0, start)
    return shortest_path

class Snake:
    def __init__(self, width = 10, height = 6):
        self.grid = Grid(width, height)
        self.body = [(0, 0)]
        self.paths = []
        self.grid.matrix[0][0] = BODY
        self.apples = 0
    def display(self):
        self.grid.display()
        print 'Snake eats %d apples.' %self.apples
    def beg_apple(self):
        # FIXME: infinite loop
        while True:
            x = random.randrange(self.grid.width)
            y = random.randrange(self.grid.height)
            if self.grid.matrix[x][y] == ROAD:
                self.grid.matrix[x][y] = APPLE
                self.display()
                return (x, y)
    def try_eat(self, apple):
        global delta
        # from mid to apple
        # traverse snake body from its tail
        for mid in reversed(self.body):
            mpaths = dijkstra(mid, apple, self.grid)
            if mpaths:
                break
        else:
            return False
        # from head to mid
        # save snake.grid BODY
        tmp_paths = []
        for (x, y) in reversed(self.body):
            self.grid.matrix[x][y] = TEMP_ROAD
            tmp_paths.append((x, y))
            if (x, y) == mid:
                break
        spaths = dijkstra(self.body[0], mid, self.grid)
        # restore snake.grid BODY
        for (x, y) in tmp_paths:
            self.grid.matrix[x][y] = BODY
        # Can snake move from head to mid?
        tail_advance = len(spaths) - 1
        tail_should_advance = self.body[::-1].index(mid) + 1
        if tail_advance < tail_should_advance:
            return False
        # final paths: head -> mid -> apple
        self.paths = spaths + mpaths[1:]
        delta = 0.05
        return True

    def try_try_eat(self, apple):
        global delta
        # from mid to apple
        # traverse snake body from its tail
        for mid in reversed(self.body):
            mpaths = dijkstra(mid, apple, self.grid)
            if mpaths:
                break
        else:
            return False

        # save snake.grid BODY
        tmp_paths = []
        for (x, y) in reversed(self.body):
            self.grid.matrix[x][y] = TEMP_ROAD
            tmp_paths.append((x, y))
            if (x, y) == mid:
                break
        # from head to mid
        tail_advance_steps = self.body[::-1].index(mid) + 1
        for steps in range(tail_advance_steps, 50):
            spaths = find_specified_path(self.body[0], mid, steps, self.grid)
            if spaths:
                break
        # restore snake.grid BODY
        for (x, y) in tmp_paths:
            self.grid.matrix[x][y] = BODY

        if spaths:
            # final paths: head -> mid -> apple
            self.paths = spaths + mpaths[1:]
            delta = 0.5
            return True
        return False

    def want_eat(self, apple):
        global delta
        self.paths = dijkstra(self.body[0], apple, self.grid)
        delta = 0.05
        return self.paths != []
    # There is a reachable path to apple,
    # so snake follows self.paths and eat the apple.
    def advance_eat(self, (ax, ay)):
        for (x, y) in self.paths[1:]:
            # create new head
            (hx, hy) = self.body[0]
            self.grid.matrix[hx][hy] = BODY     # old head -> body
            self.grid.matrix[x][y] = HEAD       # new head
            self.body.insert(0, (x, y))
            # drop old tail
            (ox, oy) = self.body.pop()
            if self.body[-1] != (ox, oy):       # if eating apple, it has a new tail
                self.grid.matrix[ox][oy] = ROAD
            # wait a delay let people see
            time.sleep(delta)
            self.display()
        # eat an apple, making snake longer
        self.body.append(self.body[-1])
        self.apples += 1

if __name__ == '__main__':
    snake = Snake(5, 10)
    while True:
        apple = snake.beg_apple()   # generate an apple
        if not snake.want_eat(apple):
            if not snake.try_eat(apple):
                if not snake.try_try_eat(apple):
                    break
        snake.advance_eat(apple)
    print 'Oooooops, game over!'
