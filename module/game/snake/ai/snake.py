#!/usr/bin/python
import random
import time
import os

# basic elements
ROAD = ' '
CORNER = '+'    # corner
VWALL = '|'     # vertical wall
LWALL = '-'     # level wall
HEAD = 'O'      # snake head
BODY = 'X'      # snake body
APPLE = '@'
road = [ROAD, APPLE]

class Grid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.matrix = [[ROAD] * height + [LWALL] for i in range(width)] + \
                      [[VWALL] * height + [CORNER]]
    def display(self):
        # Althugh the display method is so fool, clear and flip all each time,
        # it can work well now.
        os.system('clear')
        for y in range(-1, self.height + 1):
            print ''.join([self.matrix[x][y] for x in range(-1, self.width + 1)])

# shortes path algorithm
def dijkstra(start, end, grid):
    infinity = 0xdeadbeef
    # paths[x][y] -> (path len from start to (x, y), previous path coordinate)
    paths = [[(infinity, None)] * grid.height for i in range(grid.width)]
    paths[start[0]][start[1]] = (0, None)
    # reachable paths for next loop
    next = [start]
    while next:
        (x, y) = next.pop()
        # This judgement makes it fast to break loop.
        if (x, y) == end:
            break
        # auxilary function, finding shorter path to (nx, ny)
        def __shorter((nx, ny), (x, y)):
            next_len = paths[x][y][0] + 1
            if (grid.matrix[nx][ny] in road) and paths[nx][ny][0] > next_len:
                paths[nx][ny] = (next_len, (x, y))
                next.insert(0, (nx, ny))
        __shorter((x, y - 1), (x, y))   # up
        __shorter((x + 1, y), (x, y))   # right
        __shorter((x, y + 1), (x, y))   # down
        __shorter((x - 1, y), (x, y))   # left
    # no path from start to end
    if not paths[end[0]][end[1]][1]:
        return []
    # get complete path
    shortest_path = []
    (x, y) = end
    while (x, y) != start:
        shortest_path.insert(0, (x, y))
        (x, y) = paths[x][y][1]
    shortest_path.insert(0, start)
    return shortest_path

class Snake:
    def __init__(self):
        self.grid = Grid(10, 10)
        self.body = [(0, 0)]
        self.paths = []
        self.grid.matrix[0][0] = HEAD
        self.apples = 0
    def display(self):
        self.grid.display()
        print 'Snake eats %d apples.' %self.apples
    def beg_apple(self):
        # FIXME: infinite loop
        while True:
            x = random.randrange(self.grid.width)
            y = random.randrange(self.grid.height)
            if self.grid.matrix[x][y] == ROAD:
                self.grid.matrix[x][y] = APPLE
                self.display()
                return (x, y)
    def want_eat(self, apple):
        self.paths = dijkstra(self.body[0], apple, self.grid)
        return self.paths != []
    # There is a reachable path to apple,
    # so snake follows self.paths and eat the apple.
    def advance_eat(self, (ax, ay)):
        for (x, y) in self.paths[1:]:
            # create new head
            (hx, hy) = self.body[0]
            self.grid.matrix[hx][hy] = BODY
            self.grid.matrix[x][y] = HEAD
            self.body.insert(0, (x, y))
            # drop old tail
            (ox, oy) = self.body.pop()
            if self.body[-1] != (ox, oy):
                self.grid.matrix[ox][oy] = ROAD
            time.sleep(0.04)
            self.display()
        # eat apple
        self.body.append(self.body[-1])
        self.apples += 1

if __name__ == '__main__':
    snake = Snake()
    while True:
        apple = snake.beg_apple()   # generate an apple
        if not snake.want_eat(apple):
            break
        snake.advance_eat(apple)
    print 'Oooooops, game over!'
