#!/usr/bin/python
import time
import sys

ROAD = 0
WALL = 1
THROUGH = 2

class Maze:
    def __init__(self, width, height):
        self.width, self.height = width, height
        # ERROR: id(matrix[0]) == id(matrix[1])
        # self.matrix = [[ROAD] * width + [WALL]] * height + \
        #               [[WALL] * (width + 1)]

        # init matrix
        self.matrix = [[ROAD] * width + [WALL] for i in range(height)] + \
                      [[WALL] * (width + 1)]
        self.text_display()
        # init other params
        self.paths = []
        self.path = []
        self.start = ()
        self.end = ()
    def set_start(self, start):
        if self.start == start:
            return ()
        old = self.start
        self.start = start
        return old
    def set_end(self, end):
        if self.end == end:
            return ()
        old = self.end
        self.end = end
        return old
    def text_display(self):
        print '\n'.join([str(i) for i in self.matrix])

#    def walk_

    # A* algorithm
    def walk_astar(self, grid = None):
        if not self.start or not self.end:
            return
        # define f(x) = g(x) + h(x)
        def h((x, y)):
            return abs(self.end[0] - x) + abs(self.end[1] - y)
        def f((x, y)):
            desc = path_dict[(x, y)]
            #      g(x)      h(x)
            return desc[0] + desc[1]
        def open_to_close_list():
            oxy = open_list.pop()
            close_list.append(oxy)
            # debug to display
            if grid:
                grid.drawon_update_screen((oxy[1], oxy[0]), grid.axcube)
                time.sleep(0.1)
            return oxy
        def add_to_open_list(xy):
            index = 0
            fxy = f(xy)
            # open_list: decreasing order
            for oxy in open_list:
                if fxy > f(oxy):
                    break
                index += 1
            open_list.insert(index, xy)
            # debug to display
            if grid:
                grid.drawon_screen((xy[1], xy[0]), grid.aocube)
        # A* auxilary function
        def handle_adjacent((cx, cy), (ax, ay)):
            if self.matrix[ax][ay] == WALL or (ax, ay) in close_list:
                return
            cg = path_dict[(cx, cy)][0]
            if (ax, ay) in open_list:
                adesc = path_dict[(ax, ay)]
                # g(x) of current path < g(x) of adjacent path
                if cg < adesc[0]:
                    adesc[0] = cg
                    adesc[2] = (cx, cy)
            else:
                path_dict[(ax, ay)] = [cg + 1, h((ax, ay)), (cx, cy)]
                add_to_open_list((ax, ay))
        # auxilary arguments
        open_list = [self.start]
        close_list = []
        path_dict = {self.start:[0, h(self.start), None]} # key-(x, y):value-[g, h, prev]
        # A* body
        while open_list:
            cx, cy = open_to_close_list()
            if (cx, cy) == self.end:
                break
            handle_adjacent((cx, cy), (cx, cy + 1)) # up
            handle_adjacent((cx, cy), (cx + 1, cy)) # right
            handle_adjacent((cx, cy), (cx, cy - 1)) # down
            handle_adjacent((cx, cy), (cx - 1, cy)) # left
        else:
            self.astar_paths = []
            return
        # caculate final path
        self.path = [self.end]
        prev = self.end
        while prev != self.start:
            prev = path_dict[prev][2]
            self.path.insert(0, prev)

    def walk_dijkstra(self, grid = None):
        if not self.start or not self.end:
            return
        # auxilary function:
        def __shorter_path((x, y), (ax, ay)):
            if self.matrix[ax][ay] == ROAD and paths_len[ax][ay][0] > paths_len[x][y][0] + 1:
                paths_len[ax][ay] = (paths_len[x][y][0] + 1, (x, y))
                # debug to display
                if grid:
                    grid.drawon_screen((ay, ax), grid.axcube)
                next.insert(0, (ax, ay))
        # solve
        infinity = 0xdeadbeef
        paths_len = [[(infinity, None)] * self.width for i in range(self.height)]
        paths_len[self.start[0]][self.start[1]] = (0, None)
        next = [self.start]
        while next:
            (x, y) = next.pop()
            if grid:
                grid.update_screen()
                time.sleep(0.1)
            # This condition makes it fast to break loop.
            if (x, y) == self.end:
               break
            __shorter_path((x, y), (x - 1, y)) # up
            __shorter_path((x, y), (x, y - 1)) # left
            __shorter_path((x, y), (x + 1, y)) # down
            __shorter_path((x, y), (x, y + 1)) # right
        if not paths_len[self.end[0]][self.end[1]][1]:
            print '\nNo road from (%d, %d) to (%d, %d)\n' \
                %(self.start[0], self.start[1], self.end[0], self.end[1])
            return
        print '\nFIND the road from (%d, %d) to (%d, %d)\n' \
            %(self.start[0], self.start[1], self.end[0], self.end[1])
        # calculate final path
        (x, y) = self.end
        while (x, y) != self.start:
            self.path.insert(0, (x, y))
            (x, y) = paths_len[x][y][1]
        self.path.insert(0, self.start)

    def walk_bidirect_bfs(self, grid = None):
        if not self.start or not self.end:
            return
        # auxilary function
        def goto_adjacent(open_list, close_list, path_dict, (ax, ay)):
            if (ax, ay) not in close_list and (ax, ay) not in open_list and self.matrix[ax][ay] == ROAD:
                open_list.insert(0, (ax, ay))
                path_dict[(ax, ay)] = n
                # debug to display
                if grid and (ax, ay) not in [self.start, self.end]:
                    grid.drawon_screen((ay, ax), grid.aocube)
        def open_to_close_list(open_list, close_list):
            oxy = open_list.pop()
            close_list.append(oxy)
            # debug to display
            if grid:
                grid.drawon_update_screen((oxy[1], oxy[0]), grid.axcube)
                time.sleep(0.05)
            return oxy
        def get_path(src, dst, path_dict):
            path = [dst]
            while path[0] != src:
                path.insert(0, path_dict[path[0]])
            return path

        # from start direction
        open_start_list = [self.start]
        close_start_list = []
        start_path_dict = {self.start:None}

        # from end direction
        open_end_list = [self.end]
        close_end_list = []
        end_path_dict = {self.end:None}

        crossing = None

        while open_start_list and open_end_list:
            # from start direction
            n = open_to_close_list(open_start_list, close_start_list)
            if n in open_end_list:
                crossing = n
                break
            (x, y) = n
            goto_adjacent(open_start_list, close_start_list, start_path_dict, (x, y - 1))
            goto_adjacent(open_start_list, close_start_list, start_path_dict, (x - 1, y))
            goto_adjacent(open_start_list, close_start_list, start_path_dict, (x, y + 1))
            goto_adjacent(open_start_list, close_start_list, start_path_dict, (x + 1, y))
            # from end direction
            n = open_to_close_list(open_end_list, close_end_list)
            if n in open_start_list:
                crossing = n
                break
            (x, y) = n
            goto_adjacent(open_end_list, close_end_list, end_path_dict, (x, y - 1))
            goto_adjacent(open_end_list, close_end_list, end_path_dict, (x - 1, y))
            goto_adjacent(open_end_list, close_end_list, end_path_dict, (x, y + 1))
            goto_adjacent(open_end_list, close_end_list, end_path_dict, (x + 1, y))

        # get final path
        if crossing:
            s = get_path(self.start, crossing, start_path_dict)
            s.pop()
            e = get_path(self.end, crossing, end_path_dict)
            self.path = s + e[::-1]
            print 'got'

    def walk_bfs(self, grid = None):
        if not self.start or not self.end:
            return
        # auxilary function
        def goto_adjacent((ax, ay)):
            if (ax, ay) not in close_list and (ax, ay) not in open_list and self.matrix[ax][ay] == ROAD:
                open_list.insert(0, (ax, ay))
                path_dict[(ax, ay)] = n
                # debug to display
                if grid and (ax, ay) not in [self.start, self.end]:
                    grid.drawon_screen((ay, ax), grid.aocube)
        def open_to_close_list():
            oxy = open_list.pop()
            close_list.append(oxy)
            # debug to display
            if grid:
                grid.drawon_update_screen((oxy[1], oxy[0]), grid.axcube)
                time.sleep(0.05)
            return oxy

        open_list = [self.start]
        close_list = []
        path_dict = {self.start:None}
        while open_list:
            n = open_to_close_list()
            if n == self.end:
                break
            # handle adjacent points
            (x, y) = n
            goto_adjacent((x, y - 1))
            goto_adjacent((x - 1, y))
            goto_adjacent((x, y + 1))
            goto_adjacent((x + 1, y))

        # get final path
        if path_dict.has_key(self.end):
            path = [self.end]
            while path[0] != self.start:
                path.insert(0, path_dict[path[0]])
            self.path = path

    def walk(self, grid = None):
        if not self.start or not self.end:
            return
        # auxilary function:
        def __shorter_path((ax, ay)):
            if self.matrix[ax][ay] == ROAD and (ax, ay) not in walkthrough:
                cross.append((ax, ay))
        # solve
        walkthrough = []
        paths = []
        curpath = []
        crosses = [[self.start]]
        while crosses:
            curpath.append(crosses[-1].pop())
            if not crosses[-1]:
                crosses.pop()
            (x, y) = curpath[-1]
            walkthrough.append((x, y))
            if (x, y) == self.end:
                paths.append(curpath)
                break
            # depth first
            cross = []
            # up
            __shorter_path((x - 1, y))  # up
            __shorter_path((x, y - 1))  # left
            __shorter_path((x + 1, y))  # down
            __shorter_path((x, y + 1))  # right
            if len(cross) == 0:
                curpath = []
            elif len(cross) == 1:
                crosses.append(cross)
            elif len(cross) > 1:
                paths.append(curpath)
                curpath = []
                crosses.append(cross)
        else:
            print '\nNo road from (%d, %d) to (%d, %d)\n' \
                %(self.start[0], self.start[1], self.end[0], self.end[1])
            return
        print '\nFIND the road from (%d, %d) to (%d, %d)\n' \
            %(self.start[0], self.start[1], self.end[0], self.end[1])
        self.paths = paths
        self.path = reduce(lambda prevtotal, next: prevtotal + next, paths)
