import pygame
import time
from gamecolor import *
from maze import *

ENUM = 0
DIJKSTRA = 1
ASTAR = 2
BFS = 3
BIBFS = 4

class Cube:
    def __init__(self, len, color):
        self.len = len
        self.color = color
        # create surface block
        self.block = pygame.Surface((self.len, self.len))
        self.block.fill(self.color)
    def set_color(self, color):
        self.color = color
        self.block.fill(color)

class Grid:
    def __init__(self, width, height):
        self.cube = Cube(10, ColorBlue)
        self.width, self.height = width, height
    def set_bgcolor(self, bgcolor):
        self.cube.set_color(bgcolor)
    def init_screen(self):
        self.screen = pygame.display.set_mode((self.width * self.cube.len, self.height * self.cube.len))
        self.screen.fill(self.cube.color)
        pygame.display.update()
    def drawon_screen(self, (left, top), cube):
        left, top = left * self.cube.len, top * self.cube.len
        self.screen.blit(cube.block, (left, top), (0, 0, cube.len, cube.len))
    def update_screen(self):
        pygame.display.flip()
    def drawon_update_screen(self, xy, cube):
        self.drawon_screen(xy, cube)
        self.update_screen()

class MazeGrid(Grid):
    def __init__(self, width, height, algorithm = ASTAR):
        Grid.__init__(self, width, height)
        self.throughcube = Cube(self.cube.len, ColorYellow)
        self.aocube = Cube(self.cube.len, ColorLightBlue) # A* open list cube 
        self.axcube = Cube(self.cube.len, ColorPuple) # A* close list cube
        self.startcube = Cube(self.cube.len, ColorRed)
        self.endcube = Cube(self.cube.len, ColorGreen)
        self.wall = Cube(self.cube.len, ColorBlack)
        self.ccube = self.wall
        self.maze = Maze(self.width, self.height)
        self.cube2code = { self.wall:WALL, self.cube:ROAD }
        self.algorithm = algorithm

    def handle_mouse_down(self, event):
        # tuple( left, top )
        # print event.pos, type(event.pos)
        left = event.pos[0] / self.cube.len
        top = event.pos[1] / self.cube.len
        old = ()
        if event.button == 1:
            # set wall
            self.drawon_screen((left, top), self.ccube)
            self.maze.matrix[top][left] = self.cube2code[self.ccube]
            # self.maze.text_display()
        elif event.button == 2:
            print 'set start'
            old = self.maze.set_start((top, left))
            self.drawon_screen((left, top), self.startcube)
        elif event.button == 3:
            print 'set end'
            old = self.maze.set_end((top, left))
            self.drawon_screen((left, top), self.endcube)
        if old:
            print self.cube
            self.drawon_screen((old[1], old[0]), self.cube)
        self.update_screen()

    def handle_keydown(self, event):
        if event.key == pygame.K_w:
            self.ccube = self.wall
        elif event.key == pygame.K_r:
            self.ccube = self.cube
        elif event.key == pygame.K_RETURN:
            # display final path
            if self.algorithm == ENUM:
                self.maze.walk(self)
            elif self.algorithm == DIJKSTRA:
                self.maze.walk_dijkstra(self)
            elif self.algorithm == ASTAR:
                self.maze.walk_astar(self)
            elif self.algorithm == BFS:
                self.maze.walk_bfs(self)
            elif self.algorithm == BIBFS:
                self.maze.walk_bidirect_bfs(self)
            self.walk_display()
        elif event.key == pygame.K_q:
            print 'Quit'
            return False
        return True

    def events(self):
        mainloop = True
        while mainloop:
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    print 'Exit'
                    mainloop = False
                elif e.type == pygame.MOUSEBUTTONDOWN:
                    self.handle_mouse_down(e)
                elif e.type == pygame.KEYDOWN:
                    mainloop = self.handle_keydown(e)
                if not mainloop:
                    break

    def walk_display(self):
        if not self.maze.path:
            print 'Not solved'
            return
        # kill end and start cube
        self.maze.path.pop()
        self.maze.path.pop(0)
        for (x, y) in self.maze.path:
            self.drawon_update_screen((y, x), self.throughcube)
            time.sleep(0.1)
