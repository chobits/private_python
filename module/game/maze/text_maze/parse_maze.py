#!/usr/bin/python
import sys
import re

ROAD = 0
WALL = 1
THROUGH = 2

mazechar2code = { '.':ROAD, 'o':WALL, ' ':THROUGH }
mazecode2char = { ROAD:'.', WALL:'o', THROUGH:' ' }

class maze:
    def __init__(self, (width, height), start, end):
        (self.width, self.height) = (width, height)
        self.start = (start[0] + 1, start[1] + 1)
        self.end = (end[0] + 1, end[1] + 1)
        self.matrix = [None] * height
        self.matrix.insert(0, [WALL] * (width + 2))
        self.matrix.append([WALL] * (width + 2))
    def line_fill(self, row, line):
        if len(line) != self.width + 1:     # line is terminated with '\n'
            raise Exception('0 Invalid line: %s' %line)
        try:
            row += 1    # first row has been filled with wall      
            self.matrix[row] = [ mazechar2code[ch] for ch in line[0:-1] ]
            self.matrix[row].insert(0, WALL)
            self.matrix[row].append(WALL)
        except:
            raise Exception('1 Invalid line: %s' %line)
    def text_display(self):
        for row in self.matrix:
            print ''.join([mazecode2char[code] for code in row])
    def walk(self):
        # solve
        walkthrough = []
        paths = []
        curpath = []
        crosses = [[self.start]]
        while crosses:
            curpath.append(crosses[-1].pop())
            if not crosses[-1]:
                crosses.pop()
            (x, y) = curpath[-1]
            walkthrough.append((x, y))
            if (x, y) == self.end:
                paths.append(curpath)
                break
            # depth first
            cross = []
            # up
            if self.matrix[x - 1][y] == ROAD and (x - 1, y) not in walkthrough:
                cross.append((x - 1, y))
            # left
            if self.matrix[x][y - 1] == ROAD and (x, y - 1) not in walkthrough:
                cross.append((x, y - 1))
            # down
            if self.matrix[x + 1][y] == ROAD and (x + 1, y) not in walkthrough:
                cross.append((x + 1, y))
            # right
            if self.matrix[x][y + 1] == ROAD and (x, y + 1) not in walkthrough:
                cross.append((x, y + 1))
            if len(cross) == 0:
                curpath = []
            elif len(cross) == 1:
                crosses.append(cross)
            elif len(cross) > 1:
                paths.append(curpath)
                curpath = []
                crosses.append(cross)
        else:
            print '\nNo road from (%d, %d) to (%d, %d)\n' \
                %(self.start[0], self.start[1], self.end[0], self.end[1])
            return
        print '\nFIND the road from (%d, %d) to (%d, %d)\n' \
            %(self.start[0], self.start[1], self.end[0], self.end[1])
        for path in paths:
            for (x, y) in path:
                self.matrix[x][y] = THROUGH

def parse_maze(filename):
    try:
        f = open(filename, 'r')
    except:
        print 'cannot open file:', filename
        return None

    # parse (w, h), start, end
    try:
        width, height = [int(i) for i in re.findall('[0-9]+', f.readline())]
        start = [int(i) for i in re.findall('[0-9]+', f.readline())]
        end = [ int(i) for i in re.findall('[0-9]+', f.readline())]
    except Exception, evalue:
        print str(evalue)
        print 'The format(size, start, end) of maze file is invalid'
        return None

    # sanity check
    if (width < 1 or width > 10) and (height < 1 or height > 10):
        print 'Invalid width=%d height=%d' %(width, height)
        return None
    if start[0] >= height or start[1] >= end:
        print 'start coordinate is invalid'
        return None
    if end[0] >= height or end[1] >= end:
        print 'end coordinate is invalid'
        return None

    # create maze
    m = maze((width, height), start, end)
    lino = 0
    for line in f:
        if lino < height:
            try:
                m.line_fill(lino, line)
            except Exception, e:
                print str(e)
                return None
        else:
            print 'Warning: too many maze line'
            break
        lino += 1
    f.close()
    return m

if len(sys.argv) == 1 or len(sys.argv) > 2:
    print 'Usage: %s mazefile' %sys.argv[0]
    sys.exit(-1)

m = parse_maze(sys.argv[1])
if not m:
    sys.exit(-1)
m.text_display()
m.walk()
m.text_display()
