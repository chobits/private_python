#!/usr/bin/python
import sys
from grid import *

def usage(ret):
    print 'Usage: %s ALGORITHM' %sys.argv[0]
    print 'ALGORITH: 0 for enum'
    print '          1 for djkstra'
    print '          2 for A*'
    print '          3 for BFS'
    print '          4 for Bidirect BFS'
    sys.exit(ret)

if len(sys.argv) != 2:
    usage(-1)

algorithm = int(sys.argv[1])
if algorithm not in range(5):
    usage(-1)

mg = MazeGrid(20, 20, algorithm)
mg.init_screen()
mg.events()

