import pygame
import random
from gamecolor import *

DEAD = 0
LIVE = 1

class Cube:
    def __init__(self, len, color):
        self.len = len
        self.color = color
        # create surface block
        self.block = pygame.Surface((self.len, self.len))
        self.block.fill(self.color)
    def set_color(self, color):
        self.color = color
        self.block.fill(color)

class Grid:
    def __init__(self, width, height, len = 10, fps = 10):
        self.dead_cube = Cube(len, ColorWhite)
        self.live_cube = Cube(len, ColorBlack)
        self.width, self.height = width, height
        self.matrix = [[DEAD] * (height + 1) for i in range(width + 1)]
        #timer
        self.clock = pygame.time.Clock()
        self.fps = fps
    def init_screen(self):
        self.screen = pygame.display.set_mode((self.width * self.dead_cube.len,
                                               self.height * self.dead_cube.len))
        self.screen.fill(self.dead_cube.color)
        pygame.display.update()
    def drawon_screen(self, (left, top), cube):
        left, top = left * cube.len, top * cube.len
        self.screen.blit(cube.block, (left, top), (0, 0, cube.len, cube.len))
    def update_screen(self):
        pygame.display.flip()
    def evolveit(self, x, y):
        lives = self.matrix[x - 1][y - 1] + \
                self.matrix[x][y - 1] + \
                self.matrix[x + 1][y - 1] + \
                self.matrix[x - 1][y] + \
                self.matrix[x + 1][y] + \
                self.matrix[x - 1][y + 1] + \
                self.matrix[x][y + 1] + \
                self.matrix[x + 1][y + 1]
        # 1 Any live cell with fewer than two live neighbours dies,
        #   as if caused by under-population.
        # 2 Any live cell with two or three live neighbours lives on
        #   to the next generation.
        # 3 Any live cell with more than three live neighbours dies,
        #   as if by overcrowding.
        if self.matrix[x][y] == LIVE and (lives < 2 or lives > 3):
            # print x, y, 'is dead', lives
            return [DEAD, (x, y)]
        # 4 Any dead cell with exactly three live neighbours becomes
        #   a live cell, as if by reproduction.
        if self.matrix[x][y] == DEAD and lives == 3:
            # print x, y, 'is live', lives
            return [LIVE, (x, y)]
        return None
    def text(self):
        for y in range(self.height): 
            for x in range(self.width):
                print self.matrix[x][y],
            print
    def init_live(self, (x, y), live):
        self.matrix[x][y] = live
        self.drawon_screen((x, y),
            {DEAD:self.dead_cube, LIVE:self.live_cube}[live])
    def init_lives(self):
        for i in range(self.width * self.height / 4):
            x = random.randrange(self.width)
            y = random.randrange(self.height)
            self.init_live((x, y), LIVE)
        self.update_screen()
        self.clock.tick(self.fps)
    def evolve(self):
        # self.text()
        evolves = []
        for x in range(self.width):
            for y in range(self.height):
                r = self.evolveit(x, y)
                if r:
                    evolves.append(r)
        for (live, coordinate) in evolves:
            self.init_live(coordinate, live)
        # timer
        self.update_screen()
        self.clock.tick(self.fps)
    def events(self):
        mainloop = True
        while mainloop:
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    print 'Exit'
                    mainloop = False
                    break
            self.evolve()
