#!/usr/bin/python
import pygame
from gamecolor import *

width = 20
height = 20
margin = 5

row = 10
column = 10
grid = [[0] * column for i in range(row)]

pygame.init()

screen_size = [255, 255]
screen = pygame.display.set_mode(screen_size)
pygame.display.set_caption("Grid Example")

clock = pygame.time.Clock()

screen.fill(ColorBlack)
for y in range(row):
    for x in range(column):
        pygame.draw.rect(screen, ColorWhite, [margin + (width + margin) * y, margin + (height + margin) * x, width, height])
 
mainloop = True
while mainloop:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            mainloop = False
            break
        if event.type == pygame.MOUSEBUTTONDOWN:
            x, y = pygame.mouse.get_pos()
            x = (x - 5) / (width + margin)
            y = (y - 5) / (height + margin)
            if x < 0:
                x = 0
            if y < 0:
                y = 0
            print x, y
            grid[y][x] = 1

    for y in range(row):
        for x in range(column):
            if grid[x][y] > 0:
                pygame.draw.rect(screen, ColorGreen,\
                                 [margin + (width + margin) * y,
                                  margin + (height + margin) * x,
                                  width, height])
    clock.tick(20)
    pygame.display.flip()
pygame.quit()

