#!/usr/bin/python
import pygame
from gamecolor import *
import random

def update(self):
    self.rect.y += 2
    if self.rect.y >= screen_height:
        self.rect.y = random.randrange(-20, -10)
        self.rect.x = random.randrange(0, screen_width)

class Block(pygame.sprite.Sprite):
    def __init__(self, color, width, height):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([width, height])
        self.image.fill(color)
        self.rect = self.image.get_rect()


pygame.init()
screen_width = 700
screen_height = 400
screen = pygame.display.set_mode([screen_width, screen_height])

player = Block(ColorRed, 20, 15)
all_sprites_list = pygame.sprite.RenderPlain()
all_sprites_list.add(player)

block_list = pygame.sprite.RenderPlain()
# hook update method to my defined method
pygame.sprite.Sprite.update = update

print id(block_list)
print id(all_sprites_list)
for i in range(50):
    block = Block(ColorBlack, 20, 15)
    block.rect.x = random.randrange(screen_width)
    block.rect.y = random.randrange(screen_height)
    block_list.add(block)
    all_sprites_list.add(block)

score = 0
clock = pygame.time.Clock()

mainloop = True
while mainloop:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            mainloop = False
            break
    screen.fill(ColorWhite)

    # locate player
    pos = pygame.mouse.get_pos()
    player.rect.x = pos[0]
    player.rect.y = pos[1]

    # kill hit blocks and caculate score
    blocks_hit_list = pygame.sprite.spritecollide(player, block_list, True)
    if len(blocks_hit_list) > 0:
        score += len(blocks_hit_list)
        print score

    # draw left blocks
    all_sprites_list.draw(screen)
    pygame.display.flip()
    block_list.update()

    clock.tick(20)

pygame.quit()
