#!/usr/bin/python
import pygame
from gamecolor import *

pi = 3.1415926

pygame.init()
size = [400, 400]
pygame.display.set_caption('ARCS')
screen = pygame.display.set_mode(size)
screen.fill(ColorWhite)
clock = pygame.time.Clock()

# text: 'Text'
font = pygame.font.Font(None, 25)
text = font.render('Text', True, ColorBlack)
screen.blit(text, [200, 200])

# line
pygame.draw.line(screen, ColorGreen, [0, 0], [100, 100], 5)
pygame.draw.line(screen, ColorGreen, [10, 0], [110, 100], 5)
pygame.draw.line(screen, ColorGreen, [20, 0], [120, 100], 5)
pygame.draw.line(screen, ColorGreen, [30, 0], [130, 100], 5)

# rectangle
pygame.draw.rect(screen, ColorBlack, [10, 10, 250, 100], 2)
# ellipse
pygame.draw.ellipse(screen, ColorBlack, [10, 10, 250, 100], 2)
# arc
#       pi/2
#         |
#   pi +--+--+ 0
#
pygame.draw.arc(screen, ColorRed, [10, 10, 250, 100], 0, pi / 2, 2)

# This draws a triangle using the polygon command
pygame.draw.polygon(screen , ColorBlack, [[100 ,100] ,[0 ,200] ,[200 ,200]] ,5)

# flick
pygame.display.flip()
mainloop = True
    
while mainloop:
    clock.tick(10)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            mainloop = False
            break
pygame.quit()
