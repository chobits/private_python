#!/usr/bin/python
import random
import pygame
from gamecolor import *

pygame.init()
size = [400, 400]
pygame.display.set_caption('ARCS')
screen = pygame.display.set_mode(size)
clock = pygame.time.Clock()


# init snow
snow_list = []
for i in range(100):
    x = random.randrange(0, 400)
    y = random.randrange(0, 400)
    size = random.randrange(1,5)
    snow_list.append([x, y, size])
 

# flick
mainloop = True
    
while mainloop:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            mainloop = False
            break
    screen.fill(ColorBlack)
    for snow in snow_list:
        pygame.draw.circle(screen, ColorWhite, snow[0:2], snow[2])
        # move to right
        snow[0] += 1
        if snow[0] >= 400:
            snow[0] = random.randrange(-30, -5)
            snow[2] = random.randrange(1, 3)
        # drop
        snow[1] += 3
        if snow[1] >= 400:
            snow[1] = random.randrange(-50, -10)
    pygame.display.flip()
    clock.tick(10)
pygame.quit()
