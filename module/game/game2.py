#!/usr/bin/python
import pygame, random, time
pygame.init() 
# (width, heigth): (0, 0) for fill all screen
screen = pygame.display.set_mode((200, 100))
screen.fill([255,255,255])
mainloop = True
x, y, color = 0, 0, (32,32,32)
fontsize, delta, fps = 35, 1, 30
clock = pygame.time.Clock() # create clock object

while mainloop:
    tick_time = clock.tick(fps) # milliseconds since last frame
    pygame.display.set_caption("press Esc to quit. FPS: %.2f" % (clock.get_fps()))
    fontsize = 50
    myFont = pygame.font.SysFont("None", fontsize)
    color = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
    screen.fill((255,255,255))
    screen.blit(myFont.render("LOVE", 0, (color)), (x,y))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            mainloop = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                mainloop = False
    pygame.display.update()
    time.sleep(0.5)

# Game Over
screen.fill((255,255,255))
screen.blit(myFont.render("Game Over", 0, (color)), (0, 0))
pygame.display.update()
time.sleep(1)
