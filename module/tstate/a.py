#!/usr/bin/python
try:
    1/0
except Exception:
    import sys
    # like PyThreadState_GET() 
    # tstate = PyThreadState::(curexc_type, curexc_value, curexc_traceback)
    tstate = sys.exc_info()
    print '\n'.join([ repr(i) for i in tstate])
