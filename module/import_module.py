# how to handle import error
try: import no_exist_module
except ImportError: pass    # handle error

# import module whose name contains dash symbol or hyphen '-'
foo_bar = __import__('foo-bar')

# How to import a module given the full path?
import imp
foo = imp.load_source('module_name', '/path/to/foo.py')
print foo   # output '<module 'module_name' from '/path/to/foo.py'>'

# import a directory
# foo_dir/ is in current directory.
# foo_dir/ contains __init__.py(must have) and bar.py
import foo_dir.bar      # import __init__.py first, and then import bar.py
import foo_dir          # only import __init__.py
from foo_dir import bar # import __init__.py first, and then import bar.py
