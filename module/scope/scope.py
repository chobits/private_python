# scope.py

def g():
    # g_global: scopy.py
    # g_local: g
    print global_a      # 'global'
    print f_a           # NameError: global name 'f_a' is not define

def f():
    # f_global: scopy.py
    # f_local: f
    f_a = 'f'
    g()

def k():
    def kk():
        print k_a
        print global_a
    k_a = 'k'
    kk()
    return kk

global_a = 'global'
#f()
kkf = k()
k_a = 'gloabl_k_a'
kkf()

