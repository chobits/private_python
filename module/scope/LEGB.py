# local-(enclosing)-global-builtin scope order

def f():
    def ff():
        print ff.func_code.co_freevars      # closure
        print a
    a = 'a in f'
    print f.func_code.co_freevars
    return ff
a = 'a in global'
f()()
