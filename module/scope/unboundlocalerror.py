def g():
    print a
    # 0 LOAD_GLOBAL            0 (a)
    # 3 PRINT_ITEM
    # 4 PRINT_NEWLINE

def f():
    print a # UnboundLocalError: local variable 'a' referenced before assignment
    # 0 LOAD_FAST              0 (a)
    # 3 PRINT_ITEM
    # 4 PRINT_NEWLINE
    a = 2
    # 5 LOAD_CONST             1 (2)
    # 8 STORE_FAST             0 (a)

a = 1
g()
f()
