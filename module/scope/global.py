#____ f ____
#[consts]
# (0) None
# (1) 1
#[names]
# (0) 'a'
#[<disassembled code>]
# 3           0 LOAD_CONST             const[1] (1)
#             3 STORE_GLOBAL           names[0] (a)
#             6 LOAD_CONST             const[0] (None)
#             9 RETURN_VALUE
def f():
    global a
    a = 1

#____ g ____
#[consts]
# (0) None
# (1) 1
#[locals]
# (0) 'a'
#[<disassembled code>]
# 6           0 LOAD_CONST             const[1] (1)
#             3 STORE_FAST             local[0] (a)
#             6 LOAD_CONST             const[0] (None)
#             9 RETURN_VALUE
def g():
    a = 1

a = 0
print 'before f(): a =', a
f()
print 'after f(): a =', a

