#!/usr/bin/python

class myiter:
    def __init__(self, callable, initial, sentinel):
        self.value = initial
        self.callable = callable
        self.sentinel = sentinel

    def __iter__(self):
        return self

    def next(self):
        if self.value == self.sentinel:
            raise StopIteration
        else:
            self.value = self.callable(self.value) # calculate next value from prev value
            return self.value

if __name__ == '__main__':
    call_iter = myiter(lambda x:x + 1, 0, 100)
    for i in call_iter:
        print i
