#!/usr/bin/python
#Filename:cat.py
import sys

def usage(program):
	print 'USAGE: %s [option] [filename]'%program
	sys.exit(1)

def readfile(filename):
	f = file(filename, 'r')
	try:
		while True:
			line = f.readline()
			if not len(line):
				break
			print line,
	finally:	
		f.close()

if len(sys.argv) < 2:
	usage(sys.argv[0])

if sys.argv[1][0:2] == '--':
	option = sys.argv[1][2:]
	if option == 'version':
		print 'Version 0.0'
	elif option == 'help':
		usage(sys.argv[0])
	else:
		print 'unknown option:%s'%option
else:
	readfile(sys.argv[1])

	
	
