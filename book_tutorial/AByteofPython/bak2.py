#!/usr/bin/python
#Filename:bak1.py

import os
import time

source = './bak1.py'
target_dir = './'
today = target_dir + time.strftime('%Y%m%d')
now = time.strftime('%H%M%S')

if not os.path.exists(today):
	os.mkdir(today)
	print 'success created directory', today


target = today + os.sep + now + '.zip'
zip_command = 'zip -qr %s %s'%(target, source)
if os.system(zip_command) == 0:
	print 'success zip', target
else:
	print 'failed'
