#!/usr/bin/python
#Filename:error.py

import sys
class myerr(Exception):
	def __init__(self, name):
		Exception.__init__(self)
		self.name = name

try:
	s = raw_input('print your string:')
	if len(s) < 5:
		raise myerr('len < 5')
except myerr, e:
	print '\nerr name:', e.name
	sys.exit(1)
except EOFError:
	print '\nwhy a eof?'
	sys.exit(1)
except:
	print 'some err happen'
else:
	print 'no err'
print 'done'
