#!/usr/bin/python
#Filename:dict.py
ab = {1:'ii',2:'20',5:'6'}
print ab
print 'ab[1] is %s'%ab[1]
del ab[2]
print ab
ab['11'] = 11
print '%d'%ab['11']
print ab
for a,b in ab.items():
	print '%s,%s' %(a,b)
if '11' in ab:
	print '%s' %ab['11']
if ab.has_key(1):
	print '%s' %ab[1]
