#!/usr/bin/python
#Filename: inherit.py
class schoolmember:
	def __init__(self, name, age):
		self.name = name
		self.age = age
		print 'init %s :old %d'%(self.name, self.age)
	def tell(self):
		print 'detail: %s %d'%(self.name, self.age)
	def ha(self):
		print 'ha'

class teacher(schoolmember):
	def __init__(self, name, age, salary):
		schoolmember.__init__(self, name, age)
		self.salary = salary
		print 'init teacher %s'%self.name
	def tell(self):
		schoolmember.tell(self)
		print 'salary:%d'%self.salary

		
class student(schoolmember):
	def __init__(self, name, age, grade):
		schoolmember.__init__(self, name, age)
		self.grade = grade
		print 'init student %s'%self.name
	def tell(self):
		schoolmember.tell(self)
		print 'grade:%d'%self.grade

t = teacher('fukun', 26, 4000)
c = student('wangbo', 20, 39)
members = (t, c)		
print
for member in members:
	member.tell() 
	schoolmember.tell(member)
	member.ha()
