#!/usr/bin/python
#Filename:seq.py
name = '0123456789'
for i in range(0, len(name)):
	print '%d item is'%i, name[i]
print '%d item is'%(-1), name[-1]
print 'char 1 to 3 is', name[1:3]
print 'char 3 to end is', name[3:]
print 'char start to end is', name[:]
print 'char start to 3 is', name[:3]
print 'char 4 to -2 is', name[4:-2]

