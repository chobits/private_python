#!/usr/bin/python
list = ['hello', 'world', 1, 3]
print list
print "legth of list is", len(list)
print list[1]
#insert element in front of list[1]
list[1:1] = ["the inserted one"] # using [ "abc" ], "abc" means ['a', 'b', 'c']
print list
print list[1][0] + list[1][1] + list[1][2]
#insert element to tail of list
list.append("new tail")
print list
#remove list[1]
list[1:2] = []
print list
#change entries: delete list[0], list[1], and insert [1, 2, 3] to head of list
list[0:2] = [1, 2, 3]
print list
#change all list
list = [1,2,3]
print list
#remove all entries of list
list[:] = []
print list
print "-----------------------"
list = [1,3,4,5,6,7]
list.insert(1, 2)
print list
