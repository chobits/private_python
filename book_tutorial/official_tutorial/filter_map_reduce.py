#!/usr/bin/python

def mul(a, b):
    return a * b

def fib(n):
    a, b = 1, 0
    for i in range(n):
        a, b = b, a + b
    return b

def iseven(x):
    return x % 2 == 0 and 1 or 0

print "Even number is ", filter(iseven, range(0, 10))
print "Sacc wireless password is", map(fib, range(0, 10))
print "10! =", reduce(mul, range(1, 11))
