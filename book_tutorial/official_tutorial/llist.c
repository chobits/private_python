#include <stdio.h>
#include <stdlib.h>

struct matrix {
	int *array;
	int x, y;
};

int main(void)
{
	int i, j, k, sum;
	struct matrix m, n, p;
	m.x = 5;
	m.y = 100000;
	m.array = malloc(sizeof(int)*m.x*m.y);
	if (!m.array) {
		perror("malloc");
		return -1;
	}
	for (i = 0; i < m.x; i++)
		for (j = 0; j < m.y; j++)
			m.array[i*m.y + j] = i;

	n.x = 100000;
	n.y = 6;
	n.array = malloc(sizeof(int)*n.x*n.y);
	if (!n.array) {
		perror("malloc");
		return -1;
	}
	for (i = 0; i < n.x; i++)
		for (j = 0; j < n.y; j++)
			n.array[i*n.y + j] = j;

	p.x = 5;
	p.y = 6;
	p.array = malloc(sizeof(int)*p.x*p.y);
	if (!p.array) {
		perror("malloc");
		return -1;
	}

	for (i = 0; i < p.x; i++)
		for (j = 0; j < p.y; j++) {
			sum = 0;
			for (k = 0; k < m.y; k++)
				sum += m.array[i*m.y + k] * n.array[k*n.y + j];
			p.array[i*p.y + j] = sum;
		}


	for (i = 0; i < p.x; i++) {
		printf("|");
		for (j = 0; j < p.y; j++)
			printf("%2d ", p.array[i*p.y + j]);
		printf("|\n");
	}
	return 0;
}
