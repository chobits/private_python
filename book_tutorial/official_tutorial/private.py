#!/usr/bin/python
class mainclass:
    def __init__(self, value):
        print "mainclass __init__"
        self.__update(value)
    def update(self, value):
        print "mainclass update"
        self.value = value
    # make __init__ call mainclass update always!
    __update = update

class subclass(mainclass):
    def update(self, value):
        print "subclass update"
        self.value = 2 * value

x = subclass(10)
print x.value
