#!/usr/bin/python
for i in range(10):
    print i,
print
for i in range(5, 10):
    print i,
print
for i in range(0, 10, 3): # the third parameter of range() is step
    print i,
print
for i in range(0, -10, -3):
    print i,

list = ['dog', 'cat', 'turtle', 'elephant', 'horse', 'butterfly', 'bird', 'penguin']
for i in range(len(list)):
    print "list[%d]"%(i-1),"is "+list[i]
