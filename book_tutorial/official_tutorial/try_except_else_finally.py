#!/usr/bin/python

def divide(x, y):
	try:
		result = x/y
	except ZeroDivisionError:
		print "divide zero"
	else:
		print "the result is {0}".format(result)
	finally:
		print "___left try___"
