#!/usr/bin/python

if 1 == 0:
    pass
else:
    print "pass!"

for i in range(100):
    if (i % 2 == 0):
        continue
    print i,
    if (i == 10):
        break
