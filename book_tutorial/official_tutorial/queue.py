#!/usr/bin/python
import sys
from collections import deque

cmds = ["help", "quit", "enqueue", "dequeue", "create", "list"]

q = None
while 1:
    try:
        cmdl = raw_input("[queue shell] ").split()
    except EOFError:
        print "\nexit"
        break
    except:
        print "\nError:", sys.exc_info()[0]
        break
    if (cmdl[0] not in cmds):
        print "unknown command:", cmdl[0]
        continue
    if (cmdl[0] == "help"):
        print "no help information"
    elif (cmdl[0] == "quit"):
        break
    elif (cmdl[0] == "enqueue"):
        if (len(cmdl) == 1):
            print "enqueue entry [other entries]"
            continue
        if (q == None):
            q = deque(cmdl[1:])
        else:
            q.extend(cmdl[1:])
    elif (cmdl[0] == "dequeue"):
        try:
            print q.popleft(), "is dequeued"
        except IndexError:
            print "queue is empty"
    elif (cmdl[0] == "create"):
        q = deque([])
    elif (cmdl[0] == "list"):
        if (q == None):
            print "no create queue"
        elif (len(q) == 0):
            print "empty queue"
        else:
            print q
