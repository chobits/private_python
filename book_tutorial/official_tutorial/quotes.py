#!/usr/bin/python
print """+---------------+
|               |
+---------------+ """

# r"string" ==> raw string, '\n' will not be translated to newline
print r"This is newline: '\n'"

# '+' concatenate strings automatically
#word = 'a' 'b'  # is ok!
word = 'a' + 'b'
# '*' repeats the string
print '<' + word*5 + '>'

#
# +---+---+---+---+---+
# | a | b | c | d | e |
# +---+---+---+---+---+
# 0   1   2   3   4   5
#-5  -4  -3  -2  -1
word = 'abcde'
print word[1]
print word[1:3] # print word[1],[2],  not [3]
print word[:3]
print word[1:]
print word[:1] + word[1:] # <==> print word
print word[-1] # last one char
#print word[3:1] ==> "", so the string is just a newline
print word + " legth is", len(word) # len is built-in function
