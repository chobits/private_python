#!/usr/bin/python
def matrix_mul(mat1, mat2):
    return ([[ sum([mat1[0][m][p]*mat2[0][p][n] for p in range(mat1[2])])\
            for n in range(mat2[2])] for m in range(mat1[1])], mat1[1], mat2[2])

def matrix_print(matrix):
    for i in range(matrix[1]):
        print "|",
        for j in range(matrix[2]):
            print "%2d"%matrix[0][i][j],
        print "|"


matrixa = ([[i for k in range(0, 100000)] for i in range(0, 5)], 5, 100000)    # 5 * 3
matrixb = ([[k for k in range(0, 6)] for i in range(0, 100000)], 100000, 6)    # 3 * 6
matrixc = matrix_mul(matrixa, matrixb)

#matrix_print(matrixa)
print "*"
#matrix_print(matrixb)
print "-------------------------"
print "="
matrix_print(matrixc)
