#!/usr/bin/python
#Filename: quitsort.py
def quitsort(l):
	if len(l) <= 1:
		return l
	else:
		return quitsort([x for x in l[1:] if x <= l[0]]) \
			+ l[0:1] + quitsort([x for x in l[1:] if x > l[0]])


list = []
print 'input number:'
while 1:
	tmp = raw_input()
	try:
		num = int(tmp)
	except:
		break
	list.append(num)

quitlist = quitsort(list)
print 'orign list:', ','.join([str(x) for x in list])
print 'sorted list:', ','.join([str(x) for x in quitlist])
