#!/usr/bin/python
#Filename: bublesort.py

list = []
print 'input number:'
while 1:
	tmp = raw_input()
	try:
		num = int(tmp)
	except:
		break
	list.append(num)
print 'orign list:', ','.join([str(x) for x in list])

n = len(list)
for i in range(0, n - 1):
	for j in range(0, n - 1 - i):
		if list[j] > list[j + 1]:
			tmpnum = list[j]
			list[j] = list[j + 1]
			list[j + 1] = tmpnum
print 'sorted list:', ','.join([str(x) for x in list])
