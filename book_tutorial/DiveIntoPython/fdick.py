#!/usr/bin/python
#Filename: fdick.py

class mdict:
	def __init__(self, dict = None):
		if dict is not None:
			self.data = dict
		else:
			self.data = {}
	def __getitem__ (self, key):
		return self.data[key]
	def __setitem__ (self, key, value):
		self.data[key] = value
	def __delitem__ (self, key):
		del self.data[key]
	def __iteritems__ (self):
		return self.data.__iteritems__ (self)
