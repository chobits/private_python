#!/usr/bin/python
#Filename: mp3.py
import os, sys
def stripnulls(data):
	return data.replace("\00", "").strip()

mp3data = {	'title': (3,33,stripnulls),
		'artist': (33, 63, stripnulls), 
		'album': (63, 93, stripnulls), 
		'year': (93, 97, stripnulls), 
		'comment': (97, 126, stripnulls), 
		'gener': (127, 128, ord)} 

def printmp3info(file):
	try:
		fd = open(file, 'rb', 0)
		fd.seek(-128, 2)
		data = fd.read(128)
		fd.close()
	#FIXME:I need to handle it better!
	except:
		print 'file handling error'
		fd.close()
		return
	
	if data[:3] == 'TAG':
		for k, (start, end, func) in mp3data.items():
			print '%s = %s' % (k, func(data[start:end]))	
		print
	else:
		return



def filelist(dir, extlist):
	if not os.path.isdir(dir):
		return
	elif dir[-1] != '/':
		dir += '/'

	list = os.listdir(dir)
	#print '\n'.join(list)
	return [dir + file for file in list\
			 if os.path.splitext(file)[1] in extlist]

	
if __name__ == '__main__':
	flist = filelist('/home/Eruda/Music', ['.mp3'])
	for file in flist:
		printmp3info(file)		

#	print '\n'.join(filelist('/home/Eruda/Music', ['.mp3']))
