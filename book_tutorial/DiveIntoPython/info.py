#!/usr/bin/python
#Filename: info.py
import sys

def info(object, spacing = 10, collapse = 1):
	methodlist = [method for method in dir(object) 
			if callable(getattr(object, method))]
	docfunc = collapse and \
		(lambda str: ' '.join(str.split())) or (lambda str:str)
	print '\n'.join(['%s %s'\
	 		% (method.ljust(spacing), \
			docfunc(str(getattr(object, method).__doc__))) \
					for method in methodlist])

if __name__ == '__main__':
	pass	
