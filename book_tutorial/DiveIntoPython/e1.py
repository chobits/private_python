#!/usr/bin/python
#Filename: e1.py

def buildconnectionstring(params):
	"""hello,this is func
	it can return string"""
	return '***'.join(["%s=%s"%(k,v) for k,v in params.items()])

if __name__ == '__main__':
	myParams = {'server':'mpilgrim', \
			'database':'master', \
			'uid':'sa', \
			'pwd':'secret'}
	print buildconnectionstring(myParams)
