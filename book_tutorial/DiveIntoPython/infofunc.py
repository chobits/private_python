#!/usr/bin/python
#Filename: infofunc.py

def info(obj, spacing = 10, change = 1):
	'''111'''
	'''21321123'''
	mlist = [method for method in dir(obj) if callable(getattr(obj, method))]
	changedoc = change and (lambda doc:' '.join(doc.split())) or (lambda doc:doc)
	#getattr() may be an object that has no __doc__ so it may happen that None.split
	dlist = ['%s %s' %(m.ljust(spacing), changedoc(str(getattr(obj, m).__doc__))) for m in mlist]
	print '\n'.join(dlist)
