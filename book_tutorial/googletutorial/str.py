#!/usr/bin/python

# len(string)
# str(integer)
text = "hello wrold"
print text + " has " + str(len(text)) + " chars"
# strip() remove trailing and ending whitespace(default) chars
text = "	hello".strip("o")
print "No space: " + "["+text+"]"
# join
print '|'.join(['hello', 'world'])
# split
print "hello,world".split(",")
# Python does not have a separate character type.
print "\"hello\"[1] == " + "hello"[1]
# slices
print "\"hello\"[2:4] == " + "hello"[2:4]
# string %:
print ("the string is: %s"
	%("hello world"))
#unicode
unistr = u"hello wrold"
print type(unistr)
print type(unistr.encode("utf-8"))
