from inspect import isfunction

def metatrace(name, parents, attrs):
    real_attrs = {}
    for k, v in attrs.iteritems():
        if not k.startswith('__') and isfunction(v):
            real_attrs[k] = generate_trace_func(k, v)
        else:
            real_attrs[k] = v
    real_attrs['__tracelevel'] = 0

    return type(name, parents, real_attrs)

def generate_trace_func(func_name, func):

    def wrap(*args, **kwargs):
        self = args[0]

        print ' ' * self.__tracelevel + '> %s' %func_name

        self.__tracelevel += 1
        ret = func(*args, **kwargs)
        self.__tracelevel -= 1
        return ret

    return wrap

# intercept all classes in this file
#__metaclass__= metatrace

class A():
    # only intercept this class
    __metaclass__= metatrace
    def __init__(self):
        pass

    def bar(self, a, b):
        print a, b

    def foo(self, a, b='world'):
        self.bar(a, b)

a = A()
a.foo('hello')
