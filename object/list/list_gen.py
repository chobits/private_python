#!/usr/bin/python
# id(L1[0]) == id(L1[1])
L1 = [[1, 2]] * 2
# id(L1[0]) != id(L1[1])
L2 = [[1, 2] for i in range(2)]
# id(L1[0]) != id(L1[1])
L3 = [[1, 2], [1, 2]]
