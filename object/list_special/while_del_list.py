#!/usr/bin/python

l = range(0, 10)
while l:
    c = l.pop()
    # do something with c

# the same as following:
l = range(0, 10)
for c in l:
    # do something with c
    pass
l = []
