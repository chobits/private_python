#!/usr/bin/python

print 'for c in l'
l = range(0, 10)
for c in l:
    print c,
    del l[0]
    print l

print '\nfor c in l[::-1]: new reversed list'
l = range(0, 10)
for c in l[::-1]:
    print c,
    del l[0]
    print l

print '\nfor c in reversed(l): reversed list iter'
l = range(0, 10)
for c in reversed(l):
    print c,
    del l[0]
    print l

print '\nfor c in reversed(l) & l.pop(): reversed list iter'
l = range(0, 10)
for c in reversed(l):
    print c,
    l.pop()
    print l

