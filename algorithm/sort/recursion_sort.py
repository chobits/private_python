#!/usr/bin/python
# for CRLS thinking problem 7-4 b)
from itertools import islice

def getmin(l, i, j):
    min_index = 0
    minimum = 999999
    for index in xrange(i, j + 1):
        if l[index] < minimum:
            min_index = index
            minimum = l[index]
    return min_index

# recursion stack depth: O(n)
def recursion_sort(l, i, j):
    if i < j:
        min_index = getmin(l, i, j)
        l[min_index], l[i] = l[i], l[min_index]
        recursion_sort(l, i + 1, j)

l = [2, 3, 4, 1, 2, 3 ,4 ,5 ,1 ,9]
print l
recursion_sort(l, 0, len(l) - 1)
print l
