#!/usr/bin/python
# for CRLS 7-4 c)
# Recursion depth worsest situation is o(lgn)

# assert e > s
def partition(l, s, e):
    i = s           # l[s+1..i] <= l[0]
    for k in xrange(s + 1, e + 1):
        if l[k] <= l[s]:
            i += 1
            l[i], l[k] = l[k], l[i]
    l[s], l[i] = l[i], l[s]
    return i        # l[s..i-1] <= l[i] < l[i+1..e]

def qsort(l, s, e):
    if s < e:
        m = partition(l, s, e) # l[s..m-1] < l[m] < l[m+1..e]
        qsort(l, s, m - 1)
        qsort(l, m + 1, e)

# optimize qsort via tail recursion
def qsort_tail(l, s, e):
    while s < e:
        m = partition(l, s, e)
        qsort_tail(l, s, m - 1)
        s = m + 1

# Modify the code for qsort_tail so that the worst-case stack depth is O(lg n).
# Maintain the O(nlgn) expected running time of the algorithm.
def qsort_tail2(l, s, e):
    while s < e:
        m = partition(l, s, e)
        if m - s < e - m:
            qsort_tail2(l, s, m - 1)
            s = m + 1
        else:
            qsort_tail2(l, m + 1, e)
            e = m - 1

def test(sort, l):
    print 'orignal:', l
    sort(l, 0, len(l) - 1)
    print 'sorted: ', l

test(qsort, [9,5,2,7,1,9,9,0])
test(qsort_tail, [9,5,2,7,1,9,9,0])
test(qsort_tail2, [9,5,2,7,1,9,9,0])
