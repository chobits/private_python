# Time: O(n), Space: O(n), wherer n = len(strings)
def string_count_sort(index, strings):
    slots = [[] for _ in range(256)]
    for s in strings:
        v =  ord(s[index]) if index < len(s) else 0
        slots[v].append(s)
    si = 0
    for slot in slots:
        for s in slot:
            strings[si] = s
            si += 1

# sort in place
# Time: O(nk), Space: O(n), where n = len(strings), k = maxlen
# sometimes, we think its time is O(n), k is think as constant value
def string_radix_sort(strings):
    maxlen = len(max(strings, key=len))     # max(imap(len, strings))
    for i in reversed(range(maxlen)):
        string_count_sort(i, strings)

# will destroy strings
def strings_duplicate_removal(strings):
    if not strings: return []
    string_radix_sort(strings)
    rstrings = []
    for s in strings:
        if not rstrings or rstrings[-1] != s:
            rstrings.append(s)
    return rstrings

# Time: O(n), Space: O(n)
def easy_sdr(strings):
    return list(set(strings))
    
def test():
    strings = ['abcd', 'hello', 'world', 'abcd', 'abc', 'abc', 'abed', 'kk', 'wocao', 'hier!']
    print '\n'.join(strings)

    string_radix_sort(strings)
    print '---------------[sort]----------------'
    print '\n'.join(strings)

def test2():
    strings = ['abcd', 'hello', 'world', 'abcd', 'abc', 'abc', 'abed', 'kk', 'wocao', 'hier!']
    print strings
    print strings_duplicate_removal(strings)

if __name__ == '__main__':
    test2()
