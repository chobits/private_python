import time

def print_time(*ptargs):
    if len(ptargs) == 1 and type(ptargs[0]) == type(lambda:0):
        print '1', ptargs
        return ptargs[0]
    def pt_decorator(f):
        if len(ptargs) != 1 or type(ptargs[0]) != int:
            print '2', ptargs
            return f
        setattr(f, '__ptimes__', ptargs[0])
        def wrap(*args):
            if f.__ptimes__ == 0:
                return f(*args)
            f.__ptimes__ -= 1
            old = time.time()
            r = f(*args)
            print '%s costs %.3f s' %(f.func_name, time.time() - old)
            return r
        return wrap
    return pt_decorator
