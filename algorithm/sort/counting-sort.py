#!/usr/bin/python
import time
import sys

if len(sys.argv) != 2:
    l = range(10, -1, -1)
else:
    l = range(int(sys.argv[1]))

# k is maximum element in l
def counting_sort(l, k, n): # Time: O(n+k)
    r = [0] * n             # O(n)
    counts = [0] * (k+1)    # O(k)
    cur = time.time()
    for x in l:             # O(n)
        counts[x] += 1
    for i in range(1, k+1): # O(k)
        counts[i] += counts[i - 1]
    for x in reversed(l):   # if not reversed, not stable
        r[counts[x] - 1] = x
        counts[x] -= 1
    print 'time:', time.time()-cur
    return r

def my_counting_sort(l, k, n):              # O(n+k)
    r = [0] * n
    counts = [0] * (k+1)                    # O(k)
    cur = time.time()
    for x in l:                             # O(n)
        counts[x] += 1
    ii = 0
    for x, cnt in enumerate(counts):        # O(n+k)
        for j in range(cnt):
            r[ii] = x
            ii += 1
    print 'time:', time.time()-cur
    return r

print counting_sort(l, max(l), len(l))
print my_counting_sort(l, max(l), len(l))
