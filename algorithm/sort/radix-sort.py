#!/usr/bin/python

def log2down(x):
    r = log2up(x)
    if 2**r != x:
        r -= 1
    return r

def log2up(x):
    n = 0
    while 2**n < x:
        n += 1
    return n

# k is maximum element in l
# Time:  O(n+k)
# Space: O(n+k)
def help_counting_sort(l, i, k, n):
    r = [0] * n
    counts = [0] * (k+1)
    for x in l:
        counts[(x/(k**i))%k] += 1
    for j in range(1, k+1):
        counts[j] += counts[j - 1]
    for x in reversed(l):
        r[counts[(x/(k**i))%k] - 1] = x
        counts[(x/(k**i))%k] -= 1
    return r

# time: O(b/r(n+2^r))
# @base=2
# n - length(l)
# b - bit length of maximum element in l
# r - bit length of every radix comparation part
def radix_sort(l, n, r, b):
    for i in range((b+r-1)/r):
        l = help_counting_sort(l, i, 2**r, n)
        # for debug:
        print [(x, (x/((2**r)**i))%(2**r)) for x in l]
    return l

l = [199, 700, 213, 952, 719, 7, 221, 13, 1, 10]

# How to get r?
n = len(l)
b = log2up(max(l))
if 2**b <= n:
    r = b           # radix-sort O(n)
else:
    r = log2down(n) # radix-sort O(bn/lgn)

print radix_sort(l, n, r, b)
