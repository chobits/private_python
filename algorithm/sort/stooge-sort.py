#!/usr/bin/python
# O(n^2.71)

l1 = [4,3,2,38,4,3,2,1,4,3,2,1]
l2 = [4,3,2,38,4,3,2,1,4,3,2,1]

k2 = 0
def stooge_sort2(l, s, e):
    global k2
    k2 += 1
    if s + 1 >= e:
        if l[s] > l[e]:
            l[s], l[e] = l[e], l[s]
        return
    r = s+2*(e-s+1)/3
    r = r - 1 if r == e else r
    stooge_sort2(l, s, r)
    stooge_sort2(l, s+(e-s+1)/3, e)
    stooge_sort2(l, s, r)

k1 = 0
def stooge_sort1(l, s, e):
    global k1
    k1 += 1
    if l[s] > l[e]:
        l[s], l[e] = l[e], l[s]
    if s + 1 >= e:
        return
    r = s+2*(e-s+1)/3
    r = r - 1 if r == e else r
    stooge_sort1(l, s, r)
    stooge_sort1(l, s+(e-s+1)/3, e)
    stooge_sort1(l, s, r)

stooge_sort1(l1, 0, len(l1) - 1)
stooge_sort2(l2, 0, len(l2) - 1)
if l1 != l2:
    print 'error'
else:
    print l1
    print '1:', k1
    print '2:', k2
