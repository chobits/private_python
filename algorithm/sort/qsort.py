#!/usr/bin/python

import sys
import random

size = 10

if len(sys.argv) > 2:
    print 'Usage: %s [array size]' %sys.argv[0]
    sys.exit(-1)
elif len(sys.argv) == 2:
    size = int(sys.argv[1])
    if size < 1 or size > 20:
        print 'size %d is invalid' %size
        sys.exit(-1)

nswap = 0
nrecursion = 0

def qsort_simple(L):
    if len(L) <= 1:
        return L
    return qsort([lt for lt in L[1:] if lt < L[0]]) + L[0:1] + \
           qsort([ge for ge in L[1:] if ge >= L[0]])

def split(L, p, q):
    global nswap
    direct = 1
    while p != q:
        # [p]  <=q
        if direct == 1:
            if L[p] > L[q]:
                L[p], L[q] = L[q], L[p]
                nswap += 1
                p += 1
                direct = -1
            else:
                q -= 1
        # p=>  [q]
        else:
            if L[p] > L[q]:
                L[p], L[q] = L[q], L[p]
                nswap += 1
                q -= 1
                direct = 1
            else:
                p += 1
    return p                

def qsort_highperformance_nogenlist(L, start, end):
    global nrecursion
    nrecursion += 1
    if start < end:
        index = split(L, start, end)
        print 'split:', index
        qsort_highperformance_nogenlist(L, start, index - 1)
        qsort_highperformance_nogenlist(L, index + 1, end)

# ascending order
def qsort_highperformance(L):
    qsort_highperformance_nogenlist(L, 0, len(L) - 1)

def generate_list(size):
    return [ random.randint(-size / 2, size / 2) for i in range(0, size) ]

L = generate_list(size)
print L
qsort_highperformance(L)
print L
print 'recursion: %d, swap: %d' %(nrecursion, nswap)
