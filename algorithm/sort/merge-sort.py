#!/usr/bin/python
from clock_decorator import print_time

def merge(l, ls, le, rs, re):
    r = []
    j = rs
    i = ls
    while i <= le:
        if j <= re and l[j] < l[i]:
            r.append(l[j])
            j += 1
        else:
            r.append(l[i])
            i += 1
    while j <= re:
        r.append(l[j])
        j += 1
    for (i, x) in enumerate(r):
        l[ls + i] = x

@print_time(1)
def merge_sort(l, i, j):
    if i == j:
        return
    left_end = (i + j) / 2
    merge_sort(l, i, left_end)
    merge_sort(l, left_end + 1, j)
    merge(l, i, left_end, left_end + 1, j)

# Merge_sort cannot be optimal to tail recursion!
# Not tail recursion, its tail is merge()

#l = [1, 9, 9, 0, 0, 7, 2, 2]
l = range(1000)
#print l
merge_sort(l, 0, len(l) - 1)
#print l
