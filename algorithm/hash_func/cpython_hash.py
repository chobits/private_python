

i = 10 #
bitmap = [0] * (1 << i)
j = 10000000
perturb = j
j = j % (1 << i)
PERTURB_SHIFT = 1
for k in range(1 << 10):
    perturb >>= PERTURB_SHIFT
    j = ((5 * j) + 1 + perturb) % (1 << i)
    if bitmap[j]:
        print 'Duplicate hash key:', j
    bitmap[j] = 1
