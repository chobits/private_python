#!/usr/bin/python

# if list root index is 0
#   left(i) = 2i+1
#   right(i) = 2i+2
#   parent(i) = (i-1)/2
# if list root index is 1
#   left(i) = 2i
#   right(i) = 2i+1
#   parent(i) = i/2

# Time: O(lgn)
def max_heapify(A, i, heapsize):
    left = i * 2 + 1    # left child index
    right = left + 1    # right child index
    # get largest entry index from parent, right child, right child
    largest = i
    if left < heapsize and A[i] < A[left]:
        largest = left
    if right < heapsize and A[largest] < A[right]:
        largest = right
    if largest != i:
        A[largest], A[i] = A[i], A[largest]
        max_heapify(A, largest, heapsize)

# tail call optimization
def max_heapify_tail(A, i, heapsize):
    while True:
        left = i * 2 + 1    # left child index
        right = left + 1    # right child index
        # get largest entry index from parent, right child, right child
        largest = i
        if left < heapsize and A[i] < A[left]:
            largest = left
        if right < heapsize and A[largest] < A[right]:
            largest = right
        if largest == i:
            break
        # move larger child up
        A[largest], A[i] = A[i], A[largest]
        i = largest

# Time: ?
# recursion implementation
def build_max_heapr(A, heapsize, i = 0):
    if i < heapsize:
        build_max_heapr(A, heapsize, i * 2 + 1)
        build_max_heapr(A, heapsize, i * 2 + 2)
        max_heapify_tail(A, i, heapsize)

# Time: O(n)
# for anilysis, see CRLS charpter 6.3
def build_max_heap(A, heapsize):
    for i in xrange(heapsize / 2 - 1, -1, -1):
        max_heapify_tail(A, i, heapsize)

def test(l, build = build_max_heap):
    print 'orignal heap:', l
    build(l, len(l))
    print 'built heap  :', l

if __name__ == '__main__':
    test([1, 9, 9, 0, 0, 7, 2, 2])
    test([1, 9, 9, 0, 0, 7, 2, 2], build_max_heapr)
