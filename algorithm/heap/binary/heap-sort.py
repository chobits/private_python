#!/usr/bin/python
from max_heap import build_max_heap, max_heapify

# Time: O(nlgn)
def heap_sort(l):
    build_max_heap(l, len(l))           # O(n)
    for i in xrange(len(l) - 1, 0, -1): # n - 1
        l[0], l[i] = l[i], l[0]
        max_heapify(l, 0, i)            # O(lgn), n = i
            
def test(l):
    print 'orig:  ', l
    heap_sort(l)
    print 'sorted:', l

if __name__ == '__main__':
    test([1,9,9,0,7,2,2])
    test([1])
    test([1, 2])
    test([2, 1])
    test(range(20,0,-1))
