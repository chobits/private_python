#!/usr/bin/python
from heapq import merge as k_merge
from time import time
from itertools import islice

# k way merge sort
# if k = 1, infinite recursion
# if k = 2, it turns out merge sort
# if k = len(l), it turns out heap sort
# return iterable object
def k_merge_sort(l, s, e, k):
    assert k > 1
    if s < e:
        klen = (e - s + k) / k
        ll = [k_merge_sort(l, s + i * klen, min(s + (i + 1) * klen - 1, e), k)
              for i in xrange(k)]
        return k_merge(*ll)
    return islice(l, s, e + 1)

if __name__ == '__main__':
    l = [1, 9, 9, 0, 0, 7, 2, 2]
    l = range(1000, 0, -1)
    n = len(l)
    for k in xrange(2, n + 1, 99):
        old = time()
        k_merge_sort(l, 0, n - 1, k)
        print '%d way merge sort on %d values costs %.3fs' %(k, n, time() - old)
