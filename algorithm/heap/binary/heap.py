#!/usr/bin/python
import max_heap

class heap:
    def __init__(self, l):
        self.heap_list = list(l)
        max_heap.build_max_heap(self.heap_list, len(self.heap_list))

    def decrease_key(self, i, key):
        if i >= len(self.heap_list):
            raise IndexError('heap index out of range')
        self.heap_list[i] = key
        max_heap.max_heapify(self.heap_list, i, len(self.heap_list))

    def increase_key(self, i, key):         # like heapq._siftdown()
        if i >= len(self.heap_list):
            raise IndexError('heap index out of range')
        # follow the path to the root, moving parents down
        # until finding a place key fits!
        self.heap_list[i] = key
        while i > 0:
            p = (i - 1) / 2     # parent index
            if self.heap_list[p] >= self.heap_list[i]:
                break
            self.heap_list[p] ,self.heap_list[i] = self.heap_list[i] ,self.heap_list[p]
            i = p

    def insert(self, key):
        self.heap_list.append(None)
        self.increase_key(len(self.heap_list) - 1, key)

    def extract_max(self):
        if len(self.heap_list) > 1:
            max_key = self.heap_list[0]
            self.heap_list[0] = self.heap_list.pop()
            max_heap.max_heapify(self.heap_list, 0, len(self.heap_list))
            return max_key
        elif len(self.heap_list) == 1:
            return self.heap_list.pop()
        raise IndexError('extract_max from empty heap')

    def delete(self, i):
        if i < 0:
            i %= len(self.heap_list)
        if i >= len(self.heap_list):
            raise IndexError('heap index out of range')
        elif i == len(self.heap_list) - 1:
            self.heap_list.pop()
        else:
            orig = self.heap_list[i]
            self.heap_list[i] = self.heap_list.pop()
            if self.heap_list[i] < orig:
                max_heap.max_heapify(self.heap_list, i, len(self.heap_list))
            else:
                self.increase_key(i, self.heap_list[i])

    def maximum(self):
        if self.heap_list:
            return self.heap_list[0]
        raise IndexError('heap is empty')

    def __repr__(self):
        return repr(self.heap_list)

    def __eq__(self, h):
        return h.__class__ == heap and self.heap_list == h.heap_list

    # return sorted list and clear orignal heap list
    def sort(self):         # O(nlgn), NOTE: build heap time is O(n), ignored.
        sort_list = []
        while self.heap_list:
            sort_list.append(self.extract_max())
        return sort_list

if __name__ == '__main__':
    h = heap(range(10))
    print h
    h.insert(1)
    print h
