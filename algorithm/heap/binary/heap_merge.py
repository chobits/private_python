#!/usr/bin/python
# for CLRS exercise 6.5-8
from heap import heap

# ll = [l1, l2 ..li.. lk]
# li = [x1,x2,x3,x4..] x1 >= x2 >= ...    
# O(nlgk), k = len(ll), n = sum([len(l) for l in ll])
def heap_merge(*ll):
    h = heap([[liter.next(), liter.next] for liter in map(iter, filter(lambda l:l, ll))])    # O(k), build max heap
    r = []
    while True:                     # O(n - k)
        try:
            m = h.maximum()         # maybe IndexError
            r.append(m[0])
            m[0] = m[1]()           # maybe StopIteration
            h.decrease_key(0, m)    # O(lgk) (O(lg(k-1)), .., lg1)
        except StopIteration:
            h.delete(0)
        except IndexError:
            break
    return r

if __name__ == '__main__':
    print heap_merge([7,5,3,1], [8,4,2,0], [20,15,10,5], [], [25])
    print heap_merge([2]*10, [2]*10, [2]*10)
