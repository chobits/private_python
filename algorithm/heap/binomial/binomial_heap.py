#!/usr/bin/python
import sys

# binomial tree node
class node:
    def __init__(self, key, degree=0):
        self.key = key
        self.degree = degree
        self.parent = None
        self.child = None
        self.sibling = None

    # dont traverse sibling of self
    def traverse(self):
        def _traverse(n):
            while n:
                print n.key
                _traverse(n.child)
                n = n.sibling
        print self.key
        _traverse(self.child)

    def search(self, key):
        if self.key == key:
            return self
        r = None
        if self.child: r = self.child.search(key)
        if not r and self.sibling: r = self.sibling.search(key)
        return r

    # WARNING: destroy this tree and return rerversed tree.
    def reverse(self):
        prev = None
        x = self
        while x:
            x.sibling, prev, x = prev, x, x.sibling
        return prev

    def __repr__(self):
        return '(%s)' %self.key

    def number(self):
        def _number(n):
            r = 0
            while n:
                r += n.degree + _number(n.child)
                n = n.sibling
            return r
        return 1 + self.degree + _number(self.child)

class heap:
    root = None

    # Time: O(lgn)
    def min_pos(self):
        prev = None
        min_prev = None
        min_node = x = self.root
        min_key = 99999999    # infinite large
        while x:
            if min_key > x.key:
                min_key = x.key
                min_node = x
                min_prev = prev
            prev, x = x, x.sibling
        return min_prev, min_node

    def minimum(self):
        return self.min_pos()[1]

    def extract_min(self):
        min_prev, min_node = self.min_pos()
        if not min_node: raise ValueError('extract_min(): empty heap')
        # delete min_node from heap list
        if not min_prev: self.root = min_node.sibling
        else: min_prev.sibling = min_node.sibling
        # reverse min_node.child list
        if min_node.child:
            min_node.child.parent = None
            # merge
            self.root = union_merge(self.root, min_node.child.reverse())

    # x must be node of this heap
    def decrease_key(self, x, key):
        if x.key <= key:
            raise ValueError('decrease_key(): key >= node::key')
        # bubble up
        while x.parent and x.parent.key > key:
            x.key = x.parent.key
            x = x.parent
        x.key = key

    # x must be node of this heap
    def delete(self, x):
        self.decrease_key(x, -999999)
        self.extract_min()

    # Time: O(n)
    def search(self, key):
        x = self.root
        n = None
        while x:
            n = x.search(key)
            if n:
                break
            x = x.sibling
        return n

    def delete_key(self, key):
        n = self.search(key)
        if n:
            self.delete(n)
        else:
            raise ValueError('delete_key():', key, 'is not in heap')

    def insert(self, key):
        n = node(key)
        # insert new node(tree) into head
        if self.root: n.sibling = self.root
        self.root = n
        union_merge_dup(self)

    # for CLRS exercise 19.2-8
    def insert2(self, key):
        x = node(key)
        prev = None
        # insert new node into head
        if self.root: x.sibling = self.root
        prev = None
        self.root = x
        # start to merge dup degree nodes
        prev = None
        while x and x.sibling and x.degree == x.sibling.degree:
            x = union_link(x, x.sibling)
            if prev: prev.sibling = x
            else: self.root = x

    def traverse(self):
        n = self.root
        while n:
            print '%d: ' %n.degree
            n.traverse()
            n = n.sibling

    # return number of all elements
    def number(self):
        n = self.root
        r = 0
        while n:
            r += n.number()
            n = n.sibling
        return r


# link tree x(Bk-1) and tree y(Bk-1) to new tree(Bk)
# new tree root is minimum root of x and y
# Time: O(1)
# prev-->x-->y-->next  => prev ? (xy)--> next, you should link prev and (xy)
# prev-->x-->next y    => prev ? (xy) ?  next
def union_link(x, y):
    assert x.degree == y.degree
    if x.parent: print x.parent
    assert not x.parent
    if y.parent: print y.parent
    assert not y.parent
    if x.key < y.key:
        x, y = y, x
        y.sibling = x.sibling
    x.parent = y
    x.sibling = y.child
    y.child = x
    y.degree += 1
    return y

# O(lgn), n = elements number of tree x and tree y
def union_merge(x, y):
    if not x or not y: return x if x else y
    if x.degree > y.degree: x, y = y, x
    # merge y into x
    head = tail = x
    while tail.sibling and y:               # y > [head, tail]
        if tail.sibling.degree > y.degree:  # y < tail::next
            tail.sibling, y.sibling, tail, y = y, tail.sibling, y, y.sibling
        else:
            tail = tail.sibling
    if y: tail.sibling = y
    return head

def union_merge_dup(h):
    prev, x, next = None, h.root, h.root.sibling
    while next:
        if x.degree == next.degree and (not next.sibling or x.degree != next.sibling.degree):
            x = union_link(x, next)     # save next.sibling, not prev
            if prev:
                prev.sibling = x
            else:
                h.root = x
            next = x.sibling
        else:
            prev, x, next = x, next, next.sibling

# heap union
def union(ha, hb):
    # step 1, using ha as new heap
    ha.root = union_merge(ha.root, hb.root)
    # step 2
    union_merge_dup(ha)
    return ha

def test1(h=None, n=1000):
    if not h: h = heap()
    for x in xrange(n): h.insert2(x)
    for x in xrange(n): h.extract_min()
    h.traverse()

def test2(h=None, n=1000):
    if not h: h = heap()
    for x in xrange(n):
        if x % 2 == 0:
            h.insert(x)
        else:
            h.insert2(x)
    for x in xrange(n - 1, -1, -1):
        h.delete_key(x)
    h.traverse()

# prove time of insert2-create heap(n) is O(n)
def test3(h=None, n=1000):
    if not h: h = heap()
    from time import time
    old = time()
    for x in xrange(n):
        h.insert2(x)
    during = time() - old
    print '%d inserts: %.4f ms rate: %.2f' %(n, during, 1000000*during/n)

if __name__ == '__main__':
    test3(n=1000)
    test3(n=100000)
