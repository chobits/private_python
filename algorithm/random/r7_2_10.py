#!/usr/bin/python
import random

# return 1~7 natrue number
def rand7():
    return random.randint(1, 7)

def rand01():
    while 1:
        r = rand7()
        if r < 4: return 0
        if r > 4: return 1

# return 1~5
def rand5():
    while 1:
        r = rand7()
        if r <= 5:
            return r

# return 1~10
def rand10():
    return rand01() * 5 + rand5()

def rand10_iter(n):
    delta = 0
    while n > 0:
        r = rand7()
        if r <= 5:
            n -= 1
            yield r + delta
        else:
            delta = {6:5, 7:0}[r]

def test_rand10():
    times = [0] * 11
    for i in xrange(100000):
        times[rand10()] += 1
    print times

def test_rand10_iter():
    times = [0] * 11
    for i in rand10_iter(100000):
        times[i] += 1
    print times

if __name__ == '__main__':
    test_rand10_iter()

