#!/usr/bin/python
import sys

# O(nlogn)
def lis(l, s, e, left = 1):
    if s == e:
        return (s, e)
    elif s > e:
        print 'Fuck'
        sys.exit(0)
    mid = s + (e - s + 1) / 2
    # O(n)
    for ms in xrange(mid, s, -1):
        if l[ms - 1] >= l[ms]:
            ms += 1
            break
    ms = ms - 1
    for me in xrange(mid - 1, e):
        if l[me] >= l[me + 1]:
            me -= 1
            break
    me = me + 1
    # 2T(n/2)
    (rs, re) = lis(l, s, mid - 1, 0)
    (ls, le) = lis(l, mid, e)
    if (me - ms > re - rs) and (me - ms > le - ls):
        return (ms, me)
    if re == mid - 1 and ls == mid and l[re] < l[ls]:
        return (rs, le)
    if (re - rs > le - ls) or (left == 1 and re - rs == le - ls):
        return (rs, re)
    else:
        return (ls, le)

def test(l):
    print lis(l, 0, len(l) - 1),
    print l

test([1,2,3,4])
test([1])
test([1,0])
test([1,2,3,4,5,6,1,3,4,5,6,7,8,9,0,1,2])
