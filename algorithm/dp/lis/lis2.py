#!/usr/bin/python
# for CRLS exercise 15.4-5
# time: O(n)
# longest increasing sequence

def lis(l):
    lis_start = 0
    lis_len = 1
    length = 1
    l.append(-99999999)     # append fake infinitesimal
    for i in xrange(1, len(l)):
        if l[i - 1] >= l[i]:
            if length > lis_len:
                lis_len = length
                lis_start = i - lis_len
                length = 0
        length += 1
    l.pop()
    return (lis_start, lis_start + lis_len - 1)

l = [1, 5, 3]
print l
print lis(l)
