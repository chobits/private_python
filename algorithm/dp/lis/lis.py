#!/usr/bin/python
# for CRLS exercise 15.4-5
# time: O(n)
# longest increasing sequence

# (*) lis always return right-part sequence
# e.g:
# [1,2,1,2] return (2,3) not (0, 1)
def lis(l, start, end):
    if start == end:
        return (start, end)
    if l[end - 1] >= l[end]:
        return lis(l, start, end - 1)
    else:
        # (s, e) must be the right-part sequence
        # of all equal length longest increasing sequnces
        (s, e) = lis(l, start, end - 1)
        if e == end - 1:
            return (s, end)
        else:
            if e - s >= 2:   # make (*) work
                return (s, e)
            else:
                return (end - 1, end)

l = [3,4,5,6,7, 2, 1, 1, 2, 1, 2, 3]
print l
print lis(l, 0, len(l) - 1)
