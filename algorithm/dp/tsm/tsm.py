#!/usr/bin/python
# for CRLS thinking question 15-1
# bitonic traveling-salesman
# recursion implementation

import time 

# Optimal Cache
def memo(f):
    def wrap(P, i, j):
        global cache
        if cache[i][j] == -1:
            cache[i][j] = f(P, i, j)
        return cache[i][j]
    return wrap

def distance(P, i, j):
    xval = P[i][0] - P[j][0]
    yval = P[i][1] - P[j][1]
    return (xval**2+yval**2)**0.5

# start from 0 to j
# P[i]<->P[j]
@memo
def btsm(P, i, j):
    if i == 0:
        return sum([distance(P, k, k + 1) for k in xrange(j)]) + distance(P, 0, j)
    if i == j:
        return min([btsm(P, k, j) for k in xrange(i - 1)])
    if i == j - 1:
        return min([btsm(P, k, j) for k in xrange(i)])
    return btsm(P, i, j - 1) + distance(P, j - 1, j) + distance(P, i, j) - distance(P, i, j - 1)

#P = [(0, 0), (1, 6), (2, 3), (5, 2), (6, 5), (7, 1), (8, 4)]
P = [(i, i & 1) for i in xrange(400)]

# create cache
n = len(P)
cache = [[-1] * n for _ in xrange(n)]

# output
old = time.time()
print btsm(P, n - 1, n - 1)
print 'it costs %.4fs' %(time.time() - old)
