#!/usr/bin/python
# for CRLS thinking question 15-1
# bitonic traveling-salesman
# bottom-up implementation
import time

def distance(P, i, j):
    xval = P[i][0] - P[j][0]
    yval = P[i][1] - P[j][1]
    return (xval**2+yval**2)**0.5

# start from 0 to j
# P[i]<->P[j]
# O(n^3)
def btsm(P):
    n = len(P)
    btsm_len = [[11] * n for _ in xrange(n)]
    for j in xrange(n):
        btsm_len[0][j] = sum([distance(P, k, k + 1) for k in xrange(j)], distance(P, 0, j))
    for i in xrange(1, n):                                                # O(n)
        btsm_len[i][i] = btsm_len[i - 1][i]
        for j in xrange(i + 1, n):                                        # O(n)
            if i == j - 1:
                btsm_len[i][j] = min([btsm_len[k][j] for k in xrange(i)]) # O(n)
            else:
                btsm_len[i][j] = btsm_len[i][j - 1] - distance(P, i, j - 1)\
                               + distance(P, i, j) + distance(P, j - 1, j)
    return btsm_len[n - 1][n - 1]

# init
P = [(0, 0), (1, 6), (2, 3), (5, 2), (6, 5), (7, 1), (8, 4)]
#P = [(i, i & 1) for i in xrange(400)]

# output
old = time.time()
print btsm(P)
print 'it costs %.4fs' %(time.time() - old)
