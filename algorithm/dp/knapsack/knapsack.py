#!/usr/bin/python
# for CRLS exercise 16-2
# knapsack problem
# dynamic programming implementation
WEIGHT = 0
VALUE = 1

# space: O(nw)
def memo(f):
    def wrap(l, n, w):
        if cache[n][w] == None:
            cache[n][w] = f(l, n, w)
        return cache[n][w]
    return wrap

@memo
# return max value with <=w weight
# time: O(nw)
def knapsack(l, n, w):
    if n == 0: 
        return 0
    return max(knapsack(l, n - 1, w),\
               knapsack(l, n - 1, w - l[n - 1][WEIGHT]) + l[n - 1][VALUE]\
               if w >= l[n - 1][WEIGHT] else 0)

# [(weight, value), ...]
l = [(10, 60), (20, 100), (30, 120)]
n = len(l)
w = 50
# cache
cache = [[None] * (w + 1) for _ in xrange(n + 1)]

print knapsack(l, n, w)
