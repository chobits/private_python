#!/usr/bin/python
# for CRLS exercise 15.4-4
# longest common subsequence

# Space O(2*len2)
# Time  O(mn)
# m is len1, n is len2
def lcs_length1(l1, l2):
    len1 = len(l1) + 1
    len2 = len(l2) + 1
    # make len2 smaller
    if len2 > len1:
        len1, len2 = len2, len1
        l1, l2 = l2, l1
    c = [[0] * len2 for _ in xrange(2)]
    for i in xrange(1, len1):
        for j in xrange(1, len2):
            if l1[i - 1] == l2[j - 1]:
                c[1][j] = c[0][j - 1] + 1
            else:
                c[1][j] = max(c[0][j], c[1][j - 1])
        c[0] = c[1]
        c[1] = [0] * len2
    return c[0][len2 - 1]

# Space O(len2)
# Time  O(mn)
# m is len1, n is len2
def lcs_length2(l1, l2):
    len1 = len(l1) + 1
    len2 = len(l2) + 1
    # make len2 smaller
    if len2 > len1:
        len1, len2 = len2, len1
        l1, l2 = l2, l1
    c = [0] * len2
    for i in xrange(1, len1):
        bak = 0
        for j in xrange(1, len2):
#            print c[j],
            if l1[i - 1] == l2[j - 1]:
                bak, c[j] = c[j], bak + 1
            else:
                bak, c[j] = c[j], max(c[j], c[j - 1])
#        print
#    print ''.join(['%d ' %i for i in c[1:]])
    return c[len2 - 1]

print lcs_length1('10010101', '010110110')
print lcs_length2('10010101', '010110110')
