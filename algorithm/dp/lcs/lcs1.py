#!/usr/bin/python
# longest common subsequence

# Space O(mn)
# Time  O(mn)
# m is len1, n is len2
def lcs_length(l1, l2):
    len1 = len(l1) + 1
    len2 = len(l2) + 1
    c = [[0]*len2 for _ in xrange(len1)]  # len1 * len2 matrix
    for i in xrange(1, len1):
        for j in xrange(1, len2):
            if l1[i - 1] == l2[j - 1]:
                c[i][j] = c[i - 1][j - 1] + 1
            else:
                c[i][j] = max(c[i - 1][j], c[i][j - 1])
    return c

def lcs(l1, l2):
    c = lcs_length(l1, l2)
    i, j = len(l1), len(l2)
    lcslen = c[i][j]
    lcsl = []
    while lcslen > 0:
        if l1[i - 1] == l2[j - 1]:
            lcsl.insert(0, l1[i - 1])
            i -= 1
            j -= 1
            lcslen -= 1
        elif c[i][j - 1] > c[i - 1][j]:
            j -= 1
        else:
            i -= 1
    return lcsl

print lcs('ABCBDAB', 'BDCABA')
print lcs('10010101', '010110110')
