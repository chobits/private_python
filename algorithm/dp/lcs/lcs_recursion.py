#!/usr/bin/python
# for CRLS exercise 15.4-2

def memo(f):
    cache = {}
    def wrap(l1, i, l2, j):
        if not cache.has_key((l1, l2)):
            c = [[-1] * (len(l2) + 1) for _ in xrange(len(l1) + 1)]
            for i in xrange(len(l1) + 1):
                c[i][0] = 0
            for j in xrange(len(l2) + 1):
                c[0][j] = 0
            cache[(l1, l2)] = c
        c = cache[(l1, l2)]
        if c[i][j] == -1:
            r = f(l1, i, l2, j)
            c[i][j] = r
        return c[i][j]
    return wrap

@memo
def lcs_recursion(l1, i, l2, j):
    if l1[i - 1] == l2[j - 1]:
        return lcs_recursion(l1, i - 1, l2, j - 1) + 1
    else:
        return max(lcs_recursion(l1, i - 1, l2, j), lcs_recursion(l1, i, l2, j - 1))


def lcs_length(l1, l2):
    return lcs_recursion(l1, len(l1), l2, len(l2))

print lcs_length('hello', 'hell')
print lcs_length('ABCBDAB', 'BDCABA')
print lcs_length('10010101', '010110110')
