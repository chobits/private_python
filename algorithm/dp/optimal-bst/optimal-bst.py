#!/usr/bin/python
# for CRLS chapter 15.5
from itertools import islice

def print_root(root, s, e, isroot = 0):
    r = root[s][e]
    if isroot:
        print 'k%d is root' %r
    if s == e:
        print 'd%d is left child of k%d' %(r - 1, r)
        print 'd%d is right child of k%d' %(r, r)
    else:
        if s == r:
            print 'd%d is left child of k%d' %(s-1, r)
            print '%s is right child of k%d' %(print_root(root, r + 1, e), r)
        elif e == r:
            print '%s is left child of k%d' %(print_root(root, s, r - 1), r)
            print 'd%d is right child of k%d' %(e, r)
        else:
            print '%s is left child of k%d' %(print_root(root, s, r - 1), r)
            print '%s is right child of k%d' %(print_root(root, r + 1, e), r)
    return 'k%d'%r


#    0  1  ... n
# P: ?  p1 ... pn
# Q: q0 q1 ... pn
def optimal_bst(P, Q):
    assert len(P) == len(Q)
    n = len(P)
    w = [[0] * n for _ in xrange(n)]
    for i in xrange(1, n):
        for j in xrange(i, n):
            w[i][j] = sum(islice(P, i, j+1)) + sum(islice(Q, i - 1, j+1))
    e = [[0] * n for _ in xrange(n+1)]
    root = [[0] * n for _ in xrange(n)]
    for i in xrange(1, n+1):
        e[i][i - 1] = Q[i - 1]
    for i in reversed(xrange(1, n)):
        for j in xrange(i, n):
            e[i][j] = 99999
            for r in xrange(i, j + 1):
                # e[i, j] =_/ Q[i-1] if i-1=j
                #           \ min([e[i, r-1]+e[i, r+1]+w[i, j] for r=i->j])
                if e[i][j] > e[i][r-1]+ e[r+1][j]+ w[i][j]:
                    # (i,i-1) (i, i) (i, i+1) .. (i, j)
                    # (i+1,j) (i+2, j) .. (j+1, j)
                    e[i][j] = e[i][r-1]+e[r+1][j]+w[i][j]
                    root[i][j] = r
    print 'final expectation:', e[1][n - 1]
    print_root(root, 1, n - 1, 1)


#P = [0, 15, 10, 5, 10, 20]
#Q = [5, 10, 5, 5, 5, 10]
#optimal_bst(P, Q)
P = [0, 4, 6, 8, 2, 10, 12, 14]
Q = [6, 6, 6, 6, 5, 5, 5, 5]
optimal_bst(P, Q)
