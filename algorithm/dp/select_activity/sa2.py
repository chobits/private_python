#!/usr/bin/python
# for CRLS charpter 16.1
# dynamic programming implementation
import sys
from itertools import islice

START = 0
FINAL = 1

def memo(f):
    def wrap(t, i, j):
        if cache[i][j] == None:
            cache[i][j] = f(t, i, j)
        return cache[i][j]
    return wrap

@memo
def maxset(t, i, j):
    if i >= j:
        return []
    i_f = t[i][FINAL]
    j_s = t[j][START]
    S = [maxset(t, i, k) + [t[k]] + maxset(t, k, j)\
            for k in range(i + 1, j)\
            if t[k][START] >= i_f and t[k][FINAL] <= j_s]
    if not S:
        return []
    return max(S, key = len)

# finish time increasing order
# test activity (start, finish) time
#time_set =  [(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9)]
#time_set = [(i, 2*i) for i in xrange(400)]
time_set =  [(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9), (6, 10), (8, 11), (8, 12), (2, 13), (12, 14)]

# cache
time_set.insert(0, (-2, -1))              # senial first
time_set.append((999999998, 999999999)) # senial final
n = len(time_set)
cache = [[None] * n for _ in xrange(n)]

# output
mset = maxset(time_set, 0, n - 1)
iset = ['a%d' %time_set.index(x) for x in mset]
print '{', ', '.join(iset), '}'
