#!/usr/bin/python
# for CRLS charpter 16.1
# dynamic programming implementation
import sys
from itertools import islice

START = 0
FINAL = 1

def memo(f):
    def wrap(t, length):
        if cache[length] == None:
            cache[length] = f(t, length)
        return cache[length]
    return wrap

@memo
def maxset(t, length):
    if length == 0:
        return []
    elif length == 1:
        return [t[0]]
    # if last not in maxset(t, length)
    set1 = maxset(t, length - 1)
    # if last in maxset(t, length)
    last = t[length - 1]
    set2 = [last]
    last_start = last[START]
    # maybe optimal to binary search
    for (i, (start, final)) in enumerate(islice(t, 0, length)):
        if final > last_start:
            set2 = maxset(t, i) + set2
            break
    return max(set1, set2, key = len)

# finish time increasing order
# test activity (start, finish) time
#time_set =  [(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9)]
time_set = [(i, 2*i) for i in xrange(400)]
#time_set =  [(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9), (6, 10), (8, 11), (8, 12), (2, 13), (12, 14)]
cache = [None] * (len(time_set) + 1)

# output
mset = maxset(time_set, len(time_set))
iset = ['a%d' %(time_set.index(x) + 1) for x in mset]
print '{', ', '.join(iset), '}'
