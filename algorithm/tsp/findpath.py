#!/usr/bin/python
import sys
from lib import *

def nsteps(matrix, start, end, steps):
    global fail
    # auxilary functions
    def flag_path((x, y), matrix):
        orig = matrix[x][y]
        matrix[x][y] = THROUGH
        return orig

    def unflag_path(orig, (x, y), matrix):
        matrix[x][y] = orig

    def adjacent_paths((x, y)):
        return [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
    # get final?
    if start == end:
        if steps == 0:
            return [start]
        return None
    # recursion traverse
    for apath in adjacent_paths(start):
        if matrix[apath[0]][apath[1]] != ROAD:
            continue
        orig = flag_path(apath, matrix)
        # recursive traversal to (steps - 1) length path
        path = nsteps(matrix, apath, end, steps - 1)
        unflag_path(orig, apath, matrix)
        if path:
            path.insert(0, start)
            return path
        fail += 1
    return None

# parse args
if len(sys.argv) < 3:
    print 'Usage: %s height width' %sys.argv[0]
    sys.exit(-1)

height = int(sys.argv[1])
width = int(sys.argv[2])
matrix = alloc_matrix(width, height)

# run
fail = 0
matrix[0][0] = THROUGH
path = nsteps(matrix, (0, 0), (1, 0), width * height - 1)
display(matrix, path)
print 'fail times:', fail
