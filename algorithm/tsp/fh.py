#!/usr/bin/python
import sys
import time
from lib import *

def nsteps(matrix, start, end, steps):
    global fail
    # auxilary functions
    def flag_path((x, y), matrix):
        orig = matrix[x][y]
        matrix[x][y] = THROUGH
        return orig

    def unflag_path(orig, (x, y), matrix):
        matrix[x][y] = orig

    # select the farthest node
    def adjacent_paths((x, y)):
        def distance((sx, sy), (ex, ey)):
            return abs(sx - ex)**2 + abs(sy - ey)**2
        def lcmp(a, b):
            return distance(b, end) - distance(a, end)
        l = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
        l.sort(lcmp)
        return l
    # get final?
    if start == end:
        if steps == 0:
            return [start]
        return None
    # recursion traverse
    for apath in adjacent_paths(start):
        if matrix[apath[0]][apath[1]] != ROAD:
            continue
        orig = flag_path(apath, matrix)
        # recursive traversal to (steps - 1) length path
        path = nsteps(matrix, apath, end, steps - 1)
        unflag_path(orig, apath, matrix)
        if path:
            path.insert(0, start)
            return path
        fail += 1
    return None

# parse args
if len(sys.argv) not in [3, 4]:
    print 'Usage: %s height width [barriers]' %sys.argv[0]
    sys.exit(-1)

# create grids(matrix)
height = int(sys.argv[1])
width = int(sys.argv[2])
matrix = alloc_matrix(width, height)
matrix[0][0] = THROUGH
fail = 0
barriers = 0

# set barriers
if len(sys.argv) == 4:
    barriers = int(sys.argv[3])
    set_barriers(matrix, barriers) 

# run
path = nsteps(matrix, (0, 0), (1, 0), width * height - barriers - 1)

# output result
display(matrix, path)
print 'fail times:', fail
