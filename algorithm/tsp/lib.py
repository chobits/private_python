import time
import os
import random

ROAD = ord(' ')
WALL = ord('#')
THROUGH = WALL
BALL = ord('O')

def display(matrix, path):
    if not path:
        return
    for (x, y) in path:
        matrix[x][y] = BALL
        os.system('clear')
        for row in matrix:
            print row
        time.sleep(0.05)

#  +- width -+ (y)
#  |
# height
#  |
#  +
# (x)
def alloc_matrix(width, height):
    matrix = [bytearray([ROAD] * width + [WALL]) for x in range(height + 1)]
    matrix[height] = bytearray([WALL] * (width + 1))
    return matrix

def set_barriers(matrix, n):
    height = len(matrix) - 1
    width = len(matrix[0]) - 1
    walls = []
    for i in range(n):
        while 1:
            x = random.randrange(height)
            y = random.randrange(width)
            if (x, y) not in walls:
                matrix[x][y] = WALL
                walls.append((x, y))
                break
    for row in matrix:
        print row

