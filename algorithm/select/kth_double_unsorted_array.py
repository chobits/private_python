import time
# two arrays are sorted
# O(lgn)
def get_kth_double_array(k, a1, a2):
    s1, e1 = 0, len(a1) - 1
    s2, e2 = 0, len(a2) - 1
    while True:
        if k == 1:
            return min(a1[s1], a2[s2])
        if k == 2:
            return max(a1[s1], a2[s2])

        if s1 == e1:
            if s2 + k - 1 < len(a2) and a1[s1] > a2[s2 + k - 1]:
                return a2[s2 + k - 1]
            else:
                return max(a1[s1], a2[s2 + k - 2])

        if s2 == e2:
            if s1 + k - 1 < len(a1) and a2[s2] > a1[s1 + k - 1]:
                return a1[s1 + k - 1]
            else:
                return max(a2[s2], a1[s1 + k - 2])

        m1 = (s1 + e1 + 1) / 2
        m2 = (s2 + e2 + 1) / 2

        if a1[m1] == a2[m2]:
            if m1 - s1 + m2 - s2 >= k:
                e1 = m1 - 1
                e2 = m2 - 1
            else:
                k -= m1 - s1 + m2 - s2
                s1 = m1
                s2 = m2
        elif a1[m1] > a2[m2]:
            if m1 - s1 + m2 - s2 >= k:
                e1 = m1 - 1
            else:
                k -= m2 - s2
                s2 = m2
        else:
            if m1 - s1 + m2 - s2 >= k:
                e2 = m2 - 1
            else:
                k -= m1 - s1
                s1 = m1

# broute-force algorithm
# O(n)
def get_kth(k, a1, a2):
    s1, s2 = 0, 0
    r = 0
    for _ in range(k - 1):
        if s2 == len(a2):
            s1 += 1
        elif s1 == len(a1):
            s2 += 1
        elif a1[s1] < a2[s2]:
            s1 += 1
        else:
            s2 += 1
    if s2 == len(a2):
        return a1[s1]
    if s1 == len(a1):
        return a2[s2]
    return min(a1[s1], a2[s2])

def test(l1, l2):
    print [get_kth_double_array(k, l1, l2) for k in range(1, len(l1) + len(l2) + 1)]
    print [get_kth(k, l1, l2) for k in range(1, len(l1) + len(l2) + 1)]

def testf(f, l1, l2):
    l = [0] * (len(l1) + len(l2))
    t = time.time()
    for k in xrange(len(l1) + len(l2)):
        l[k] = f(k + 1, l1, l2)
    print '%s, %.6f ms' %(f.func_name, time.time() - t)
    return l

if __name__ == '__main__':
    n = 1000
    l1 = range(0, n, 2)
    l2 = range(1, n, 2)
    r1 = testf(get_kth, l1, l2)
    r2 = testf(get_kth_double_array, l1, l2)
    assert r1 == r2
