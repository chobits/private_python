#!/usr/bin/python
import random

# select the first one as pivor
def first_partition(A, s, e):
    i = s
    for j in range(s, e + 1):
        if A[j] < A[s]:
            i += 1
            A[i], A[j] = A[j], A[i]
    A[i], A[s] = A[s], A[i]
    return i

# O(e - s)
def random_partition(A, s, e):
    i = random.randint(s, e)
    A[i], A[s] = A[s], A[i]
    return first_partition(A, s, e)


# try to find ith minimux of unsorted array A[s, e]
#             (i rank)
# T(n) = T(n/2) + O(n) = O(n)
def random_select(A, s, e, i):                      # T(n), n = e - s + 1
    if s == e:
        return A[s]
    mid = random_partition(A, s, e)                 # O(n)
    k = mid - s + 1 # k = rank of mid
    if k == i:
        return A[mid]
    elif k > i:
        return random_select(A, s, mid, i)          # at most T(n/2)
    else:
        return random_select(A, mid + 1, e, i - k)  # at most T(n/2)

if __name__ == '__main__':
    A = list(reversed(range(1, 10)))
    i = 100
    print random_select(A, 0, len(A) - 1, min(i, len(A)))
