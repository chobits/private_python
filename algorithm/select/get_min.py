#!/usr/bin/python
import time

# binary search
# but the time complexity is O(n)
def get_min(l, start, end):
    length = end - start + 1
    if length == 1:
        return l[start]
    elif length == 2:
        return min(l[start], l[end])
    return min(get_min(l, start, start + length / 2 - 1),
               get_min(l, start + length / 2, end))

# create test list
l = range(3200000)
print 'list is created'
# our function
cur = time.time()
print get_min(l, 0, len(l) - 1)
print 'time:', time.time() - cur

# python builtin min function O(n)
cur = time.time()
print min(l)
print 'time:', time.time() - cur
