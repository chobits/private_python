from itertools import product
from pprint import pprint

def sum_row(g, row):
    return sum(g[row])

def sum_col(g, col):
    return sum([g[row][col] for row in range(len(g))])

# CRLS ex-22.1-6
# Time: O(V)
# find universal sink from adjacency matrix
# (universal sink is a vertex, in which case
#  its indegree is V-1 and outdegree is 0)
#
# if not found, return -1

def guess_universal_sink_state(g):
    vlen = len(g)
    # if vlen == 1: return -1 if g[0][0] else 0
    us = -1
    state2sink = {
        (0, 0, 0, 0):-1, (0, 0, 0, 1):-1, (0, 0, 1, 0):0, (0, 0, 1, 1):0,
        (0, 1, 0, 0):1, (0, 1, 0, 1):-1, (0, 1, 1, 0):-1, (0, 1, 1, 1):-1,
        (1, 0, 0, 0):-1, (1, 0, 0, 1):-1, (1, 0, 1, 0):-1, (1, 0, 1, 1):-1,
        (1, 1, 0, 0):1, (1, 1, 0, 1):-1, (1, 1, 1, 0):-1, (1, 1, 1, 1):-1
    }
    for w in range(vlen):
        if us == -1:
            us = w
        else:
            sink = state2sink[(g[us][us], g[us][w], g[w][us], g[w][w])]
            us = w if sink == 1 else us if sink == 0 else 0
    return us

def guess_universal_sink_cmp(g):
    vlen = len(g)
    us = 0
    for w in range(1, vlen):
        if g[us][us] or (g[w][w] == 0 and g[us][w]):    # 1 ? ? ? or  0 ? ? 0
                us = w
    return us

def verify_universal_sink(g, us):
    return us >= 0 and sum_row(g, us) + sum_col(g, us) == len(g) - 1

def find_universal_sink(g):
    us = guess_universal_sink_state(g)
    #us = guess_universal_sink_cmp(g)
    return us if verify_universal_sink(g, us) else -1

def list2matrix(lg):
    vlen = len(lg)
    mg = [[0 for _ in range(vlen)] for _ in range(vlen)]
    for (u, al) in enumerate(lg):
        for v in al:
            mg[u][v] = 1
    return mg

def matrix2list(mg):
    vlen = len(mg)
    return [[v for v in range(vlen) if mg[u][v]] for u in range(vlen)]

# O(V^2 + V + E) = O(V^2 + E) = O(V^2)
def equal_graph_ml(mg, lg):
    if len(mg) != len(lg): return False
    for (u, adjlist) in enumerate(lg):
        for v in adjlist:
            if mg[u][v] != 1:
                return False
    size_lg = sum([len(adjlist) for adjlist in lg])
    size_mg = sum([sum(row) for row in mg])
    return size_lg == size_mg

# CRLS ex-22.1-5 for adjacency matrix
def matrix_square(g):
    vlen = len(g)
    sg = [[0 for _ in range(vlen)] for _ in range(vlen)]
    for (u, v, w) in product(range(vlen), range(vlen), range(vlen)):
        if g[u][v] and g[v][w]:
            sg[u][w] = 1
    return sg

# CRLS ex-22.1-5 for adjacency list
# O(V+E) < T(n) < O((V+E)*V)
def list_square(g):
    vlen = len(g)
    sg = [[] for _ in range(vlen)]
    for (u, adjlist) in enumerate(g):
        for v in adjlist:               # O(V+E)
            sg[u].extend(g[v])          # O(len(g[v]))      0 < len(g[v]) < V
    return duplicate_removal(sg)

# CRLS ex-22.1-4
# duplicate removal for adjacency list
# using a hash set for one adjacency list every time
# O(V + E)
def duplicate_removal(g):
    return [list(set(adjlist)) for adjlist in g]

# CRLS ex-22.1-3
# edge transposition for adjacency list
# O(V+E)
def list_edge_transpose(g):
    tg = [[] for _ in range(len(g))]
    for (u, adjlist) in enumerate(g):
        for v in adjlist:
            tg[v].append(u)
    return tg

# edge transposition for adjacency matrix
# O(V^2)
def matrix_edge_transpose(g):
    vlen = len(g)
    tg = [[None for _ in range(vlen)] for _ in range(vlen)]
    for u in range(vlen):
        for v in range(vlen):
            tg[u][v] = g[v][u]
    return tg

if __name__ == '__main__':
#   lg = [[1,2], [3], [3], []]
#   mg = [[0, 1, 1, 0], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 0]]
#   assert equal_graph_ml(mg, lg)
#   assert equal_graph_ml(matrix_square(mg), list_square(lg))
#   pprint(matrix_square(mg))
#   pprint(list_square(lg))
#   pprint(lg)
#   pprint(list_edge_transpose(lg))
#   pprint(mg)
#   pprint(matrix_edge_transpose(mg))
#   lg = [[1,2,2,2,2,], [3], [3,3,3], []]
#   pprint(lg)
#   pprint(duplicate_removal(lg))
    mg = [[1,0,1,1], [0,0,1,0], [0,0,0,0], [1,1,1,1]]
    print find_universal_sink(mg)
