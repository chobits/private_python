# CRLS ex-22.2-8
from graph import list2matrix

def one_path_iter(g, mg, start, prev):
    path = [start]
    for u in g[start]:
        if prev[u] == -1:
            prev[u] = start
        elif mg[start][u]:
            path.extend([u, path[-1]])
        mg[start][u] = mg[u][start] = 0
    for u in g[start]:
        if prev[u] == start:
            path.extend(one_path_iter(g, mg, u, prev))
            path.append(start)
    return path

def one_path(g):
    prev = [-1] * len(g)
    prev[0] = -2
    return one_path_iter(g, list2matrix(g), 0, prev)

# Time: O(V + E)
# deepth first search
# divide-conquer: try start first(find all path start can reach),
#            then try others(from one of paths which start can reach and is state 0)
def one_path_iter2(g, start, state):
    path = [start]

    queue = []
    state[start] = 1
    for u in g[start]:              # not BFS, just find path around itself
        if state[u] == 0:
            queue.append(u)
        elif state[u] == 1:
            path.extend([u, start])
    state[start] = 2

    for u in queue:
        path.extend(one_path_iter2(g, u, state))
        path.append(start)
    return path

def one_path2(g):
    return one_path_iter2(g, 0, [0] * len(g))
 
# O(V + E)
# BFS + DFS
def one_path_iter3(g, start, state):
    path = [start]

    queue = []
    for u in g[start]:              # BFS
        if state[u] == 0:
            state[u] = 1
            queue.append(u)
        elif state[u] == 1:
            path.extend([u, start])
    state[start] = 2

    for u in queue:
        path.extend(one_path_iter3(g, u, state))
        path.append(start)
    return path

def one_path3(g):
    state = [0] * len(g)
    state[0] = 1
    return one_path_iter3(g, 0, state)

def test_one_path():
    #     .1.-4.
    #    / | \| \
    #   0--2--5--6
    #    \ | /  /
    #     '3'--'
    # [0, 1, 2, 1, 4, 5, 4, 6, 3, 6, 5, 6,
    #  4, 1, 5, 2, 5, 3, 5, 1, 0, 2, 3, 2, 0, 3, 0]
    g = [[1,2,3], [0,2,4,5], [0,1,3,5], [0,2,5,6], [1,5,6], [1,2,3,4,6], [3,4,5]]
    # [0, 1, 0, 1, 1, 0]
    # g = [[1], [0]]
    print one_path(g)
    assert len(one_path(g)) == sum([len(row) for row in g]) + 1
    print one_path2(g)
    assert len(one_path2(g)) == sum([len(row) for row in g]) + 1
    print one_path3(g)
    assert len(one_path3(g)) == sum([len(row) for row in g]) + 1

if __name__ == '__main__':
    test_one_path()
