# for CRLS ex-22.3-12
def dfs_single_connect(g):
    def dfs_iter(u):
        colors[u] = 1
        for v in g[u]:
            # u -> v
            if colors[v] == 0:
                parent[v] = u
                if not dfs_iter(v):
                    return False
            elif colors[v] == 2:
                if not parent[v]:
                    parent[v] = u
                    continue
                a = v
                p = set()
                while a:
                    p.add(a)
                    a = parent[a]
                b = u
                while b:
                    if b in p:
                        return False
                    b = parent[b]
        colors[u] = 2
        return True

    colors = dict([(u, 0) for u in g])
    parent = dict([(u, None) for u in g])
    for u in g:
        if colors[u] == 0:
            if not dfs_iter(u):
                return False

    # self circles > 1 ?
    for u in g:
        if g[u].count(u) > 1:
            print u, '->', u
            return False
    return True

def test_sc():
    g = {'u':[], 'x':['u'], 'z':['u','x']}
    print dfs_single_connect(g)
    print dfs_dict(g)[0]
#   g = {'u':['v', 'v'], 'v':[]}
#   print dfs_single_connect(g)
#   g = {'u':['u', 'u']}
#   print dfs_single_connect(g)


# for CRLS ex-22.3-11
# find connected non-direct flows
def dfs_find_connected_flows(g):
    def dfs_iter(u):
        gone[u] = 1
        cc[u] = k
        for v in g[u]:
            if not gone[v]:
                dfs_iter(v)

    gone = dict([(u, 0) for u in g])
    cc = dict([(u, 0) for u in g])
    k = 0
    for u in g:
        if not gone[u]:
            k += 1
            dfs_iter(u)
    print '%d connected non-direct flows' %k
    print 'cc:', cc

def test_fcf():
    g = {'u':['v', 'x'], 'v':['y'], 'w':['z'], 'z':['z'], 'x':['v'], 'y':['x']}
    g = non_direct_dict(g)
    dfs_find_connected_flows(g)

# for CRLS ex-22.3-9
# Note: destroy @g
def dfs_dict_type_nondirect(g):
    def dfs_iter(u):
        colors[u] = 1
        first[u] = time[0]
        time[0] += 1
        for v in g[u]:
            assert colors[v] != 2
            if colors[v] == 0:
                print '%s->%s: tree edge' %(u, v)
                if u in g[v]: g[v].remove(u)
                parent[v] = u
                dfs_iter(v)
            elif colors[v] == 1:
                print '%s->%s: reverse edge' %(u, v)
                if u in g[v]: g[v].remove(u)
        colors[u] = 2
        last[u] = time[0]
        time[0] += 1

    time = [1]
    colors = dict([(u, 0) for u in g])
    parent = dict([(u, None) for u in g])
    first = dict([(u, 0) for u in g])
    last = dict([(u, 0) for u in g])

    for u in g:
        if colors[u] == 0:
            dfs_iter(u)
    return parent, first, last

# for CRLS ex-22.3-9
def dfs_dict_type_direct(g):
    def dfs_iter(u):
        colors[u] = 1
        first[u] = time[0]
        time[0] += 1
        for v in g[u]:
            if colors[v] == 0:
                print '%s->%s: tree edge' %(u, v)
                parent[v] = u
                dfs_iter(v)
            elif colors[v] == 1:
                print '%s->%s: reverse edge' %(u, v)
            else:
                print '%s->%s: corse edge or inorder edge' %(u, v)
        colors[u] = 2
        last[u] = time[0]
        time[0] += 1

    time = [1]
    colors = dict([(u, 0) for u in g])
    parent = dict([(u, None) for u in g])
    first = dict([(u, 0) for u in g])
    last = dict([(u, 0) for u in g])

    for u in g:
        if colors[u] == 0:
            dfs_iter(u)
    return parent, first, last

def non_direct_dict(g):
    mg = dict([(u, []) for u in g])
    for u in g:
        for v in g[u]:
            if v not in mg[u]: mg[u].append(v)
            if u not in mg[v]: mg[v].append(u)
    return mg

def test_type():
    # direct graph
    print 'direct'
    g = {'u':['v', 'x'], 'v':['y'], 'w':['y', 'z'], 'z':['z'], 'x':['v'], 'y':['x']}
    dfs_dict_type_direct(g)
    # non-direct graph
    print 'non-direct'
    ng = non_direct_dict(g)
    p = dfs_dict_type_nondirect(ng)[0]
    print p

# non-iteration implementation via stack
def dfs_dict_stack(g):
    stack = []
    time = 1
    parent = dict([(u, None) for u in g])
    colors = dict([(u, 0) for u in g])
    first = dict([(u, 0) for u in g])
    last = dict([(u, 0) for u in g])
    for u in g:
        if colors[u] != 0: continue
        stack.append(u)
        while stack:
            v = stack[-1]           # get stack.top()
            if colors[v] != 0:
                stack.pop()
                if colors[v] == 1:  # if color=2, v has been preemptly handled!
                    last[v] = time
                    time += 1
                    colors[v] = 2
                continue

            # handling: gray
            colors[v] = 1
            first[v] = time
            time += 1
            for w in reversed(g[v]):
                if colors[w] == 0:
                    # in: white
                    stack.append(w)
                    parent[w] = v
    return parent, first, last

# depth first search
def dfs_dict(g):
    def dfs_iter(u):
        colors[u] = 1
        first[u] = time[0]
        time[0] += 1
        for v in g[u]:
            if colors[v] == 0:
                parent[v] = u
                dfs_iter(v)
        colors[u] = 2
        last[u] = time[0]
        time[0] += 1

    time = [1]
    colors = dict([(u, 0) for u in g])
    parent = dict([(u, None) for u in g])
    first = dict([(u, 0) for u in g])
    last = dict([(u, 0) for u in g])

    for u in g:
        if colors[u] == 0:
            dfs_iter(u)
    return parent, first, last

def dfs(g):
    def dfs_iter(u):
        colors[u] = 1
        first[u] = time
        time += 1
        for v in g[u]:
            if colors[v] == 0:
                dfs_iter(v)
        colors[u] = 2
        last[u] = time
        time += 1

    time = 1
    vlen = len(g)
    colors = [0] * vlen
    parent = [-1] * vlen
    first = [0] * vlen
    last = [0] * vlen

    for u in g:
        dfs_iter(u)

def test_dfs():
    g = {'u':['v', 'x'], 'v':['y'], 'w':['y', 'z'], 'z':['z'], 'x':['v'], 'y':['x']}
    assert dfs_dict_stack(g) == dfs_dict_stack(g)
    p, f, l = dfs_dict_stack(g)
    print p
    print f
    print l

if __name__ == '__main__':
#   test_dfs()
#   test_type()
#   test_fcf()
    test_sc()

