from collections import deque

# for CRLS ex-22.2-6
def bfs_color(g):
    vlen = len(g)
    states = [None] * vlen  # None, good, bad

    # start from 0
    states[0] = 'good'
    queue = deque([0])

    while queue:
        u = queue.pop()
        vstate = 'bad' if states[u] == 'good' else 'good'
        for v in g[u]:
            if not states[v]:
                states[v] = 'bad' if states[u] == 'good' else 'good'
                queue.appendleft(v)
            elif states[v] != vstate:
                return 'cannot arrange players'
    return states

def test_bfs_color():
    g = [[1, 3], [0, 2], [1], [0, 4, 5], [3, 5, 6], [3, 4, 6, 7], [4, 5], [5]]
    print bfs_color(g)
    g = [[1, 3], [0, 2], [1], [0, 4, 5], [3, 6], [3, 6, 7], [4, 5], [5]]
    print bfs_color(g)

def find_path(g, start, end):
    assert 0 <= start < len(g)
    assert 0 <= end < len(g)

    if start == end: return [start]     # fast check

    parent = bfs(g, start)[0]
    path = []
    while end != None and start != end:
        path.insert(0, end)
        end = parent[end]
    if end == None: return []
    return [start] + path


# breadth first search for adjacency matrix
# Time: O(V^2)
def bfs_mg(g, start):
    vlen = len(g)
    assert 0 <= start < vlen

    gone = [0] * vlen
    parent = [None] * vlen
    distance = [-1] * vlen

    gone[start] = 1
    distance[start] = 0
    queue = deque([start])

    while queue:                                                                # O(V)
        u = queue.pop()
        for v in filter(lambda v:g[u][v] != 0 and gone[v] == 0, range(vlen)):   # O(V)
            gone[v] = 1
            distance[v] = distance[u] + 1
            parent[v] = u
            queue.appendleft(v)
    return parent, distance

# breadth first search for adjacency list
# Time: O(V+E)
def bfs(g, start):
    vlen = len(g)
    assert 0 <= start < vlen

    parent = [None] * vlen
    distance = [-1] * vlen
    color = ['white'] * vlen

    # init start vertex
    queue = deque([start])  # use appendleft()/pop() as FIFO
    distance[start] = 0
    color[start] = 'gray'

    while queue:
        u = queue.pop()
        assert color[u] == 'gray'
        for v in g[u]:
            if color[v] == 'white':
                parent[v] = u
                distance[v] = distance[u] + 1
                color[v] = 'gray'
                queue.appendleft(v)
        color[u] = 'black'
    return parent, distance

def bfs_depth(g, depth=10):
    current = [0]
    gone = [0] * len(g)
    gone[0] = 1
    for i in range(depth):
        print 'depth', i
        next = []
        for u in current: 
            print ' ', u
            for v in g[u]:
                if gone[v] == 0 and v not in next:
                    gone[v] = 1
                    next.append(v)
        current = next
        if not current: break

def test_bfs_depth():
    g = [[1, 3], [0, 2], [1], [0, 4, 5], [3, 5, 6], [3, 4, 6, 7], [4, 5], [5]]
    print g
    bfs_depth(g)

def test_bfs():
    g = [[1, 3], [0, 2], [1], [0, 4, 5], [3, 5, 6], [3, 4, 6, 7], [4, 5], [5]]
    print bfs(g, 0)
    from graph import list2matrix
    print bfs_mg(list2matrix(g), 0)
    print find_path(g, 2, 4)

if __name__ == '__main__':
#   test_bfs()
#   test_bfs_color()
    test_bfs_depth()
