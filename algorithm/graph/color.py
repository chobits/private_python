from collections import deque
from copy import deepcopy as copy

# use least colours to color adjacency-list graph @g
# NOTE: this function will destroy @g
def colors_iter(g):
    if len(g) == 1: return [[0]]  # use int as color
    last = g.pop()
    v = len(g)
    for u in last:
        g[u].remove(v)

    # color (first) others
    colors = colors_iter(g)

    # color last
    rcolors = []
    for color in colors:
        cset = set(color)
        for u in last:
            if color[u] in cset:
                cset.remove(color[u])
        if cset:
            for c in cset:
                rcolors.append(color + [c])
        else:
            rcolors.append(color + [max(color) + 1])
    return rcolors

def color_iter(g):
    colors = colors_iter(copy(g))
    if len(colors) == 1: return colors[0]
    return min(*colors, key=lambda x:max(x))

def test():
    #     .1.-4.
    #    / | \| \
    #   0--2--5--6
    #    \ | /  /
    #     '3'--'
    g = [[1,2,3], [0,2,4,5], [0,1,3,5], [0,2,5,6], [1,5,6], [1,2,3,4,6], [3,4,5]]
    print color_iter(g)

    #     .1.-4.
    #    / | \| \
    #   0--2--5--6
    #    \ | /
    #     '3'
    g = [[1,2,3], [0,2,4,5], [0,1,3,5], [0,2,5], [1,5,6], [1,2,3,4,6], [4,5]]
    print color_iter(g)

if __name__ == '__main__':
    test()
