#!/usr/bin/python
from itertools import imap, islice
import math

BITS = 8
BASE = 1 << BITS
MASK = BASE - 1
class bignum:
    sign = 1
    def __init__(self, numstr, base=10):
        if type(numstr) == int:
            self.array = bytearray(numstr)
            return

        if not numstr.isdigit:
            raise ValueError("'%s' is not digit" %numstr)

        # sign +|-
        if numstr[0] in '-+':
            self.sign = 1 if numstr[0] == '+' else -1
            numstr = numstr[1:]

        # calculate largest N such that:
        # BASE**N - 1 >= base**n - 1
        # array_size = N >= n * lg(base)/lg(BASE)
        #                 = n * lg(base)/8
        n = len(numstr)
        self.array = bytearray(int(math.ceil(n * math.log(base, 2) / BITS)))
        # calculate largest convwidth such that base ** convwidth <= BASE
        array_size = 0
        convwidth = int(math.floor(BITS / math.log(base, 2)))
        convmax = base ** convwidth
        for valstr in imap(lambda i:numstr[i:i+convwidth], xrange(0, n, convwidth)):
            val = int(valstr)
            valwidth = len(valstr)
            # assert that it happens at final loop
            if valwidth != convwidth:
                convmax = base ** valwidth
            # array_val = array_val * convmax + val
            for (i, av) in enumerate(islice(self.array, 0, array_size)):
                val += av * convmax
                self.array[i] = val & MASK
                val >>= BITS
            if val:
                assert val < BASE
                self.array[array_size] = val
                array_size += 1

    # make len(self) real size
    def __resize(self):
        zero_size = 0
        for v in reversed(self.array):
            if v != 0:
                break
            zero_size += 1
        # resize it
        if zero_size > 0:
            self.array = bytearray(1) if zero_size == len(self) else self.array[0:-zero_size]
        # check zero sign
        if len(self) == 1 and self.array[0] == 0:
            self.sign = 1
                
    def __repr__(self):
        sign = '-' if self.sign < 0 else ''
        return sign + str(sum([ v<<(i*BITS) for (i, v) in enumerate(self.array)]))

    def __len__(self):
        return len(self.array)

    def __iter__(self):
        return iter(self.array)

    def __sub__(a, b):
        if a.sign * b.sign > 0:
            c = a.__sub_value(b)
        else:
            c = a.__add_value(b)
            c.sign = a.sign
        c.__resize()
        return c

    def __add__(a, b):
        if a.sign * b.sign > 0:
            c = a.__add_value(b)
        else:
            c = a.__sub_value(b)
        c.__resize()
        return c

    def __compare_value(a, b):
        if len(a) != len(b):
            return len(a) - len(b)
        for (va, vb) in reversed(zip(a, b)):
            if va != vb:
                return va - vb
        return 0

    def __sub_value(a, b):

        # assert#1 abs(a) > abs(b)
        op = a.__compare_value(b)
        if op == 0:
            return bignum('0')

        c = bignum(max(len(a), len(b)))
        c.sign = a.sign
        if op < 0:
            a, b = b, a
            c.sign = -c.sign

        # let c = smaller number(b) firstly  
        for (i, v) in enumerate(b):
            c.array[i] = v

        # c_val = a_val - c_val
        borrow = 0          # assert that borrow >= 0 always
        for (i, (va, vc)) in enumerate(zip(a, c)):
            borrow = va - vc - borrow
            c.array[i] = borrow & MASK
            borrow = (borrow >> BITS) & 1       # borrow = 1 if borrow < 0
        assert borrow == 0                      # see assert#1
        return c

    def __add_value(a, b):
        c = bignum(max(len(a), len(b)) + 1)
        # make a larger
        if len(a) < len(b): a, b = b, a

        # add smaller number(b) firstly  
        for (i, v) in enumerate(b):
            c.array[i] = v
        # add larger number(a) secondly 
        val = 0
        for (i, (va, vc)) in enumerate(zip(a, c)):
            val += va + vc
            c.array[i] = val & MASK
            val >>= BITS
        c.array[i + 1] = val
        c.sign = a.sign
        return c

    def __slice__(self, i, j):
        return c.array[i:j]

    def __mul__(a, b):
        c = bignum(len(a) + len(b))
        c.sign = a.sign * b.sign
        for (ia, va) in enumerate(a):
            # c_val = c_val + (va * b_val)*(BASE**i)
            val = 0
            for (ib, vb) in enumerate(b):
                val += va * vb + c.array[ia + ib]
                c.array[ia + ib] = val & MASK
                val >>= BITS
            c.array[ia + ib + 1] += val
        c.__resize()
        return c

def test():
    from itertools import combinations, chain
    ci = chain(range(2**1000, 2**1000+5), range(-2**100-1, -2**100+10), xrange(-10, 10))
    i = 0
    for (x, y) in combinations(ci, 2):
        i += 1
        if str(x + y) != repr(bignum(str(x)) + bignum(str(y))):
            print x, '+', y
            break
        if str(x - y) != repr(bignum(str(x)) - bignum(str(y))):
            print x, '-', y
            break
        if str(x * y) != repr(bignum(str(x)) * bignum(str(y))):
            print x, '*', y
            break
    print i

if __name__ == '__main__':
    test()
