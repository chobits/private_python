

def permutations(l, n=0):
    n = n if n else len(l)

    if n == 1:
        yield l
        return

    for i in range(n):
        l[i], l[n - 1] = l[n - 1], l[i]
        for pl in permutations(l, n - 1):
            yield pl 
        l[n - 1], l[i] = l[i], l[n - 1]

for l in permutations([1,2,3,4]):
    print ' '.join([str(v) for v in l])
