import itertools
import time


def perm(string):
    if len(string) == 1:
        return [string]
    for s in perm(string[1:]):
        

def permutations(string):
    if len(string) == 1:
        return [string]
    r = []
    for (i, c) in enumerate(string):
        subs = permutations(string[:i] + string[i + 1:])
        r += [c + s for s in subs]
    return r


def permutations(iterable, r=None):
    'permutations(range(3), 2) --> (0,1) (0,2) (1,0) (1,2) (2,0) (2,1)'
    pool = tuple(iterable)
    n = len(pool)
    r = n if r is None else r
    indices = range(n)
    cycles = range(n-r+1, n+1)[::-1]
    yield tuple(pool[i] for i in indices[:r])
    while n:
        for i in reversed(range(r)):
            cycles[i] -= 1
            if cycles[i] == 0:
                indices[i:] = indices[i+1:] + indices[i:i+1]
                cycles[i] = n - i
            else:
                j = cycles[i]
                indices[i], indices[-j] = indices[-j], indices[i]
            yield tuple(pool[i] for i in indices[:r])
            break
        n -= 1
    else:
        return

if __name__ == '__main__':

    string = 'aaaaaaaaa'

    old = time.time()
    print 'itertools:',
    list(itertools.permutations(string))
    print '%.2f ms' %(time.time() - old)

    old = time.time()
    print 'my:',
    permutations(string)
    print '%.2f ms' %(time.time() - old)


