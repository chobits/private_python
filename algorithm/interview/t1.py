#!/usr/bin/python
# tencent face question
from itertools import izip

# find all element xi in (x1,...,xn),
# which xi is larger than its precursors(x1, .., xi-1)
# and is smaller than its successors(xi+1, .., xn)
def getit(l):
    ll = [1] * len(l)
    # find xi, which xi > (x1,..xi-1)
    maximum = -99999999             # dummy
    for (i, x) in enumerate(l):
        if x <= maximum:
            ll[i] = 0
        else:
            maximum = x
    # find xi, which xi < (xi+1...xn)
    minimum = 99999999              # dummy
    for (i, x) in izip(reversed(xrange(len(l))), reversed(l)):
        if x >= minimum:
            ll[i] = 0
        else:
            minimum = x
    return [x for (able, x) in izip(ll, l) if able]

if __name__ == '__main__':
    print getit([3,2,4,6,8,7])
