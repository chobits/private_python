# binary tree algorithm

class binary_tree_node:
    key = None
    parent = None
    left, right = None, None
    def __init__(self, key=None, parent=None):
        self.key = key
        self.parent = parent

def binary_tree_delete(root, node):
    assert root and node and root != node
    # delete leave
    if not (node.left or node.right):
        if node == node.parent.left:
            node.parent.left = None
        else:
            node.parent.right = None
    # delete node, who has two children
    elif node.left and node.right:
        successor = binary_tree_successor(node)
        if successor.parent.left == successor:
            successor.parent.left = successor.right
        else:
            successor.parent.right = successor.right
        if successor.right:
            successor.right.parent = successor.parent
        node.key = successor.key
    # delete node, who has only onde child
    else:
        n = node.left if node.left else node.right
        n.parent = node.parent
        if node == node.parent.left:
            node.parent.left = n
        elif node == node.parent.right:
            node.parent.right = n
        else:
            raise ErrorValue('!')

def bst_delete(root, key):
    node = binary_tree_search(root, key)
    if node:
        binary_tree_delete(root, node)

def binary_tree_insert(node, key, able_insert_same = 1):
    assert node
    prev = None
    while node:
        prev = node
        if node.key < key:
            node = node.right
        elif node.key > key:
            node = node.left
        else:
            if not able_insert_same:
                return
            node = node.left
    node = binary_tree_node(key, prev)
    if prev.key < key: prev.right = node
    else: prev.left = node

def binary_tree_minimum(root):
    assert root
    while root.left: root = root.left
    return root

def binary_tree_maximum(root):
    assert root
    while root.right: root = root.right
    return root

def binary_tree_predecessor(node):
    if node.left:
        return binary_tree_maximum(node)
    parent = node.parent
    while parent and parent.left == node:
        node = parent
        parent = parent.parent
    return parent           # maybe NIL

def binary_tree_successor(node):
    if node.right:
        return binary_tree_minimum(node.right)
    parent = node.parent
    while parent and parent.right == node:
        node = parent
        parent = parent.parent
    return parent           # maybe NIL

def binary_tree_search(node, key):
    while node:
        if node.key > key:
            node = node.left
        elif node.key < key:
            node = node.right
        else:
            return node
    return None

INORDER, PREORDER, POSTORDER = 0, 1, 2
def binary_tree_traverse(root, order=INORDER, level = -1):
    if not root:
        return
    level += 1
    if order is INORDER:
        binary_tree_traverse(root.left, order, level)
        print '    ' * level, root.key
        binary_tree_traverse(root.right, order, level)
    elif order is PREORDER:
        print '    ' * level, root.key
        binary_tree_traverse(root.left, order, level)
        binary_tree_traverse(root.right, order, level)
    elif order is POSTORDER:
        binary_tree_traverse(root.left, order, level)
        binary_tree_traverse(root.right, order, level)
        print '    ' * level, root.key
    else:
        raise ValueError('error order value')

# non-recursion traverse, PREORDER
def binary_tree_traverse_nr(root):
    stack = [root]
    while stack:
        n = stack.pop()
        print n.key
        if n.right: stack.append(n.right)
        if n.left: stack.append(n.left)

# inorder traverse
# T(n) = O(n) (For proof, see CRLS ex-12.2-7)
def binary_tree_traverse_nr2(root):
    node = binary_tree_minimum(root)
    while node:
        print node.key
        node = binary_tree_successor(node)

def bst_insert(root, *iterable):
    for i in iterable:
        binary_tree_insert(root, i)

if __name__ == '__main__':
    root = binary_tree_node(10)
    bst_insert(root, 5, 7, 5, 3, 11, 12)
    print 'traverse'
    binary_tree_traverse(root, PREORDER)
    print 'traverse non recursion'
    binary_tree_traverse_nr(root)
