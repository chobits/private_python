#!/usr/bin/python
# tree::traverse_fast for CLRS exercise 10.4-6
import itertools

class treenode:
    def __init__(self, key):
        self.parent = None
        self.left = None
        self.right = None
        self.key = key

class tree:
    def __init__(self):
        self.root = None
    
    # caculaate distance between node @parent and node @child
    # assert child is child or in child tree of parent
    # Time: O(lgn)
    def distance_p2c(self, parent, child):
        depth = 0
        while parent.key != child.key:
            parent = parent.right if parent.key < child.key else parent.left
            depth += 1
            assert parent
        return depth

    # caculate distance between node @na and node @nb
    # Time: O(lgn)
    def distance(self, na, nb): 
        # assert na < nb
        if na.key > nb.key: na, nb = nb, na
        n = self.root
        while n:        # Time: O(lgn)
            if n.key == na.key:
                return self.distance_p2c(n, nb)
            if n.key == nb.key:
                return self.distance_p2c(n, na)
            if n.key < na.key:
                n = n.right
            elif n.key > nb.key:
                n = n.left
            else:
                return self.distance_p2c(n, na) + self.distance_p2c(n, nb)
        raise ValueError('There is at least one node(na or nb) not in tree')

    def insert(self, key):
        node = treenode(key)
        if not self.root:
            self.root = node
            return
        x = self.root
        while x:
            parent = x
            if x.key > node.key:
                x = x.left
            elif x.key < node.key:
                x = x.right
            else:
                return
        if parent.key > node.key:
            parent.left = node
        elif parent.key < node.key:
            parent.right = node
        node.parent = parent

    def search(self, key):
        n = self.root
        while n and n.key != key:
            n = n.right if n.key < key else n.left
        return n

    # middle-left-right order
    # recursion implementation
    def traverse(self):
        def __traverse(t, level):
            if t:
                __traverse(t.left, level + 1)
                print '  ' * level, t.key
                __traverse(t.right, level + 1)
        __traverse(self.root, 0)

    # middle-left-right(preorder) order traversal
    # non-recursion implementation (cool!)
    # space: O(1)
    # time:  O(n)
    def traverse_fast(self):
        down = 1
        x = self.root
        while x:
            if down:
                # preorder
                print x.key
                if x.left:
                    x = x.left
                elif x.right:
                    x = x.right
                else:
                    prev, x = x, x.parent
                    down = 0
            else:
                if x.right and x.right != prev:
                    prev, x = x, x.right
                    down = 1
                else:
                    prev, x = x, x.parent

def test_distance():
    t = tree()
    elements = [10, 5, 1, 6, 7, 15, 11, 13, 12, 14, 14, 14, 16]
    for i in elements:
        t.insert(i)
    print '------tree-------------'
    t.traverse()
    print '------distance---------'
    for (key1, key2) in itertools.combinations(elements, 2):
        n1 = t.search(key1)
        n2 = t.search(key2)
        assert n1 and n2
        print 'distance(%d, %d) is %d' %(key1, key2, t.distance(n1, n2))


def test_insert():
    pass

if __name__ == '__main__':
    test_distance()
