# btree from CRLS charpter 18.1
from itertools import izip

class bnode:
    def __init__(self, keys=None, children=None):
        self.keys = [] if keys is None else keys
        self.children = [] if children is None else children
        self.nkeys = len(self.keys)
        assert len(self.children) in [self.nkeys + 1, 0]

    def __repr__(self):
        return 'bnode(%s)' %repr(self.keys)

    def update_nkeys(self, nkeys):
        assert nkeys < self.nkeys == len(self.keys)
        self.nkeys = nkeys
        self.keys = self.keys[0:nkeys]
        self.children = self.children[0:nkeys + 1]

    def isleaf(self):
        return self.children == []

    def debug(self, display=1):
        if display == 1:
            print ".-------debug-------."
            print 'nkeys:', self.nkeys
            print 'keys:', self.keys
            print 'children:', self.children
            print "'-------debug-------'"
        assert len(self.keys) == self.nkeys

    def insert_idx(self, key):
        if self.nkeys != len(self.keys):
            self.debug()
        for (i, k) in enumerate(self.keys):
            if key < k:
                return i
        return self.nkeys

    def insert_key(self, key, idx=None):
        if idx == None: idx = self.insert_idx(key)
        self.keys.insert(idx, key)
        self.nkeys += 1

    def delete_key(self, key):
        self.keys.remove(key)
        self.nkeys -= 1

    def traverse(self):
        isleaf =  self.isleaf()
        if not isleaf: self.children[0].traverse()
        for i in range(self.nkeys):
            print self.keys[i]
            if not isleaf: self.children[i + 1].traverse()

class btree:
    # root = bnode(), next btree() will reuse root!!
    def __init__(self, t=2):
        self.root = bnode()
        self.t = t

    def isfull(self, n=None):
        if not n: n = self.root
        return n.nkeys == 2 * self.t - 1

    def split(self, parent, cidx):
        t = self.t                                              # alias
        child = parent.children[cidx]
        parent.insert_key(child.keys[t - 1], cidx)              # before child.update_nkeys(t - 1)
        nchild = bnode(child.keys[t:], child.children[t:])      # create new splited child
        child.update_nkeys(t - 1)                               # split child
        parent.children.insert(cidx + 1, nchild)

    def insert_nonfull(self, node, key):
        assert not self.isfull(node)
        # insert key to leaf node
        if node.isleaf():
            node.insert_key(key)
            return
        # insert key to child of non-leaf node
        i = node.insert_idx(key)
        if self.isfull(node.children[i]):
            self.split(node, i)
            if key > node.keys[i]: # if yes, insert new splited child
                i += 1
        self.insert_nonfull(node.children[i], key)

    def insert(self, key):
        if self.isfull():
            self.root, n = bnode(), self.root
            self.root.children.append(n)
            self.split(self.root, 0)
        self.insert_nonfull(self.root, key)

    def delete_assert(self, n, key):
        def _merge_delete(parent, key_idx, key):
            lc = parent.children[key_idx]
            rc = parent.children.pop(key_idx + 1)   # remove rc
            mkey = parent.keys.pop(key_idx)         # remove middle key
            parent.nkeys -= 1
            # merge key, rc into lc
            lc.keys.append(mkey)
            lc.keys.extend(rc.keys)
            lc.children.extend(rc.children)
            lc.nkeys = len(lc.keys)
            # delete key from new merged tree, which contains 2t-1 keys
            self.delete_assert(lc, key)

        assert n == self.root or n.nkeys >= self.t
        # 1. n is leaf
        if n.isleaf():
            #print '1.'
            n.delete_key(key)
            return

        # 2. n is non-leaf, and key is in n.keys
        if key in n.keys:
            idx = n.keys.index(key)
            lc = n.children[idx]        # left child
            rc = n.children[idx + 1]    # right child
            # 2.a key's one child has >=t keys, using k instead of key
            if lc.nkeys >= self.t or rc.nkeys >= self.t:
                #print '2.ab'
                c = lc if lc.nkeys >= self.t else rc
                k = self.maximum(lc) if c is lc else self.minimum(rc)
                self.delete_assert(c, k)
                n.keys[idx] = k
                return
            # 2.b nkeys of two children is t-1, then merge lc, key and rc
            #print '2.c'
            _merge_delete(n, idx, key)
            return

        # 3. n is non-leaf, and key is in children of n
        idx = n.insert_idx(key)
        c = n.children[idx]
        # 3. child contains >=t keys
        if c.nkeys >= self.t:
            #print '3.'
            self.delete_assert(c, key)
            return
        # 3.a child contains t-1 keys
        lc = n.children[idx - 1] if idx > 0 else None
        rc = n.children[idx + 1] if idx < n.nkeys else None
        # 3.a-left one brother child of c contains >=t keys
        # move one key from child to c
        #  n.keys              idx-1 | idx
        #  n.children:  lc(idx-1)|c(idx)|rc(idx+1)
        if lc and lc.nkeys >= self.t:
            #print '3.a-left'
            c.keys.insert(0, n.keys[idx - 1])
            if not lc.isleaf(): c.children.insert(0, lc.children.pop())
            c.nkeys += 1
            n.keys[idx - 1] = lc.keys.pop()
            lc.nkeys -= 1
            self.delete_assert(c, key)
        # 3.a-right
        elif rc and rc.nkeys >= self.t:
            #print '3.a-right'
            c.keys.append(n.keys[idx])
            if not rc.isleaf(): c.children.append(rc.children.pop(0))
            c.nkeys += 1
            n.keys[idx] = rc.keys.pop(0)
            rc.nkeys -= 1
            self.delete_assert(c, key)
        # 3.b both brothers contains t-1 keys, merge c with one child(lc or rc)
        else:
            #print '3.b'
            _merge_delete(n, idx - 1 if lc else idx, key)

    def delete(self, key):
        self.delete_assert(self.root, key)
        if self.root.nkeys == 0 and not self.root.isleaf():
            assert len(self.root.children) == 1
            self.root = self.root.children[0]

    # return (idx, node, nidx, parent),
    # which means key is node.keys[idx]
    # and node is parent.children[nidx]
    def search(self, key):
        parent, n = None, self.root
        idx, nidx = -1, -1
        while True:
            idx = n.insert_idx(key)
            if idx > 0 and key == n.keys[idx - 1]:
                return (idx - 1, n, nidx, parent)
            if n.isleaf():
                return (-1, None, -1, None)
            parent, n = n, n.children[idx]
            idx, nidx = -1, idx

    def minimum(self, n=None):
        if n == None: n = self.root
        while not n.isleaf():
            n = n.children[0]
        return n.keys[0]

    def maximum(self, n=None):
        if n == None: n = self.root
        while not n.isleaf():
            n = n.children[-1]
        return n.keys[-1]

    def predecessor(self, key):
        pass

    def traverse(self):
        self.root.traverse()

def test1():
    tree = btree()
    n = 100000
    for x in xrange(n, 0, -1):
        if x % (n / 20) == 0:
            print 'insert', x
        tree.insert(x)

# for CRLS ex 18.2-1
def test2():
    t = btree()
    for (i, k) in enumerate('FSQKCLHTVWMRNPABXYDZE'):
        t.insert(k)
        print '%3d: insert %s [%s, %s]' %(i, k, t.minimum(), t.maximum())

# test for delete, e.g for CRLS picture 18-8
#                 also for CRLS ex 18.3-1
def test3():
    t = btree(3)
    t.root = bnode(list('P'), [None, None])
    t.root.children[0] = bnode(list('CGM'),\
        [bnode(list('AB')), bnode(list('DEF')), bnode(list('JKL')), bnode(list('NO'))])
    t.root.children[1] = bnode(list('TX'),\
        [bnode(list('QRS')), bnode(list('UV')), bnode(list('YZ'))])

    for x in 'FMGDBCPV':
        print 'delete', x
        t.delete(x)

    t.root.debug()

def test4():
    t = btree(5)
    for x in xrange(10000):
        t.insert(x)
    for x in xrange(10000):
        t.delete(x)
    t.root.debug()

if __name__ == '__main__':
    test3()
