# for CRLS thinking problem 12-2

class node:
    iskey = 0
    left = None
    right = None

class tree:
    root = node()               # dummy node

    def insert(self, *iterable):
        def _insert(n, chars):
            for c in chars:
                if c is '0':
                    if not n.left:
                        n.left = node()
                    n = n.left
                else:
                    if not n.right:
                        n.right = node()
                    n = n.right
            n.iskey = 1
        # insert all chars
        for chars in iterable:
            _insert(self.root, chars)
    def traverse(self):
        def _traverse(node, chars):
            if node:
                _traverse(node.left, chars + '0')
                if node.iskey: print chars
                _traverse(node.right, chars + '1')

        _traverse(self.root, '')

if __name__ == '__main__':
    t = tree()
    t.insert('0', '011', '10', '100', '1011', '111111')
    t.traverse()
