#!/usr/bin/python
# for CRLS 16.1
# recursion greed implementation
from itertools import islice
START = 0
FINAL = 1

def maxset(t, n):
    r = [t[0]]
    for (s, f) in islice(t, 1, n):
        if s >= r[-1][FINAL]:
            r.append((s, f))
    return r

# finish time increasing order
# test activity (start, finish) time
#time_set =  [(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9)]
time_set = [(i, 2*i) for i in xrange(400)]
#time_set =  [(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9), (6, 10), (8, 11), (8, 12), (2, 13), (12, 14)]

# output
mset = maxset(time_set, len(time_set))
iset = ['a%d' %(time_set.index(x) + 1) for x in mset]
print '{', ', '.join(iset), '}'
