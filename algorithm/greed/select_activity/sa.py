#!/usr/bin/python
# for CRLS 16.1
# recursion greed implementation
from itertools import islice
START = 0
FINAL = 1

def maxset(t, i, j):
    f_i = t[i][FINAL]
    for (index, (s, f)) in enumerate(islice(t, i + 1, j)): # [i+1, j-1]
        if s >= f_i:
            return [t[index + i + 1]] + maxset(t, index + i + 1, j)
    return []

# finish time increasing order
# test activity (start, finish) time
#time_set =  [(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9)]
time_set = [(i, 2*i) for i in xrange(400)]
#time_set =  [(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9), (6, 10), (8, 11), (8, 12), (2, 13), (12, 14)]

# senial element
time_set.insert(0, (-2, -1))
time_set.append((999999998, 999999999))

# output
mset = maxset(time_set, 0, len(time_set) - 1)
iset = ['a%d' %time_set.index(x) for x in mset]
print '{', ', '.join(iset), '}'
