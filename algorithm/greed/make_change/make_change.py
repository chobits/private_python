#!/usr/bin/python
# for CLRS thinking problem 16-1

# for 16-1 a)
# using minimum changes in l to make n
# all entries in l must be 
# 1. we can use tail call optimization to rewrite it.
# 2. @l will be destroyed
def make_change_s(n, l):
    def _make_change(n, l, r):
        if n > 0:
            change = l.pop()
            k = n / change
            r[change] = k
            _make_change(n - k * change, l, r)
    r = {}
    l.sort()
    _make_change(n, l, r)
    return r

# for 16-1 d)
# Time: O(nk), k = len(l)
# dynamic implementation
def memo(cache):
    def decorator(f):
        def wrap(n, l, start):
            if cache[n][start] == None:
                cache[n][start] = f(n, l, start)
            return cache[n][start]
        return wrap
    return decorator

# NOTE:
# For large n and small len(l), closing memo is faster
#  because alloc cache costs too much.
# For large len(l), opening memo is so fast!
def make_change(n, l):
    cache = [[None] * (len(l) + 1) for _ in xrange(n + 1)]
    @memo(cache)
    def _make_change(n, l, start):
        if n == 0:
            return 0
        if start == len(l):
            return 0xdeadbeef           # It cannot make changes.
        change = l[start]
        k = n / change
        return min(_make_change(n, l, start + 1), k + _make_change(n - k * change, l, start + 1))
    l.sort(reverse=True)
    return _make_change(n, l, 0)

if __name__ == '__main__':
    n = 60
    l = [10, 5, 25, 1]
    # test 1
    r = make_change_s(n, l)
    print n, 'cents make changes:'
    for (k, v) in r.iteritems():
        print ' %4d cent * %d' %(k, v)
    # test 2
    n = 8131
    l = range(25, 0, -1)
    r = make_change(n, l)
    if r == 0xdeadbeef:
        print '%d cents cannot make changes' %n
    else:
        print '%d cents need %d coins at least.' %(n, r)
    # test 3
    n = 60
    l = [10, 5, 25, 1, 21]
    r = make_change(n, l)
    if r == 0xdeadbeef:
        print '%d cents cannot make changes' %n
    else:
        print '%d cents need %d coins at least.' %(n, r)
