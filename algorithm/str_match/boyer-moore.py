import sys
from itertools import islice
from time import time

# O(m^2)
def bad_char_rule_table2(pattern, m=None):
    assert pattern != ''
    if m == None: m = len(pattern)
    table = [[-1] * m for _ in range(256)] # table[char_ordinal_value][pattern_index]
    for i, ic in enumerate(pattern):
        for k in range(i + 1, m):
            table[ord(ic)][k] = i
    return table

# O(m) (=cm, where c=256)
def bad_char_rule_table(pattern, m=None):
    assert pattern != ''
    if m == None: m = len(pattern)
    table = [[-1] * m for _ in range(256)] # table[char_ordinal_value][pattern_index]
    for ord_c in range(256):
        highest_index = -1
        for i, c in enumerate(pattern):
            table[ord_c][i] = highest_index
            if ord(c) == ord_c: highest_index = i
    return table

# return bad_char_rule_table(pattern)[pattern[-1]][m - 1]
def bad_char_rule_last(pattern, m=None):
    assert pattern != ''
    if m == None: m = len(pattern)
    if m == 0: return -1
    for i, c in enumerate(reversed(pattern)):
        if c == pattern[-1] and i > 0:
            return m - i - 1
    return -1
    
# Boyer-Moore-Horspool string search
# a simplification of BM algorithm using only the bad character rule
# Time: O(n + m) for best-case
#       O(nm) for worst-case (text='aaaaaaaaaaaaaa', pattern='baaaa')
# Space: O(m)
def boyer_moore_horspool_search(text, pattern):
    n = len(text)
    m = len(pattern)
    if m == 0: return 0
    if m > n: return -1
    bad_table = bad_char_rule_table(pattern, m)
    k = m - 1
    while k < n:
        for i, pchar in enumerate(reversed(pattern)):
            if text[k - i] != pchar:
                break
        else:   # loop end not break
            return k - i
        off = bad_table[ord(text[k - i])][m - 1 - i]
#       print '%s before [%d] occurs in [%d]' %(text[k - i], m - 1 - i, off)
        k = k - i + m - off - 1 # It works well when off=-1.
    return -1

# fast search algorithm used by CPython-2.5.6(C-implemented)
# a simplication of Boyer-Moore, incorporating ideas from Horspool and Sunday
# Time: best case O(n + m)
#       worst case O(nm)
# Space: O(1)
def fast_search(text, pattern):
    n = len(text)
    m = len(pattern)
    if m == 0: return 0
    if m > n: return -1
    skip = bad_char_rule_last(pattern, m)
    k = m - 1
    while k < n:
        if pattern[-1] == text[k]:          # from boyer-moore
            if text[k - m + 1:k + 1] == pattern: return k - m + 1
            if k + 1 < n and text[k + 1] in pattern:    # prefetch a char to match from sunday
                k += m - skip - 1           # from horspool
            else:
                k += m + 1                  # from sunday
        else:
            if k + 1 < n and text[k + 1] in pattern:
                k += 1
            else:
                k += m + 1                  # from sunday
    return -1

def test_bmh():
    text = 'a' * 10000
    worst_pattern = 'b' + 'a' * 800
    best_pattern = 'b' * 801

    # best case
    old = time()
    boyer_moore_horspool_search(text, best_pattern)
    print 'best case costs', time() - old

    # worst case
    old = time()
    boyer_moore_horspool_search(text, worst_pattern)
    print 'worst case costs :', time() - old

def test_fast_search():
    print fast_search(sys.argv[1], sys.argv[2])

if __name__ == '__main__':
#   test_bmh()
    test_fast_search()  
#   print boyer_moore_horspool_search(sys.argv[1], sys.argv[2])
