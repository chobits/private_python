import itertools
CHARS = 256


# for CRLS exercise 32.4-5
# Time: O(n), n = len(a)
def is_recyclable(a, b):
    # why pi + pi > len(a)?
    #    see: a->'xxyx' b-> 'xyxx'
    # or see: a->'xxxx' b-> 'xxxx'
    return len(a) == len(b) and (create_pi(b + a)[-1] + create_pi(a + b)[-1]) >= len(a)

# Time: O(n), n = len(pattern)
# pi[len] -> common len
# pi[0] -> nonexist (0)
def create_pi(pattern):
    pi = [0] * (len(pattern) + 1)
    for poff, c in enumerate(pattern):       # plen <- poff + 1
        k = pi[poff]
        while k > 0 and c != pattern[k]:
            k = pi[k]
        if c == pattern[k] and k < poff:
            k += 1
        pi[poff + 1] = k
#   print pi
    return pi

def kmp_string_match(pattern, string):
    pi = create_pi(pattern)
    offsets = []
    plen = len(pattern)
    slen = len(string)
    poff = 0
    off = 0
    while off < slen:
        if poff < plen and string[off] == pattern[poff]:
            off += 1
            poff += 1
            if poff == plen:
                offsets.append(off - plen)
        elif poff > 0:
            poff = pi[poff]
        else:
            off += 1
    return offsets


# Time: O(CHARS*len(pattern))
def create_automaton(pattern):
    pi = create_pi(pattern)
    states = len(pattern) + 1
    table = [[0] * CHARS  for _ in range(states)]
    for ochar in range(CHARS):
        for state in range(states):
            # 0 1 2 ..|.|.|....|state|char
            #        0|1|2| .........|nstate| .... |state|char
            #        ====== equal len =======
            if state == len(pattern) or chr(ochar) != pattern[state]:
                table[state][ochar] = table[pi[state]][ochar]
            else:
                table[state][ochar] = state + 1
    return lambda state, char: table[state][ord(char)]

# Time: O(CHARS*(len(pattern)**3))
# return a table-driven auto machine
# NOTE: assert pattern is filled with chars
def create_automaton2(pattern):
    states = len(pattern) + 1
    table = [[0] * CHARS  for _ in range(states)]
    for ochar in range(CHARS):
        for state in range(states):
            # 0 1 2 ..|.|.|....|state|char
            #        0|1|2| .........|nstate| .... |state|char
            #        ====== equal len =======
            for nstate in range(min(states - 1, state + 1), -1, -1):
                # equal len == nstate
                if pattern[state - nstate + 1:state] + chr(ochar) == pattern[0:nstate]:
                    break
            table[state][ochar] = nstate
    return lambda state, char: table[state][ord(char)]

def automaton_string_match(pattern, string):
    automaton_func = create_automaton(pattern)
    final_state = len(pattern)
    offsets = []
    state = 0
    for off, char in enumerate(string):
        state = automaton_func(state, char)
        if state == final_state:
            offsets.append(off - final_state + 1)
    return offsets
# modq is larger, the times(tval == pval) is smaller
def rabin_karp_match(text, pattern, digit,\
                     modq=13, tvalf=lambda x:x, pvalf=lambda x:x):
    tlen = len(text)
    plen = len(pattern)
    dplen = (digit ** (plen - 1)) % modq

    # preprocessing
    pval, tval = 0, 0
    for c, t in zip(pattern, text):                 # O(m)
        pval = (pval * digit + pvalf(c)) % modq
        tval = (tval * digit + tvalf(t)) % modq

    # matching
    offsets = []
#   prematched = 0
    for off in range(0, tlen - plen + 1):
#       print tval, pval, pattern, text[off:off + plen]
        if tval == pval:
#           prematched += 1
            if pattern == text[off:off + plen]:
                offsets.append(off)
        if off < tlen - plen:
            tval = (digit * (tval - dplen * tvalf(text[off])) + tvalf(text[off + plen])) % modq
#   print 'modq %d, prematched %d' %(modq, prematched)
    return offsets

def rabin_karp_string_match(pattern, string):
    return rabin_karp_match(string, pattern, CHARS, 43, ord, ord)

# For CLRS exercise 32.1-4
# n = len(string)
# m = len(pattern)
# q = '*' number
def isprefix(pattern, string):
    slen = len(string)
    if len(pattern.replace('*', '')) > slen:
        return 0
    soff = 0
    for i, c in enumerate(pattern):             # O(m)
        if c == '*':                            # O(q)
            for ssoff in range(soff, slen):     # O(n)
                if isprefix(pattern[i+1:], string[ssoff:]):
                    return 1
        else:
            if c == string[soff]:
                soff += 1
            else:
                return 0
    return 1
# '*' may be in pattern, which can match 0-infinite len string
def star_string_match(pattern, string):
    offsets = []
    plen = len(pattern)
    slen = len(string)
    for off in range(0, slen - plen + 1): # O(n+m-1)
        substr = string[off:]
        if isprefix(pattern, substr):
            offsets.append(off)
    return offsets

# Time: O(n)
# only work for pattern, in which case all chars in it are different
# For CLRS exercise 32.1-2
def naive_string_match2(pattern, string):
    plen = len(pattern)
    slen = len(string)
    offsets = []
    off = 0
    while off < slen - plen + 1:
        prefix_length = 0
        substr = string[off:off + plen]
        for (i, (p, s)) in enumerate(zip(pattern, substr)):
            if p != s:
                prefix_length = i
                break
        else:
            offsets.append(off)
            prefix_length = plen
        off += prefix_length
    return offsets

                
# Time: O((n+m-1)m)
# m = len(pattern), n = len(string)
def naive_string_match(pattern, string):
    offsets = []
    plen = len(pattern)
    slen = len(string)
    for off in range(0, slen - plen + 1): # O(n+m-1)
        substr = string[off:off + plen]
        if substr == pattern:               # O(m)
            offsets.append(off)
    return offsets

def test(match, pattern, string):
    print 'TEST FOR %s():' %match.func_name
    offsets = match(pattern, string)
    if not offsets:
        print '%s isnt matched in %s' %(pattern, string)
        return
    print string
    for off in offsets:
        print ' ' * off + string[off:off + len(pattern)]


# string combinations
def strcb(string, n):
    return (''.join(text) for text in itertools.combinations(string, n))

def test_with():
    import string
    text = string.ascii_lowercase
    n = 2
    for t in strcb(text, n):
        for p in itertools.chain(*[strcb(text, x) for x in range(1,n)]):
            if naive_string_match(p, t) != automaton_string_match(p, t):
                print 'error:', p, t

if __name__ == '__main__':
#   test(naive_string_match, 'llo', 'hello wrold! lloo')
#   test(naive_string_match, 'llol', 'hello wrold! lloo')
#   test(naive_string_match, 'a'*10, 'a'*11)
#   test(naive_string_match2, 'abcdef', 'abcdef'*10)
#   test(star_string_match, 'ab*ba*c', 'cabccbacbacab')
#   test(star_string_match, 'a*b*c', 'kxxaxbxxxcc')
#   test(star_string_match, '*a', 'kxxaxbxxxcc')
#   test(rabin_karp_string_match, 'llo', 'hello wrold! lloo')
#   test(automaton_string_match, 'llo', 'hello wrold! lloo')
#   test(automaton_string_match, 'aaa', 'aaaabaabababaaaa')
#   test(automaton_string_match, 'hello', 'hello')
    test_with()

