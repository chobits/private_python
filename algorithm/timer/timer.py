
class timer:
    def __init__(self, when, handler):
        self.when = when
        self.handler = handler

    def __repr__(self):
        return '<timer %d>' %self.when


class time_manager:
    def __init__(self, ticks):
        self.ticks = ticks
        self.tick = 0

    def add(self, when, handler):
        pass

    def get_expired_timer(self):
        pass

    # test the alogrithm
    def run(self):
        i = 0
        while i < self.ticks:
            self.tick += 1
            timers = self.get_expired_timer()
            if timers:
                print 'tick: ', i
            for t in timers:
                t.handler(t)
            i += 1

class timing_wheel(time_manager):
    def __init__(self):
        time_manager.__init__(self, 2 ** 32)
        # super(timing_wheel, self).__init__(2 ** 32)      # error
        self.wheel1 = [[] for _ in range(1 << 12)]       # high   12 bit size
        self.wheel2 = [[] for _ in range(1 << 10)]       # middle 10 bit size
        self.wheel3 = [[] for _ in range(1 << 10)]       # low    10 bit size

    def tick2hml(self, tick):
        return tick >> 20, (tick >> 10) & 0x3ff, tick & 0x3ff
        
    def add(self, when, handler):
        assert 0 < when < 2 ** 32
        t = timer(when, handler)
        self.add_timer(t)

    def add_timer(self, t):
        when = t.when
        if when <= self.tick:
            return False

        idx = when - self.tick
        if idx < (1 << 10):
            wheel = self.wheel1[when & 0x3ff]
        elif idx < (1 << 20):
            wheel = self.wheel2[(when >> 10) & 0x3ff]
        else:
            wheel = self.wheel3[(when >> 20)]
        wheel.append(t)
        return True
        
    def get_expired_timer(self):
        ret_timers = []
        high, mid, low = self.tick2hml(self.tick)

        # #2->#1
        wheel, self.wheel2[mid] = self.wheel2[mid], []
        for t in wheel: self.add_timer(t)

        # #3->#2->#1
        wheel, self.wheel3[high] = self.wheel3[high], []
        for t in wheel: self.add_timer(t)

        # get expired from #1
        ret_timers, self.wheel1[low] = self.wheel1[low], []
        return ret_timers
            
    def test(self):
        def test_handler(t):
            print 'timer %d run %d ticks' % (t.when, self.tick)
            h = [x for x in self.wheel1 if x]
            m = [x for x in self.wheel2 if x]
            l = [x for x in self.wheel3 if x]
            if h: print 'high:', h
            if m: print 'mid :', m
            if l: print 'low :', l

        for i in range(10, 1, -1):
            self.add(i + (1 << 10), test_handler)
        for i in range(10, 1, -1):
            self.add(i + (2 << 10), test_handler)
        for i in range(10, 1, -1):
            self.add((3 << 10), test_handler)

        self.run()
        
        
if __name__ == '__main__':
    
    tw = timing_wheel()
    tw.test()
    
