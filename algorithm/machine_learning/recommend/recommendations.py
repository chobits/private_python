critics = {
    'lisa rose':{
        'lady in the water':2.5,
        'snakes on a plane':3.5,
        'just my luck':3.0,
        'superman returns':3.5,
        'you, me and dupree':2.5,
        'the night listener':3.0
    },
    'gene seymour':{
        'lady in the water':3.0,
        'snakes on a plane':3.5,
        'just my luck':1.5,
        'superman returns':5.0,
        'the night listener':3.0,
        'you, me and dupree':3.5
    },
    'michael phillips':{
        'lady in the water':2.5,
        'snakes on a plane':3.0,
        'superman returns':3.5,
        'the night listener':4.0
    },
    'claudia puig':{
        'snakes on a plane':3.5,
        'just my luck':3.0,
        'the night listener':4.5,
        'superman returns':4.0,
        'you, me and dupree':2.5,
    },
    'mick lasalle':{
        'lady in the water':3.0,
        'snakes on a plane':4.0,
        'just my luck':2.0,
        'superman returns':3.0,
        'the night listener':3.0,
        'you, me and dupree':2.0
    },
    'jack mathews':{
        'lady in the water':3.0,
        'snakes on a plane':4.0,
        'the night listener':3.0,
        'superman returns':5.0,
        'you, me and dupree':3.5
    },
    'toby':{
        'snakes on a plane':4.5,
        'you, me and dupree':1.0,
        'superman returns':4.0
    }
}


from math import sqrt
from itertools import combinations

def transform_prefs(prefs):
    tprefs = {}
    for item in prefs:
        for i in prefs[item]:
            if tprefs.has_key(i):
                tprefs[i][item] = prefs[item][i]
            else:
                tprefs[i] = {item:prefs[item][i]}
    return tprefs

def distance(iterable):
    return sqrt(sum([pow(x, 2) + pow(y, 2) for (x, y) in iterable]))
def sim_distance(prefs, person1, person2):
    shared_item = set([x for x in prefs[person1] if x in prefs[person2]])
    if not shared_item: return 0
    sum_distance = sqrt(sum([pow(prefs[person1][x] - prefs[person2][x], 2) for x in shared_item]))
    return 1 / (sum_distance + 1)

def sim_distance2(prefs, person1, person2):
    shared_item = set([x for x in prefs[person1] if x in prefs[person2]])
    n = len(shared_item)
    if n == 0: return 1

    sum1 = sum([prefs[person1][x] for x in shared_item])
    sum2 = sum([prefs[person2][x] for x in shared_item])

    sumsq1 = sum([pow(prefs[person1][x], 2) for x in shared_item])
    sumsq2 = sum([pow(prefs[person2][x], 2) for x in shared_item])

    psum = sum([prefs[person1][x] * prefs[person2][x] for x in shared_item])

    num = psum - (sum1 * sum2 / n)
    den = sqrt((sumsq1 - pow(sum1, 2) / n) * (sumsq2 - pow(sum2, 2) / n))
    return num / den if den else 0

# so slow: O(len(prefs))
def top_matches(prefs, person, n=5, similarity=sim_distance2):
    scores = [(similarity(prefs, person, other), other) for other in prefs if other != person]
    scores.sort(reverse=True)
    return scores[0:n]

def get_recommendations_fast(prefs, similar_item, user):
    total_sim = {}
    total_score = {}

    for item1 in prefs[user]:                       # O(1)
        rating = prefs[user][item1]
        for (sim, item2) in similar_item[item1]:    # O(1)
            if item2 in prefs[user]: continue # and prefs[user] != 0: continue
            if total_sim.has_key(item2):
                total_sim[item2] += sim
                total_score[item2] += sim * rating
            else:
                total_sim[item2] = sim
                total_score[item2] = sim * rating

    rankings = [(total_score[item] / total_sim[item], item) for item in total_sim]
    rankings.sort(reverse=True)
    return rankings

def test_grf():
    similar_items = caculate_similar_items(critics)
    r = get_recommendations_fast(critics, similar_items, 'toby')
    print 'fast recommendations, toby:'
    print '\n'.join(['%-22s %.4f' %(item, score) for (score, item) in r])

# n -> len(prefs)
# R -> sum([len(item) for item in prefs]) ~= CONST*n
def get_recommendations2(prefs, person, similarity=sim_distance2):
    total_critics = {}
    total_scores = {}
    # O(n + R)
    for other in prefs:                 # O(n)
        if person == other: continue
        sim = similarity(prefs, person, other)
        if sim <= 0: continue
        for item in prefs[other]:
            if item in prefs[person] and prefs[person][item] != 0: continue # person has watched it!
            if total_scores.has_key(item):
                total_critics[item] += sim * prefs[other][item]
                total_scores[item] += sim
            else:
                total_critics[item] = sim * prefs[other][item]
                total_scores[item] = sim
    rankings = [(total_critics[item] / total_scores[item], item) for item in total_scores]
    rankings.sort(reverse=True)
    return rankings

def get_recommendations(prefs, person, similarity=sim_distance2):
    # O(n)
    scores = [(similarity(prefs, person, other), other) for other in prefs if other != person]
    scores = [(score, other) for (score, other) in scores if score > 0]

    have_items = prefs[person]
    none_items = set()
    # O(n+R)
    for (score, other) in scores:
        for item in prefs[other]:
            if item not in have_items or prefs[other][item] == 0:
                none_items.add(item)
    if not none_items: return []

    recommendations = []
    for item in none_items:
        sum_critics = sum([score * prefs[other][item] for (score, other) in scores if item in prefs[other]])
        sum_score = sum([score for (score, other) in scores if item in prefs[other]])
        recommendations.append((sum_critics / sum_score, item))
    recommendations.sort(reverse=True)
    return recommendations

def caculate_similar_items(prefs, n=10):
    sitems = {}
    tprefs = transform_prefs(prefs)
    for item in tprefs:
        sitems[item] = top_matches(tprefs, item, n=n, similarity=sim_distance)
    return sitems

def test_csi():
    similar_items = caculate_similar_items(critics)
    for item in similar_items:
        print '%s:' %item
        print '\n'.join(['  %-20s %.3f' %(person, score) for (score, person) in similar_items[item] if score > 0])

def test_gr():
#   r = get_recommendations(critics, 'toby')
#   print '\n'.join(['%-20s %.4f' %(item, score) for (score, item) in r])
#   r = get_recommendations2(critics, 'toby')
#   print '\n'.join(['%-20s %.4f' %(item, score) for (score, item) in r])
    r = get_recommendations(critics, 'toby', sim_distance)
    print 'recommendations toby:'
    print '\n'.join(['%-20s %.4f' %(item, score) for (score, item) in r])
#   r = get_recommendations2(critics, 'toby', sim_distance)
#   print '\n'.join(['%-20s %.4f' %(item, score) for (score, item) in r])

def test_sd():
    answers = [(person1, person2, sim_distance2(critics, person1, person2))
                for (person1, person2) in combinations(critics, 2)]
    answers.sort(key=lambda x:x[2], reverse=True)
    print '\n'.join(['%-20s %.4f %20s'%(person1, s, person2) for (person1, person2, s) in answers])

def test_tm():
    answers = top_matches(critics, 'toby', 10)
    print '\n'.join(['%-20s %.4f' %(person, score) for (score, person) in answers])

def test_tprefs():
    movies = transform_prefs(critics)

    print "customers who likes 'superman returns' also like:"
    answers = top_matches(movies, 'superman returns')
    print '\n'.join(['%-20s %.4f' %(person, score) for (score, person) in answers])

    print "customers who likes 'just my luck' also like:"
    answers = get_recommendations(movies, 'just my luck')
    print '\n'.join(['%-20s %.4f' %(person, score) for (score, person) in answers])

if __name__ == '__main__':
#   test_sd()
#   test_tm()
#   test_gr()
#   test_tprefs()
#   test_csi()
    test_grf()
