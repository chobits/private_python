import recommendations as rc
from time import time
from movie_stats.critics import get_critics, get_movies
from cPickle import dump, load

def test_time(f, *args, **kwargs):
    old = time()
    ret = f(*args, **kwargs)
    print '\n%s() takes %.3f sec.' %(f.func_name, time() - old)
    return ret

def display_movie_rating(ratings, movies, n=10):
    if n > len(ratings): n = len(ratings)
    print 'rating | movie'
    for rating, mid in ratings[0:n]:
        print ' %.3f | %s' %(rating, movies[mid])

def load_si(critics=None):
    try:
        f = open('movie_stats/si.dump', 'r')
        similar_items = load(f)
        f.close()
    except IOError:
        if not critics: return None
        f = open('movie_stats/si.dump', 'w')
        similar_items = test_time(rc.caculate_similar_items, critics)
        dump(similar_items, f)
        f.close()
    return similar_items

def test():
    critics = get_critics()     # {user_id:{movie_id:rating}}
    movies = get_movies()       # {movie_id:movie_name}

    # recommendations2
    r = test_time(rc.get_recommendations2, critics, '87')
    display_movie_rating(r, movies, 10)

    # recommendations
    r = test_time(rc.get_recommendations, critics, '87')
    display_movie_rating(r, movies, 10)

    similar_items = load_si()
    # get_recommendations_fast
    r = test_time(rc.get_recommendations_fast, critics, similar_items, '87')
    display_movie_rating(r, movies, 10)



if __name__ == '__main__':
    test()
else:
    critics = get_critics()     # {user_id:{movie_id:rating}}
    movies = get_movies()       # {movie_id:movie_name}
    similar_items = load_si()
