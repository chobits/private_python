import random
import math
import itertools
from optimization import *

dorms = ['a', 'b', 'c', 'd', 'e']
prefn = [('wangbo', ('a', 'd')),
         ('zhuzhuan', ('a', 'd')),
         ('shicong', ('a', 'e')),
         ('yehanjia', ('a', 'a')),
         ('bianmingliang', ('e', 'd')),
         ('guotou', ('c', 'a')),
         ('daitou', ('d', 'a')),
         ('erpang', ('c', 'b')),
         ('mucheng', ('d', 'd')),
         ('liuqing', ('e', 'a'))]

def dormcost(vec):
    cost = 0
    select_dorms = [dorms[i / 2] for i in range(len(dorms) * 2)]
    for people, v in enumerate(vec):
        first, second = prefn[people][1]
        real = select_dorms.pop(v)
        if first == real: pass
        elif second == real: cost += 1
        else: cost += 3
    return cost

def printsolution(vec):
    print 'cost:', dormcost(vec)
    select_dorms = [dorms[i / 2] for i in range(len(dorms) * 2)]
    for people, v in enumerate(vec):
        first, second = prefn[people][1]
        real = select_dorms.pop(v)
        like = 'first' if first == real else ('second' if second == real else 'unlike')
        print '%-15s in %s (%s select)' %(prefn[people][0], real, like)

# O(n!), n = len(len(dorms) * 2)
def best():
    domain_ranges = [range(i) for i in range(len(dorms) * 2, 0, -1)]
    return min(itertools.product(*domain_ranges), key=dormcost)

def test():
    domains = [(0, i) for i in range(len(dorms) * 2 - 1, -1, -1)]

    cost, result = genetic_optimize(domains, dormcost)
    print '\ngenetic:'
    printsolution(result)

    cost, result = annealing_optimize(domains, dormcost, cooldown=0.99)
    print '\nannealing:'
    printsolution(result)

    cost, result = multi_hillclimb_optimize(domains, dormcost)
    print '\nhillclimb:'
    printsolution(result)

#   print '\nbest'
#   result = best()
#   printsolution(result)

if __name__ == '__main__':
    test()
