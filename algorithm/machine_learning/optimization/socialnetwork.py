import optimization as op
from PIL import Image, ImageDraw
import itertools
import math

def chunk(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

people = ['x', 'a', 'b', 'c', 'd']
links = [('x', i) for i in people[1:]] + [(x, y) for x, y in chunk(people[1:], 2)]

# Is p above ab?
# return False if p in ab
def isup((ax, ay), (bx, by), (px, py)):
    if ax == bx: return px < ax    # left as above
    # line: y = k(x - ax) + ay
    k = float(by - ay) / (bx - ax)
    return py > (k * (px - ax) + ay)

def iscross((a, b), (c, d)):
    return (isup(a, b, c) ^ isup(a, b, d)) and (isup(c, d, a) ^ isup(c, d, b))

def distance((ax, ay), (bx, by)):
    return math.sqrt((ax - bx) ** 2 + (ay - by) ** 2)

def linkcost(vec):
    location = dict([(p, loc) for p, loc in zip(people, chunk(vec, 2))])    # {people:(x, y)}

    # point-to-point tention
    distances = [distance(location[a], location[b]) for a, b in itertools.combinations(people, 2)]
    maxd = max(distances)
    mind = max(distances)
    dcost = sum([1 - d / maxd for d in distances])

    # link pull
    linkpull = 0.0
    for (a, b) in links:
        d = distance(location[a], location[b])
        linkpull += d / mind

    cross = 0
    for (a, b), (c, d) in itertools.combinations(links, 2):
        # duplicate points
        if a in (c, d) or b in (c, d): continue
        # determin whether ab is cross with cd
        if iscross((location[a], location[b]), (location[c], location[d])):
            cross += 1

    return float(cross) + dcost + linkpull

def draw_network(width=400, height=400, opf=op.random_optimize):
    # get answers
    domains = [(height / 10, height * 9/ 10) if i & 1 else (width / 10, width * 9 / 10)
               for i in range(len(people) * 2)]
    cost, vecs = opf(domains, linkcost)

    # draw background
    img = Image.new('RGB', (width, height), (255, 255, 255))
    draw = ImageDraw.Draw(img)
    # draw peoples
    location = {}
    for p, (x, y) in zip(people, chunk(vecs, 2)):
        draw.text((x, y), p, (0, 0, 0))
        location[p] = (x, y)

    # draw link
    for (a, b) in links:
        draw.line((location[a], location[b]), fill=(255, 0, 0))

    img.save('%s.jpg' %opf.func_name, 'JPEG')


def test_iscross():
    print iscross(((-1, -1), (1, 1)), ((-1, 0), (1, 0)))
    print iscross(((0, 1), (0, 2)), ((-1, 0), (1, 0)))
    print iscross(((0, 1), (0, 2)), ((0, 1), (1, 0)))

def test():
    draw_network()
    draw_network(opf=op.genetic_optimize)

if __name__ == '__main__':
    test()
