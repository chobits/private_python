import time
import random
import math

# get flight infomation:
# $ wget http://kiwitobes.com/optimize/schedule.txt
people = [('Seymour', 'BOS'),
          ('Franny', 'DAL'),
          ('Zooey', 'CAK'),
          ('Walt', 'MIA'),
          ('Buddy', 'ORD'),
          ('Les', 'OMA')]

destination = 'LGA'
debug = False

def chunck(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

def getminutes(t):
    ts = time.strptime(t, '%H:%M')
    return ts.tm_hour * 60 + ts.tm_min

# {(src, dst):(src_time, dst_time, price)}
def get_fligtinfo(filename):
    f = open(filename)
    flights = {}
    for line in f:
        src, dst, srctime, dsttime, price = line.split(',')
        flights.setdefault((src, dst), [])
        flights[(src, dst)].append((srctime, dsttime, int(price)))
    f.close()
    return flights
flights = get_fligtinfo('schedule.txt')

def schedule_cost(r, flights=flights):
    assert len(r) & 1 == 0
    cost = 0
    lastest_arrival = 0
    earliest_departure = 24 * 60
    dst = destination
    for (_, src), (go, back) in zip(people, chunck(r, 2)):
        flight_go = flights[(src, dst)][go]
        flight_back = flights[(dst, src)][back]
        # go-back ticket price
        cost += flight_go[2] + flight_back[2]

        # wait time -> money
        atime = getminutes(flight_go[1])
        dtime = getminutes(flight_back[0])
        cost -= atime
        cost += dtime
        if atime > lastest_arrival: lastest_arrival = atime
        if dtime < earliest_departure: earliest_departure = dtime

    # caculate wait time money
    cost += lastest_arrival * (len(r) / 2)
    cost -= earliest_departure * (len(r) / 2)
    # another day money for rent car
    if lastest_arrival > earliest_departure: cost += 50
    return cost

# random searching
def random_optimize(domain, costf, iterations=1000):
    cost = 9999999
    result = None
    for i in range(iterations):
        f = [random.randint(*d) for d in domain]
        c = costf(f)
        if c < cost:
            cost = c
            result = f
    return cost, result

# using neighbour of random select best answers
def hillclimb_optimize(domain, costf):
    f = [random.randint(*d) for d in domain]
    cost = 9999999
    best = f
    while True:
        current = costf(f)
        for i, (v, (minv, maxv)) in enumerate(zip(f, domain)):
            # -1 neighbour
            if v > minv:
                f[i] = v - 1
                c = costf(f)
                if c < cost:
                    cost = c
                    best = list(f)
            # +1 neighbour
            if v < maxv:
                f[i] = v + 1
                c = costf(f)
                if c < cost:
                    cost = c
                    best = list(f)
            # restore f
            f[i] = v
        if current == cost: break
        f = best
    return cost, best

def multi_hillclimb_optimize(domain, costf, iterations=20):
    results = [hillclimb_optimize(domain, costf) for i in range(iterations)]    # [(cost, flights)]
    return min(results)

# T is cool down to T*0.95 every time
def annealing_optimize(domain, costf, T=10000.0, cooldown=0.95, step=1):
    vec = [random.randint(*d) for d in domain]
    while T > 0.1:
        # create newvec
        i = random.randint(0, len(vec) - 1)
        newvec = vec[:]
        newvec[i] += random.randint(-step, step)
        newvec[i] = max(min(domain[i][1], newvec[i]), domain[i][0])

        cost = costf(vec)
        costnew = costf(newvec)
        if costnew < cost:
            vec, cost = newvec, costnew
        else:
            # we have p probability to select worst one!
            # Note pow(e, 800+) will cause overflow,
            # so when costnew < cost we should not caculate p!
            p = pow(math.e, -(costnew - cost) / T)
            if random.random() < p:
                vec, cost = newvec, costnew
        T *= cooldown
    return cost, vec

def randindex(seq):
    return random.randint(0, len(seq) - 1)

def genetic_optimize(domain, costf, popsize=50, survival_rate=0.2, mutation_rate=0.2, generations=100, step=1):

    def mutate(vec):
        i = randindex(vec)
        vec[i] += random.randint(-step, step) or 1
        if vec[i] > domain[i][1]: vec[i] = domain[i][0]
        elif vec[i] < domain[i][0]: vec[i] = domain[i][1]

    def random_mutation(pops):      # popsize = popsize
        mutate(pops[randindex(pops)])

    def random_gen_mutation(pops):  # popsize += 1
        pops.append(pops[randindex(pops)][:])
        mutate(pops[-1])

    def child_mutation(pops):       # popsize += 1
        child = cross(pops)
        mutate(child)
    mutation = random_mutation

    def cross(pops):                # popsize += 1
        if len(pops) == 1:
            pops.append(pops[0][:])
            return pops[1]
        # how to select parents?
        # p, q = pops[0], pops[1] # best parents
        p, q = pops[randindex(pops)], pops[randindex(pops)]  # random parents
        i = random.randint(0, len(p))
        child = p[0:i] + q[i:]
        pops.append(child)
        return child

    pops = [[random.randint(*d) for d in domain] for _ in range(popsize)]
    nsurvival = int(popsize * survival_rate)
    for i in range(generations):
        pops.sort(key=costf)
        if debug: print costf(pops[0]), pops[0]
        nextpops = pops[0:nsurvival]
        while len(nextpops) < popsize:
            if random.random() < mutation_rate: # mutation
                mutation(nextpops)
            else:                               # cross reproduction
                cross(nextpops)
        pops = nextpops
    pops.sort(key=costf)
    return costf(pops[0]), pops[0]

def test():
#   s = [1, 4, 3, 2, 7, 3, 6, 3, 2, 4, 5, 3]
#   print schedule_cost(s)
#   print random_optimize([(1, 9)] * (len(people) * 2), schedule_cost, 1000)
#   print hillclimb_optimize([(1, 9)] * (len(people) * 2), schedule_cost, 5)
#   print multi_hillclimb_optimize([(1, 9)] * (len(people) * 2), schedule_cost, 5)
#   print annealing_optimize([(1, 9)] * (len(people) * 2), schedule_cost)
    print genetic_optimize([(1, 9)] * (len(people) * 2), schedule_cost, step=3)

if __name__ == '__main__':
    test()
