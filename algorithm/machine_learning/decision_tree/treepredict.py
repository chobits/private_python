from math import log
from PIL import Image, ImageDraw

def get_info():
    f = open('decision_tree_example.txt')
    data = [[a, b, c, int(d), e] for a, b, c, d, e in [line.rstrip().split('\t') for line in f]]
    f.close()
    return data

def unique_counts(rows):
    results = {}
    for row in rows:
        r = row[-1]
        results.setdefault(r, 0)
        results[r] += 1
    return results

def divide_set(rows, column, value):
    if isinstance(value, int) or isinstance(value, float):
        split_func = lambda row:row[column] >= value
    else:
        split_func = lambda row:row[column] == value
    set1 = [row for row in rows if split_func(row)]
    set2 = [row for row in rows if not split_func(row)]
    return (set1, set2)

# P = loop pi(1-pi)
#   = loop pi - loop pi^2
#   = 1 - (loop count[i]^2) / total^2
def giniimpurity(rows):
    uc = unique_counts(rows)
    impurity = 1 - float(sum([c ** 2 for c in uc.itervalues()])) / (len(rows) ** 2)
    return impurity

def entroby(rows):
    if not rows: return 0.0
    uc = unique_counts(rows)
    e = -sum([c * log(float(c) / len(rows), 2) for c in uc.itervalues()]) / len(rows)
    return e

def prune_entroby(uc):
    n = sum(uc.itervalues())
    return -sum([c * log(float(c) / n, 2) for c in uc.itervalues()]) / n

# variance = (sum (xi - x_mean)^2) = (sum xi^2) - n * x_mean^2
def variance(rows):
    if not rows: return 0
    values = [row[-1 ]for row in rows]
    sumv = float(sum(values))
    sumsq = sum([v * v for v in values])
    return sumsq - sumv * sumv / len(rows)

class desitionnode:
    def __init__(self, column=None, value=None, tb=None, fb=None, results=None):
        self.column = column    # detect whether row[column] >= value
        self.value = value
        self.tb = tb            # true branch
        self.fb = fb            # false branch
        self.results = results

# build new branchs which makes new score is smaller than current
def build_desitiontree(rows, scoref=entroby):
    assert len(rows) > 0
    n = len(rows)
    score = scoref(rows)            # current score
    gain = 0.0
    # find (best column, best value) to divide the rows
    for col in range(len(rows[0]) - 1):
        values = set([row[col] for row in rows])
        for value in values:
            set1, set2 = divide_set(rows, col, value)
            newgain = score - float(len(set1) * scoref(set1) + len(set2) * scoref(set2)) / len(rows)
            if newgain > gain:
                assert len(set1) > 0 and len(set2) > 0
                gain = newgain
                tb_row, fb_row = set1, set2
                bestcol, bestvalue = col, value
    if gain > 0:
        tb = build_desitiontree(tb_row)
        fb = build_desitiontree(fb_row)
        return desitionnode(bestcol, bestvalue, tb, fb)
    else:
        return desitionnode(results=unique_counts(rows))

# non recursive implementation
def classify(row, node):
    n = node
    while not n.results:
        col, value = n.column, n.value
        if isinstance(value, int) or isinstance(value, float):
            istrue = row[col] >= value
        else:
            istrue = row[col] == value
        n = n.tb if istrue else n.fb
    return n.results

def mdclassify(row, node):
    if node.results: return node.results
    col, value = node.column, node.value

    # predict it, run two branches
    if row[col] == None:
        tr, fr = mdclassify(row, node.tb), mdclassify(row, node.fb)
        ntr, nfr = sum(tr.values()), sum(fr.values())
        ptr = float(ntr) / (ntr + nfr)      # left result weight
        pfr = 1 - ptr                       # right result weight
        # cannot using tr as result, tr is in leave node
        result = dict([(k, v * ptr) for k, v in tr.iteritems()])
        for k, v in fr.iteritems():
            result.setdefault(k, 0)
            result[k] += pfr * v
        return result

    # only run one branch
    if isinstance(value, int) or isinstance(value, float):
        istrue = row[col] >= value
    else:
        istrue = row[col] == value
    return mdclassify(row, node.tb) if istrue else mdclassify(row, node.fb)


def get_width(node):
    width = 1
    # left
    n = node
    while not n.results:
        n = n.tb
        width += 1
    # right
    n = node
    right_width = 0
    while not n.results:
        n = n.fb
        width += 1
    return width

def get_depth(node):
    if node.results: return 1
    return max(get_depth(node.tb), get_depth(node.fb)) + 1

def draw_node(draw, node, x, y, size):
    if node.results:
        nx, ny = x + size / 2, y + size / 2
        draw.text((nx, ny), '%s' %repr(node.results), (0, 0, 0))
        return (nx, ny)

    # draw node
    nx, ny = x + get_width(node.tb) * size, y
    draw.text((nx, ny), 'Is %s(%d)?' %(repr(node.value), node.column), (0, 0, 0))

    # draw branch nodes
    lnx, lny = draw_node(draw, node.tb, x, y + size, size)
    rnx, rny = draw_node(draw, node.fb, nx, y + size, size)

    # draw branch lines
    draw.line((nx, ny, lnx, lny), fill=(255, 0, 0))
    draw.line((nx, ny, rnx, rny), fill=(255, 0, 0))
    return (nx, ny)

def draw_desitiontree(node, size=100, jpeg='desitiontree.jpg'):
    width = size * (get_width(node) + 2)
    height = size * (get_depth(node) + 1)
    img = Image.new('RGB', (width, height), (255, 255, 255))
    draw = ImageDraw.Draw(img)
    draw.text((size / 4, size / 4), 'Left branch is true.', (0, 0, 255))
    draw_node(draw, node, 0, size / 2, size)
    img.save(jpeg, 'JPEG')

def print_tree(node, depth=0, istrue=1):
    if depth: print '  ' * depth,
    print '%s>' %('T' if istrue else 'F'),
    if node.results:
        print node.results
        return
    print '%s:%s?' %(node.column, node.value)
    print_tree(node.tb, depth + 1)
    print_tree(node.fb, depth + 1, 0)

def join_results(r1, r2):
    results = {}
    for k, v in r1.iteritems():
        results.setdefault(k, 0)
        results[k] += v
    for k, v in r2.iteritems():
        results.setdefault(k, 0)
        results[k] += v
    return results

def prune(node, mingain, scoref=prune_entroby):
    if node.results: return
    if node.tb: prune(node.tb, mingain)
    if node.fb: prune(node.fb, mingain)
    if node.tb.results and node.fb.results:
        results = join_results(node.tb.results, node.fb.results)
        score = scoref(results)
        tn, fn = sum(node.tb.results.values()), sum(node.fb.results.values())
        newscore = (tn * scoref(node.tb.results) + fn * scoref(node.fb.results)) / (tn + fn)
        if score - newscore < mingain:
            node.results = results
            node.tb = node.fb = node.col = node.value = None

def test_draw_prune():
    rows = get_info()
    # orignal tree
    treenode = build_desitiontree(rows)
    draw_desitiontree(treenode)
    # pruned tree
    prune(treenode, 1.0)
    print_tree(treenode)
    draw_desitiontree(treenode, jpeg='prune.jpg')

def test_mdclassify():
    rows = get_info()
    tree = build_desitiontree(rows)
    print mdclassify(['google', None, None, None], tree)
    print mdclassify(['google', 'France', 'yes', None], tree)
    print mdclassify(['google', None, 'yes', None], tree)

if __name__ == '__main__':
#   test_draw_prune()
    test_mdclassify()
