import re
import math

splitter = re.compile(r'\W+')

# classifier
class classifier:
    def __init__(self, fgetfeature, filename=None):
        self.fc = {}    #   {feature:{category:count}}
        self.cc = {}    #   {category:count}
        self.getfeature = fgetfeature
    def incf(self, feature, cate):
        self.fc.setdefault(feature, {})
        self.fc[feature].setdefault(cate, 0)
        self.fc[feature][cate] += 1
    def incc(self, cate):
        self.cc.setdefault(cate, 0)
        self.cc[cate] += 1
    def train(self, input_data, cate):
        features = self.getfeature(input_data)
        for feature in features:
            self.incf(feature, cate)
        self.incc(cate)
    def feature_count(self, feature, cate):
        if feature not in self.fc or cate not in self.fc[feature]: return 0
        return self.fc[feature][cate]
    def cate_count(self, cate):
        return self.cc[cate] if cate in self.cc else 0
    def cates(self):
        return self.cc.keys()
    def cate_totalcount(self):
        return sum(self.cc.values())
    # P(feature|category)
    # probability of cate containing feature
    def fprob(self, feature, cate):
        return float(self.feature_count(feature, cate)) / self.cc[cate]
    def ffprob(self,feature, cate, fprob, weight=1, assumeprob=0.5):
        prob = fprob(feature, cate)
        count = sum([self.feature_count(feature, c) for c in self.cates()])
        # ???
        return (weight * assumeprob + count * prob) / (weight + count)

# naive bayes classifier
class naivebayes(classifier):
    def __init__(self, *args, **kwargs):
        classifier.__init__(self, *args, **kwargs)
        self.thresholds = {}
    def setthreshold(self, cate, threshold):
        self.thresholds[cate] = threshold
    # default 1.0
    def getthreshold(self, cate):
        return self.thresholds[cate] if cate in self.thresholds else 1.0
    # P(document|category)
    # probability of category containing document
    def docprob(self, doc, cate):
        features = self.getfeature(doc)
        p = 1.0
        for feature in features:
            p *= self.ffprob(feature, cate, self.fprob)
        return p
    # P(category|document) = P(category&document) / P(document)
    #                      = P(document|category) * P(category) / P(document)
    # probability of document being category
    def cateprob(self, doc, cate):
        pass
    # P(category&document) = P(document|category) * P(category) 
    def prob(self, doc, cate):
        p_cate = float(self.cate_count(cate)) / self.cate_totalcount()
        return self.docprob(doc, cate) * p_cate
    def classify(self, item, default=None):
        p_cates = [(self.prob(item, cate), cate) for cate in self.cates()]
        p_bestcate, bestcate = max(p_cates)
        for p_cate, cate in p_cates:
            if bestcate == cate: continue
            if p_bestcate < p_cate * self.getthreshold(bestcate): return default
        return bestcate
 

class fisherclassifier(classifier):
    def __init__(self, *args, **kwargs):
        classifier.__init__(self, *args, **kwargs)
        self.minimums = {}

    def setminimum(self, cate, minv):
        self.minimums[cate] = minv

    def getminimum(self, cate):
        return self.minimums[cate] if cate in self.minimums else 0.0
    #       P(feature|cate)
    # CP = ------------------------------------------------------------
    #      P(feature|cate1) + P(feature|cate2) + ... + P(feature|cateN)
    #
    # P =  P(cate|feature)
    # Why P is not accurate?
    #   Consider we have two categories: 'good' and 'bad' and a featue: 'money'
    #   1000000000000 'good' items contains 10000 'money'.
    #   100 'bad' items has 100 'money'(every one has one).
    #   then P('bad'|'money') = 100 / (10000 + 100) ~= 0.01, which is not accurate
    #
    # So use CP instead of P.
    def cprob(self, feature, cate):
        pfc = self.fprob(feature, cate)
        if pfc == 0: return 0
        pfc_sum = sum([self.fprob(feature, c) for c in self.cates()])
        return pfc / pfc_sum

    def invchi2(self, chi, df):
        m = chi / 2.0
        sum = term = math.exp(-m)
        for i in range(1, df // 2):
            term *= m / i
            sum += term
        return min(sum, 1.0)

    def fisherprob(self, doc, cate):
        p = 1.0
        features = self.getfeature(doc)
        for feature in features:
            p *= self.ffprob(feature, cate, self.cprob)
        fscore = -2 * math.log(p)
        return self.invchi2(fscore, len(features) * 2)

    def classify(self, doc, default=None):
        pcates = [(self.fisherprob(doc, cate), cate) for cate in self.cates()]
        bestcate = default
        pbest = 0
        for p, cate in  pcates:
            if p > self.getminimum(cate) and p > pbest:
                pbest, bestcate = p, cate
        return bestcate

def getwords(string):
    return set([c.lower() for c in splitter.split(string) if c != ''])

def test_train(cl):
    cl.train('Nobody owns the water.', 'good')
    cl.train('the quick rabbit jumps fences', 'good')
    cl.train('buy pharmaceuticals now', 'bad')
    cl.train('make quick money at the online casino', 'bad')
    cl.train('the quick brown fox jumps', 'good')

def test_cl():
    cl = classifier(getwords)
    test_train(cl)
    print cl.ffprob('money', 'good', cl.fprob)
    test_train(cl)
    print cl.ffprob('money', 'good', cl.fprob)
    test_train(cl)
    print cl.ffprob('money', 'good', cl.fprob)

def test_naviebayes():
    nb = naivebayes(getwords)
    test_train(nb)
    print nb.classify('quick rabbit', default='unknown')
    print nb.classify('quick money', default='unknown')
    nb.setthreshold('bad', 3.0)
    print nb.classify('quick rabbit', default='unknown')
    print nb.classify('quick money', default='unknown')
    for i in range(10): test_train(nb)
    print nb.classify('quick rabbit', default='unknown')
    print nb.classify('quick money', default='unknown')

def test_f():
    c = fisherclassifier(getwords)
    test_train(c)
#   print c.cprob('quick', 'good')
#   print c.cprob('money', 'bad')
#   print c.ffprob('money', 'bad', c.cprob)

#   print c.fisherprob('quick rabbit', 'good')
#   print c.fisherprob('quick rabbit', 'bad')
    print c.classify('quick rabbit')
    print c.classify('quick money')
    c.setminimum('bad', 0.8)
    print c.classify('quick money')
    c.setminimum('good', 0.4)
    print c.classify('quick money')

if __name__ == '__main__':
#   test_cl()
#   test_naviebayes()
    test_f()
    
