from math import tanh
from itertools import izip
import sqlite3 as sqlite

def dtanh(y):
    return 1.0 - y * y

class searchnet:
    def __init__(self, dbname):
        self.dbcon = sqlite.connect(dbname)
    def __del__(self):
        self.dbcon.close()

    def maketables(self):
        self.dbcon.execute('create table hiddennode(create_key)')
        self.dbcon.execute('create table wordhidden(fromid, toid, strength)')   # word layer   -> hidden layer
        self.dbcon.execute('create table hiddenurl(fromid, toid, strength)')    # hidden layer -> url layer
        self.dbcommit()

    def getstrength(self, fromid, toid, layer):
        assert layer in [0, 1]
        table = 'wordhidden' if layer == 0 else 'hiddenurl'
        res = self.dbcon.execute('select strength from %s where fromid=%d and toid=%d' %(table, fromid, toid)).fetchone()
        if res: return res[0]
        return -0.2 if layer == 0 else 0

    def setstrength(self, fromid, toid, layer, strength):
        assert layer in [0, 1]
        table = 'wordhidden' if layer == 0 else 'hiddenurl'
        res = self.dbcon.execute('select rowid from %s where fromid=%d and toid=%d' %(table, fromid, toid)).fetchone()
        if res == None:
            self.dbcon.execute('insert into %s(fromid, toid, strength) values(%d,%d,%f)' %(table, fromid, toid, strength))
        else:
            self.dbcon.execute('update %s set strength=%f where rowid=%d' %(table, strength, res[0]))

    # e.g:
    # (w1,w2), (u1,u2,u3)
    #  w1--.                     .--> u1
    #      +-> new hidden node --+--> u2
    #  w2--'                     `--> u3
    # generate only one hiddennode (privary key: (w1, w2))
    def generate_hiddennode(self, wordids, urlids):
        nword = len(wordids)
        if nword > 3: return
        create_key = '_'.join(sorted([str(wordid) for wordid in wordids]))
        res = self.dbcon.execute("select rowid from hiddennode where create_key='%s'" %create_key).fetchone()
        if res: return
        hiddennodeid = self.dbcon.execute("insert into hiddennode(create_key) values('%s')" %create_key).lastrowid
        for wordid in wordids:
            self.setstrength(wordid, hiddennodeid, 0, 1.0 / nword)
        for urlid in urlids:
            self.setstrength(hiddennodeid, urlid, 1, 0.1)
        self.dbcommit()

    def getallhiddens(self, wordids, urlids):
        hiddens = set()
        for wordid in wordids:
            for (toid,) in self.dbcon.execute('select toid from wordhidden where fromid=%d' %wordid):
                hiddens.add(toid)
        for urlid in urlids:
            for (fromid,) in self.dbcon.execute('select fromid from hiddenurl where toid=%d' %urlid):
                hiddens.add(fromid)
        return list(hiddens)

    def setup_network(self, wordids, urlids):
        self.wordids = wordids
        self.hiddenids = self.getallhiddens(wordids, urlids)
        self.urlids =urlids

        # node ouputs
        self.wo = [1.0] * len(self.wordids)
        self.ho = [1.0] * len(self.hiddenids)
        self.uo = [1.0] * len(self.urlids)

        # weights
        self.wh = [[self.getstrength(wordid, hiddenid, 0) for hiddenid in self.hiddenids] for wordid in self.wordids]
        self.hu = [[self.getstrength(hiddenid, urlid, 1) for urlid in self.urlids] for hiddenid in self.hiddenids]

    # caculate url output according to words
    def feedforward(self):
        # matrix wordoutput
        #        [1 x nword]
        for i in range(len(self.wordids)): self.wo[i] = 1.0
        
        #   matrix wordoutput * matrix word-hidden-weight
        #         [1 x nword]   [nword x nhidden]
        # = matrix hiddenoutput
        #         [1 x nhidden]
        for i in range(len(self.hiddenids)):
            self.ho[i] = tanh(sum([self.wh[j][i] * wo for j, wo in enumerate(self.wo)]))

        #   matrix hiddenoutput * matrix hidden-url-weight
        #         [1 x nhidden]   [nhidden x nurl]
        # = matrix urloutput
        #         [1 x nurl]
        for i in range(len(self.urlids)):
            self.uo[i] = tanh(sum([self.hu[j][i] * ho for j, ho in enumerate(self.ho)]))

        return self.uo[:]

    # back propagration
    def feedback(self, feedback_uo, rate=0.5):
        # caculate url output deltas
        url_deltas = [(fu - u) * dtanh(u) for u, fu in izip(self.uo, feedback_uo)]

        # caculate hidden according to url output deltas
        hidden_deltas = [sum([x * y for x, y in izip(weights, url_deltas)])
            for weights in self.hu]

        # update word->hidden weights
        for i, (wo, weights) in enumerate(izip(self.wo, self.wh)):
            for j, weight in enumerate(weights):
                self.wh[i][j] += wo * hidden_deltas[j] * rate   # use weight instead of wo??

        # update hidden->url weights
        for i, (ho, weights) in enumerate(izip(self.ho, self.hu)):
            for j, hu in enumerate(weights):
                self.hu[i][j] += ho * url_deltas[j] * rate

    def trainquery(self, wordids, urlids, select_urlid):
        self.generate_hiddennode(wordids, urlids)
        self.setup_network(wordids, urlids)
        self.feedforward()
        feedback_urlids = [0.0] * len(urlids)
        feedback_urlids[urlids.index(select_urlid)] = 1.0
        self.feedback(feedback_urlids)
        self.updatedb()

    def getresult(self, wordids, urlids):
        self.setup_network(wordids, urlids)
        return self.feedforward()

    def updatedb(self):
        for wordid, weights in izip(self.wordids, self.wh):
            for hiddenid, weight in izip(self.hiddenids, weights):
                self.setstrength(wordid, hiddenid, 0, weight)
        for hiddenid, weights in izip(self.hiddenids, self.hu):
            for urlid, weight in izip(self.urlids, weights):
                self.setstrength(hiddenid, urlid, 1, weight)

    def dbcommit(self):
        self.dbcon.commit()


if __name__ == '__main__':
    net = searchnet('nn.db')
    net.maketables()
