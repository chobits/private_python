import nn
import sys
import urllib2
import sqlite3 as sqlite
from BeautifulSoup import *
from urlparse import urljoin
from timetrack import timetracker
from itertools import izip
tt = timetracker()

destroy_sql_strings = ["'", '"']
def destroy_sql(word):
    for s in destroy_sql_strings:
        if s in word:
            return True
    return False
    
# querying
class sqlop:
    def fromnum(self, urlid):
        return self.dbcon.execute('select count(*) from link where toid=%d' %urlid).fetchone()[0]

    def tourlnum(self, urlid):
        return self.dbcon.execute('select count(*) from link where fromid=%d' %urlid).fetchone()[0]

    def fromurls(self, urlid):
        return [i for (i,) in self.dbcon.execute('select distinct fromid from link where toid=%d' %urlid)]

class searcher(sqlop):
    def __init__(self, dbname):
        self.dbcon = sqlite.connect(dbname)
        self.net = nn.searchnet('nn.db')

    def __del__(self):
        self.dbcon.close()

    def pagerank(self, urlid):
        res = self.dbcon.execute('select score from pagerank where urlid=%d' %urlid).fetchone()
        return res[0] if res else None

    def geturl(self, urlid):
        url = self.dbcon.execute('select url from urllist where rowid=%d'%urlid).fetchone()
        return url[0] if url else None
    def geturltitle(self, urlid):
        url = self.dbcon.execute('select url, title from urllist where rowid=%d'%urlid).fetchone()
        return url if url else None

    def print_pagerank(self, n=10):
        for i in range(n):
            url = self.geturl(i)
            pr = self.pagerank(i)
            if not url or not pr: continue
            print '%f: %s' %(self.pagerank(i), self.geturl(i))

    @tt
    # row = [urlid, w1_loc, w2_loc, ...]
    def nnscores(self, rows, wordids):
        urlids = list(set([row[0] for row in rows]))
        urlval = self.net.getresult(wordids, urlids)
        return self.normalize_score(dict(izip(urlids, urlval)))

    @tt
    def linktextscores(self, rows, wordids):
        scores = dict([(row[0], 0.0) for row in rows])
        for wordid in wordids:
            linkids = self.dbcon.execute('select fromid, toid from link, linkwords \
                where link.toid=linkwords.linkid and linkwords.wordid=%d' %wordid)
            for fromid, toid in linkids:
                if toid in scores: scores[toid] += self.pagerank(fromid)
        return self.normalize_score(scores)
            
    @tt
    def pagerankscores(self, rows):
        scores = dict([(row[0], self.pagerank(row[0])) for row in rows])
        return self.normalize_score(scores)

    # can be substituted by pagerankscores
    def isboundlinkscores(self, rows):
        urlids = set([row[0] for row in rows])
        scores = dict([(urlid,
            self.dbcon.execute('select count(*) from link where toid=%d' %urlid).fetchone()[0])
            for urlid in urlids])
        return self.normalize_score(scores)

    @tt
    def distancescores(self, rows):
        if len(rows[0]) <= 2: return dict([(row[0], 1) for row in rows])
        scores = dict([(row[0], 0) for row in rows])
        for row in rows:
            score = sum([abs(row[i - 1] - row[i]) for i in range(2, len(row))])
            if score < scores[row[0]] or scores[row[0]] == 0:
                scores[row[0]] = score
        return self.normalize_score(scores, small_is_better=True)

    @tt
    def locationscores(self, rows):
        scores = dict([(row[0], -1) for row in rows])
        for row in rows:
            score = sum(row[1:])
            if score < scores[row[0]] or scores[row[0]] == -1: scores[row[0]] = score
        return self.normalize_score(scores, small_is_better=True)

    @tt
    # return {urlid:score}
    def frequencyscores(self, rows):
        scores = dict([(row[0], 0) for row in rows])
        for row in rows: scores[row[0]] += 1
        return self.normalize_score(scores)

    # scores = {urlid:score}
    # make scores larger as better in [0, 1]
    def normalize_score(self, scores, small_is_better=False):
        if small_is_better:
            vmin = min(scores.values())
            return dict([(urlid, float(vmin) / max(score, 0.00001)) for urlid, score in scores.iteritems()])
        else:
            vmax = max(max(scores.values()), 0.00001)
            return dict([(urlid, float(score) / vmax) for urlid, score in scores.iteritems()])

    # return {urlid:score}
    def getscorelist(self, rows, wordids):
        scoredict = dict([(row[0], 0) for row in rows])
        weights = [
            (1.0, self.frequencyscores(rows)),
            (1.0, self.locationscores(rows)),
            (1.0, self.distancescores(rows)),
            (4.0, self.pagerankscores(rows)),
            (4.0, self.linktextscores(rows, wordids)),
            (4.0, self.nnscores(rows, wordids))]

        for (weight, scores) in weights:
            for urlid in scoredict:
                assert urlid in scores
                scoredict[urlid] += weight * scores[urlid]
        return scoredict

    def query(self, q, debug=False, n=10):
        rows, wordids = self.getmatchrows(q.lower())
        if not rows:
            if debug: print 'cannot found', q
            return []
        scores = self.getscorelist(rows, wordids)
        rankedscores = sorted([(score, urlid) for (urlid, score) in scores.iteritems()], reverse=True)
        if debug:
            for score, urlid in rankedscores[0:n]:
                print '%f\t%s' %(score, self.geturl(urlid))
        return rankedscores[0:n]

    def geturlinfo(self, urlids):
        return [(urlid, self.geturltitle(urlid)) for urlid in urlids]
    # find url html contains total words queried
    # return rows, where rows[i] = [url, 0th_word_location, 1st_word_location, ... ]
    #     wordids, where wordids[i] = ith_word_idx
    def getmatchrows(self, q):
        words = [word for word in q.split() if word != '']
        if not words: return None, None
        select_var = 'w0.urlid'
        from_var = ''
        where_var = ''
        searchwords = 0
        wordids = []

        for word in words:
            wordrow = self.dbcon.execute("select rowid from wordlist where word='%s'" %word).fetchone()
            if not wordrow: continue
            wordid = wordrow[0]

            select_var += ',w%d.location' %searchwords
            if searchwords > 0:
                from_var += ','
                where_var += ' and w%d.urlid=w%d.urlid and ' %(searchwords - 1, searchwords)
            from_var += 'wordlocation w%d' %searchwords
            where_var += 'w%d.wordid=%d' %(searchwords, wordid)

            searchwords += 1
            wordids.append(wordid)

        if searchwords > 0:
#           print 'select %s from %s where %s' %(select_var, from_var, where_var)
            cur = self.dbcon.execute('select %s from %s where %s' %(select_var, from_var, where_var))
            rows = list(cur)
            if rows: return rows, wordids
        return None, None

def print_page_rank():
    s = searcher('index.db')
    s.print_pagerank(10000)

def test_searcher():
    s = searcher('index.db')
    s.query(sys.argv[1], debug=True)
    tt.print_info()

if __name__ == '__main__':
#   create_db()
#   caculate_pr()
    test_searcher()
#   print_page_rank()
