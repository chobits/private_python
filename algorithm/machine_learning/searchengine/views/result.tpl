%include home.tpl

%if result:
  %for i, (score, url, title) in enumerate(result):
    <table cellpadding="2" cellspacing="5" id="{{i}}">
      <tbody><tr>
        <td width="10">{{'%.2f'%score}}</td>
        <td><a href="{{url}}" target="_blank">{{title}}</a></td>
      </tr></tbody>
    </table>
  %end
%else:
  <p>could not found</p>
%end

