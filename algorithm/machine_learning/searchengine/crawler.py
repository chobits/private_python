import sys
import urllib2
import sqlite3 as sqlite
from BeautifulSoup import *
from urlparse import urljoin
from itertools import izip

ignorewords = set(['', 'the', 'of', 'to', 'and', 'a', 'in', 'is', 'it'])
destroy_sql_strings = ["'", '"']
def destroy_sql(word):
    for s in destroy_sql_strings:
        if s in word:
            return True
    return False

def getsoup(url):
    try:
        c = urllib2.urlopen(url)
    except:
        print '- cound not open %s' %url
        return None
    return BeautifulSoup(c.read())

# string operations
splitter = re.compile(r'\W*')   # complement of [a-zA-Z0-9_]*
def separatewords(text):
    return [s.lower() for s in splitter.split(text) if s != '']

# @link is soup link object
# this function return strict url format: 'http://...'
# return None if error
def geturl_fromlink(link, baseurl):
    if link.has_key('href'):            # dont use `'href' not in link`
        url = urljoin(baseurl, link['href']).split('#')[0]  # drop frag
        if url[0:4] == 'http' and not destroy_sql(url):
            return url
    return None

# get title
def gettitle(soup, url=None):
    try: titletag = soup.html.head.title
    except: return url
    return titletag.string.strip() if titletag else url

# get text-type html
def gettextonly(soup):
    # for BeatifulSoup.NavigableString
    if soup.string: return soup.string.strip()

    # for BeautifulSoup
    text = ''
    for t in soup.contents:
        text += gettextonly(t) + '\n'
    return text

# querying
class sqlop:
    def fromnum(self, urlid):
        return self.dbcon.execute('select count(*) from link where toid=%d' %urlid).fetchone()[0]

    def tourlnum(self, urlid):
        return self.dbcon.execute('select count(*) from link where fromid=%d' %urlid).fetchone()[0]

    def fromurls(self, urlid):
        return [i for (i,) in self.dbcon.execute('select distinct fromid from link where toid=%d' %urlid)]

# crawler
class crawler(sqlop):
    def __init__(self, dbname):
        self.dbcon = sqlite.connect(dbname)

    def __del__(self):
        self.dbcon.close()

    def dbcommit(self):
        self.dbcon.commit()

    # get entry id (create a new entry if not exist)
    def getentryid(self, table, field, value):
        res = self.dbcon.execute("select rowid from %s where %s='%s'" %(table, field, value)).fetchone()
        if res: return res[0]
        cur = self.dbcon.execute("insert into %s (%s) values ('%s')" %(table, field, value))
        return cur.lastrowid

    def addtoindex(self, url, soup):
        if self.isindexed(url): return False
        text = gettextonly(soup)
        title = gettitle(soup, url=url)
        print '+ Indexing %s (%s)' %(url, title)
        words = separatewords(text)            # maybe []   >>>
        urlid = self.getentryid('urllist', 'url', url)
        self.dbcon.execute("update urllist set title='%s' where rowid=%d" %(title, urlid))

        for i, word in enumerate(words):
            if word in ignorewords: continue
            wordid = self.getentryid('wordlist', 'word', word)
            self.dbcon.execute('insert into wordlocation(urlid, wordid, location) \
                values (%d, %d, %d)' %(urlid, wordid, i))
        return True

    def isindexed(self, url):
        res = self.dbcon.execute("select rowid from urllist where url='%s'" %url).fetchone()
        if res != None:
            res = self.dbcon.execute("select * from wordlocation where urlid=%d" %res[0]).fetchone()
            if res != None: return True
            # maybe there is no word in this url html            <<<
        return False

    # link is in urlfrom html
    # link address = urlto, link value = lintext
    def addlinkref(self, urlfrom, urlto, linktext):
        fromid = self.getentryid('urllist', 'url', urlfrom)
        toid = self.getentryid('urllist', 'url', urlto)
        linkid = self.dbcon.execute('insert into link(fromid, toid) \
            values(%d, %d)' %(fromid, toid)).lastrowid
        for word in separatewords(linktext):
            if word in ignorewords: continue
            wordid = self.getentryid('wordlist', 'word', word)
            self.dbcon.execute('insert into linkwords(wordid, linkid) \
                values(%d, %d)' %(wordid, linkid))

    def crawl(self, pages, depth=2, max_pages=1000):
        pages = set(pages)
        stat_pages = 0
        for i in range(depth):
            newpages = set()
            for page in pages:  # page are url
                # get connection
                soup = getsoup(page)
                if not soup or not soup.html or not self.addtoindex(page, soup): continue

                # end?
                stat_pages += 1
                if max_pages > 0 and stat_pages >= max_pages: return stat_pages
    
                # search adjacent pages of page
                for link in soup('a'):
                    url = geturl_fromlink(link, page)
                    if url:
                        if not self.isindexed(url): newpages.add(url)
                        self.addlinkref(page, url, gettextonly(link))
                self.dbcommit()
            if not newpages: break
            pages = newpages
        return stat_pages

    def caculate_pagerank(self, iterations=20):
        self.dbcon.execute('drop table if exists pagerank')
        self.dbcon.execute('create table pagerank(urlid primary key, score)')

        # recaculate pagerank values (1.0 initially)
        self.dbcon.execute('insert into pagerank select rowid, 1.0 from urllist')
        self.dbcommit()

        # WARNING: ranks, newranks and stats cost large memory!
        ranks = dict(self.dbcon.execute('select urlid, score from pagerank'))
        # {urlid:(tourl_number, fromurl_ids)}
        stats = dict([(urlid, (self.tourlnum(urlid), self.fromurls(urlid))) for urlid in ranks])
        print 'caculate pagerank value for %d pages' %len(ranks)

        for time in range(iterations):
            print 'pageranking %d/%d' %(time + 1, iterations)
            for urlid in ranks:
                prs = [float(ranks[fromid]) / stats[fromid][0] for fromid in stats[urlid][1]]
                ranks[urlid] = 0.15 + 0.85 * sum(prs)
        for urlid, pr in ranks.iteritems():
            self.dbcon.execute('update pagerank set score=%f where urlid=%d' %(pr, urlid))
        self.dbcommit()

    def createindextables(self):
        self.dbcon.execute('create table if not exists urllist(url, title)')
        self.dbcon.execute('create table if not exists wordlist(word)')
        # word(wordid) in which url(urlid) html / location-th word of words in url
        self.dbcon.execute('create table if not exists wordlocation(urlid, wordid, location)')
        self.dbcon.execute('create table if not exists link(fromid integer, toid integer)')
        self.dbcon.execute('create table if not exists linkwords(wordid, linkid)')

        self.dbcon.execute('create index if not exists urlidx on urllist(url)')
        self.dbcon.execute('create index if not exists wordidx on wordlist(word)')
        self.dbcon.execute('create index if not exists wordurlidx on wordlocation(wordid)')
        self.dbcon.execute('create index if not exists urltodix on link(toid)')
        self.dbcon.execute('create index if not exists urlfromidx on link(fromid)')
        self.dbcommit()

def create_db():
    c = crawler('index.db')
    pages = ['http://kiwitobes.com/wiki/']
    print 'create database table'
    c.createindextables()
    print 'crawling...'
    c.crawl(pages, max_pages=100)

def caculate_pr():
    c = crawler('index.db')
    print 'pageranking...'
    c.caculate_pagerank(20)
    print 'pagerank ok'

def print_pages():
    dbcon = sqlite.connect('index.db')
    cur = dbcon.execute('select rowid, url, title from urllist')
    for urlid, url, title in cur: 
        print urlid, url, title
    dbcon.close()

if __name__ == '__main__':
#   create_db()
    caculate_pr()
#   print_pages()
