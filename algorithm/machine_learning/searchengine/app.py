import bottle
import searchengine
from itertools import izip

searcher = None

@bottle.route('/click')
def feedback_click():
    #wordids, urlids, urlid = 
    searcher.net.trainquery(wordids, urlids, urlid)

@bottle.route('/search')
def search_query():
    query = bottle.request.query.key
    if not query: bottle.redirect('/')
    scores = searcher.query(query)  # [(score, urlid)]
    if len(scores) > 0:
        info = searcher.geturlinfo([urlid for (score, urlid) in scores])  # [(urlid, (url, title))]
        result = [(score, url, title) for (score, _), (__, (url, title)) in izip(scores, info)]
    else:
        result = []
    return bottle.template('result', result=result)

@bottle.route('/', moethod='GET')
def homepage():
    return bottle.template('home')

def init_searchengine():
    global searcher
    searcher = searchengine.searcher('index.db')

def run_webapp():
    bottle.run(host='localhost', port=8080, reloader=True)

if __name__ == '__main__':
    init_searchengine()
    run_webapp()
