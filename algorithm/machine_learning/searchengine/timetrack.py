import time
class timetracker:
    def __init__(self):
        self.times = {}

    def __call__(self, f):
        self.times[f.func_name] = [0.0, 0]
        def wrap(*args, **kwargs):
            old = time.time()
            r = f(*args, **kwargs)
            self.times[f.func_name][0] += time.time() - old
            self.times[f.func_name][1] += 1
            return r
        return wrap

    def print_info(self):
        for func, (time, times) in self.times.iteritems():
            print '%15s cost %3.3f sec (%3d times).' %(func, time, times)

if __name__ == '__main__':
    tt = timetracker()

    class A:
        @tt
        #?
        def x(self):
            for i in range(100000):
                pass
            print 'x()'

    a = A()
    a.x()
    a.x()
    tt.print_info()
