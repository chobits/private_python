import cluster

# return 1 - len(v1 & v2) / len(v1 | v2) as distance in 0.0~1.0
def tanimoto(vec1, vec2):
    union = 0
    insection = 0
    
    for v1, v2 in zip(vec1, vec2):
        if v1 != 0 and v2 != 0: insection += 1
        if v1 != 0 or v2 != 0: union += 1
    if union == 0: assert 0
    return 1 - float(insection) / union

def test():
    wants, people, data = cluster.get_info('zebo.txt')
    cluster_tree = cluster.hcluster(data, distance_func=tanimoto)
    cluster.draw_cluster(cluster_tree, wants)

if __name__ == '__main__':
    test()
