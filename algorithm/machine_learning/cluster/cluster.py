from itertools import combinations, islice, izip
from math import sqrt
from PIL import Image, ImageDraw
from random import random

class bicluster:
    def __init__(self, vec, id=0.0, distance=0.0, left=None, right=None):
        self.vec = vec
        self.id = id
        self.distance = distance
        self.left = left
        self.right = right

def person_distance(x, y):
    sumx = sum(x)
    sumy = sum(y)
    sumxsq = sum([i*i for i in x])
    sumysq = sum([i*i for i in y])
    psum = sum([i * j for i, j in zip(x, y)])
    num = psum - sumx * sumy / len(x)
    den = sqrt((sumxsq - sumx * sumx / len(x)) * (sumysq - sumy * sumy / len(x)))
    return 1.0 - num / den if den else 0

# hierarchical clustering
def hcluster(rows, distance_func=person_distance):
    distances = {}
    clusters = [bicluster(row, id=i) for i, row in enumerate(rows)]
    cid = -1
#   __len = len(clusters)
    while len(clusters) > 1:
#       print '%d / %d' %(__len - len(clusters) + 1, __len)
        min_distance = None
        for a, b in combinations(clusters, 2):
            if (a.id, b.id) not in distances:   # Dont use (a, b) as dict key, its so slow!!
                distances[(a.id, b.id)] = distance_func(a.vec, b.vec)
            d = distances[(a.id, b.id)]
            if min_distance == None or d < min_distance:
                min_distance, min_a, min_b = d, a, b
        clusters.remove(min_a)
        clusters.remove(min_b)
        clusters.append(bicluster([(x + y) / 2.0 for x, y in zip(min_a.vec, min_b.vec)], cid, min_distance, min_a, min_b))
        cid -= 1
    return clusters[0]

# matrix transposition
def rotate_matrix(rows):
    return zip(*rows)

# column clustering
def ccluster(rows, distance_func=person_distance):
    columns = rotate_matrix(rows)
    return hcluster(columns, distance_func)

# k-means clustering
def kcluster(rows, k=10, distance_func=person_distance):
    ranges = [(min(row), max(row)) for row in zip(*rows)]
    centres = [[random() * (maxv - minv) + minv for minv, maxv in ranges] for _ in range(k)]
    prev_clusters = None

    for time in range(100):        # default run 100 times
        print 'Iteration', time
        # make clusters and add all points(rows) into clusters
        clusters = [[] for _ in range(k)]
        for r, row in enumerate(rows):      # O(n)
            i = min(range(k), key=lambda x:distance_func(centres[x], row))  # O(k)
            clusters[i].append(r)

        # end?
        if prev_clusters == clusters: break
        prev_clusters = clusters

        # recaculate cluster centres
        for i in range(k):
            n = len(clusters[i])
            if n > 0:
                centre = [sum(column) / n for column in zip(*[rows[r] for r in clusters[i]])]
                centres[i] = centre
    sums = sum([sum([distance_func(rows[i], rows[k]) for i, k in combinations(cluster, 2)])
            for cluster in clusters])
    return clusters, sums

def display_cluster(cluster, labels=None, n=0):
    if cluster == None: return
    # node
    print ' ' * n,
    if cluster.id >= 0:    # leave
        if labels == None: print '+'
        else: print labels[cluster.id]
    else:               # twig
        print '-'
    # left
    display_cluster(cluster.left, labels, n+1)
    # right
    display_cluster(cluster.right, labels, n+1)

def get_info(filename='blogdata.txt'):
    f = open(filename)
    lineiter = iter(f)
    column = lineiter.next().split('\t')[1:]
    column[-1] = column[-1].replace('\r\n', '')
    row = []
    data = []
    for line in lineiter:
        linelist = line.split('\t')
        row.append(linelist[0])
        data.append([float(i) for i in linelist[1:]])
    f.close()
    return row, column, data


# draw cluster into tree picture
def get_depth(cluster):
    if cluster == None: return 0
    return max(get_depth(cluster.left), get_depth(cluster.right)) + 1

def get_nodes(cluster):
    if cluster.left == None and cluster.right == None: return 1
    return get_nodes(cluster.left) + get_nodes(cluster.right)

def draw_node(draw, cluster, top_x, top_y, x_scale, y_scale, labels):
    assert cluster != None
    if cluster.id >= 0:
        draw.text((top_x + 5, top_y + 5), labels[cluster.id], (0, 0, 0))
        return top_x, top_y + y_scale / 2

    left_y = get_nodes(cluster.left) * y_scale

    lchild_x, lchild_y = draw_node(draw, cluster.left, top_x + x_scale, top_y, x_scale, y_scale, labels)
    rchild_x, rchild_y = draw_node(draw, cluster.right, top_x + x_scale, top_y + left_y, x_scale, y_scale, labels)

    assert lchild_x == rchild_x
    draw.line((top_x, lchild_y, top_x, rchild_y), fill=(255, 0, 0))     # -- lchild
    draw.line((top_x, lchild_y, lchild_x, lchild_y), fill=(255, 0, 0))  # |
    draw.line((top_x, rchild_y, rchild_x, rchild_y), fill=(255, 0, 0))  # -- rchild
    return top_x, top_y + left_y

def draw_cluster(cluster, labels, jpeg='cluster.jpg'):
    w_depth = get_depth(cluster) + 6 # more for text
    h_nodes = get_nodes(cluster) + 2
    width = 1500    # x
    height = 20 * h_nodes   # y

    w_scale = float(width) / w_depth
    h_scale = float(height) / h_nodes
    img = Image.new('RGB', (width, height), (255, 255, 255))
    draw = ImageDraw.Draw(img)
    draw_node(draw, cluster, w_scale, h_scale, w_scale, h_scale, labels)
    img.save(jpeg, 'JPEG')

# draw all vecs as 2-d point
# rate = 1% (default)
def scaledown(data, distance_func=person_distance, movedist=0.01):
    nrow = len(data)
    realdist = [[distance_func(vecx, vecy) for vecy in data] for vecx in data]

    fakedist = [[0.0 for _ in range(nrow)] for __ in range(nrow)]
    points = [(random(), random()) for _ in range(nrow)]
    preverror = None

    for time in range(1000):
        # caculate fakedist
        for i, (ix, iy) in enumerate(points):
            for k, (kx, ky) in enumerate(points):
                fakedist[i][k] = sqrt(pow(ix - kx, 2) + pow(iy - ky, 2))

        # caculate move distance
        moverate = [[0.0, 0.0] for _ in range(nrow)]
        error = 0
        for i, (ix, iy) in enumerate(points):
            for k, (kx, ky) in enumerate(points):
                if i == k or fakedist[i][k] == 0: continue
                # -- total_movedist = (ix - kx) * ((real - fake) / fake)
                # => moverate = total_movedist / real
                errorrate = (realdist[i][k] - fakedist[i][k]) / realdist[i][k]
                moverate[i][0] += errorrate * (ix - kx) / fakedist[i][k]
                moverate[i][1] += errorrate * (iy - ky) / fakedist[i][k]
                error += abs(errorrate)
        print 'total error rate:', error
        if preverror != None and preverror <= error: break
        preverror = error

        # move it
        for i, ((px, py), (mrx, mry)) in enumerate(izip(points, moverate)):
            points[i] = (px + mrx * movedist, py + mry * movedist)

    return points

# NOTE: point_x(y) in (0, 1)
def draw_points(points, labels, width=2000, height=2000, jpeg='points.jpeg'):
    img = Image.new('RGB', (width, height), (255, 255, 255))
    draw = ImageDraw.Draw(img)

    # draw all points
    for i, (x, y) in enumerate(points):
        draw.text(((x + 0.5) * width / 2, (y + 0.5) * height / 2), labels[i], (0, 0, 0))

    img.save(jpeg, 'JPEG')


# test methods
def test_scaledown():
    blognames, words, data = get_info()
    points = scaledown(data)
    draw_points(points, blognames)

def test_hcluster():
    blognames, words, data = get_info()
    #cluster_tree = hcluster(data)
    cluster_tree = ccluster(data)
    #display_cluster(cluster_tree, labels=blognames)
    draw_cluster(cluster_tree, words)

def test_kcluster():
    blognames, words, data = get_info()
    for k in range(5, 20):
        clusters, sums = kcluster(data, k)
        print k, sums
#   for cluster in clusters:
#       print '\n'.join([blognames[c] for c in cluster])
#       print '---'

if __name__ == '__main__':
#   test_hcluster()
#   test_kcluster()
    test_scaledown()
