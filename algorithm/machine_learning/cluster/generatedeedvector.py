import feedparser
import re

def getwords(html):
    # delete html tag
    text = re.compile('<[^>]+>').sub('', html)
    words = re.compile('[^a-zA-Z]+').split(text)
    return [word.lower() for word in words if word != '']

def getwordcounts(url):
    d = feedparser.parse(url)
    wc = {}     # word count
    for e in d.entries:
        summary = e.summary if 'summary' in e.summary else e.description
        words = getwords(e.title + summary)
        for word in words:
            wc.setdefault(word, 0)
            wc[word] += 1
    try: return d.feed.title, wc
    except AttributeError: return url, wc

import sys
def generate_data():
    f = open('feedlist.txt')
    wordcount = {}
    apcount = {}
    for url in f:
        print 'handling[%s][%s]'%(url, url.replace('\r\n', ''))
        sys.exit(0)
        break
        title, wc = getwordcounts(url.replace('\n', ''))
        wordcount[title] = wc
        for word in wc:
            apcount.setdefault(word, 0)
            apcount[word] += 1
    f.close()

    # filter some words
    total = len(wordcount)
    words = [word for word, count in apcount.iteritems()
            if 0.1 < float(count) / total < 0.5]

    # save results
    f = open('blog_count.txt', 'w')
    # titles
    f.write('blog')
    for word in words: f.write('\t' + word)
    f.write('\n')
    # data
    for blog, wc in wordcount.iteritems():
        f.write(blog)
        for word in words:
            if word in wc:
                f.write('\t%d' %wc[word])
            else:
                f.write('\t0')
        f.write('\n')
    f.close()

def test():
    generate_data()

if __name__ == '__main__':
    test()
#   print getwordcounts('http://donghao.org/rss.xml')[0]
