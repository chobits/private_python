#!/usr/bin/python
import sys
from collections import deque

def safe((x, y), xqueens):
    for (qy, qx) in enumerate(xqueens):
        if x == qx or abs(qx - x) == abs(qy - y):
            return 0
    return 1

totals = 0
def __nqueens(n, xqueens):
    global totals
    totals += 1
    if n == 0:
        return 1
    r = 0
    y = len(xqueens)
    for x in range(n + len(xqueens)):
        if safe((x, y), xqueens):
            xqueens.append(x)
            r += __nqueens(n - 1, xqueens)
            xqueens.pop()
    return r
        
def nqueens(n):
    return __nqueens(n, deque([]))

print nqueens(int(sys.argv[1]))
print totals
