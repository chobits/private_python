#!/usr/bin/python

def memo(f):
    times = 0
    l = []
    def wrap():
        print 'hello'
        f()
    return wrap

@memo
def myfunc():
    print 'world'
    myfunc()

myfunc()
