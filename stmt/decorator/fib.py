#!/usr/bin/python

# memoration for f
# Note that memo is called only once
def memo(f):
    cache = {}
    cache[0] = 0    # fib(0) = 0
    cache[1] = 1    # fib(1) = 1
    helloworld = 0
    def wrap(n):
        global helloworld
        if not cache.has_key(n):
            cache[n] = f(n)
        return cache[n]
    return wrap

@memo
def fib(n):
    return fib(n - 1) + fib(n - 2)

print fib(10)

# for python 3.0+
from functools import lru_cache
@lru_cache()
def fib(n):
    if n < 2: return n
    return fib(n - 1) + fib(n - 2)
