#!/usr/bin/python

def decorator_decorator(*args):
    def decorator(f):
        return f
    return decorator

# f = decorator_decorator(1,2,3) (f)
@decorator_decorator(1,2,3)     # @generate_decorator(?) <=> @decorator
def f():
    pass

def decorator(f):
    return f

# g = decorator(g)
@decorator
def g():
    pass
