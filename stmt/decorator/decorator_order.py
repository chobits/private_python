#!/usr/bin/python

def decorator1(f):
    def wrap1():
        print '1'
        f()
    return wrap1

def decorator2(f):
    def wrap2():
        print '2'
        f()
    return wrap2

# func = decorator1(decorator2(func)) = wrap1
# func() -> wrap1()
#           -> print '1'
#           -> f()   <=> decorator2(func) <=> wrap2
#              -> print '2'
#              -> func()
@decorator1
@decorator2
def func():
    print 'func'

func()
