# file has '__enter__' attribute 
# file has '__exit__' attribute 
with file('test.txt') as f: print f.read(10)

# <=>
# try:
#   f = file('test.txt').__enter__()
#   print f.read(10)
# finally:
#   f.__exit__()

