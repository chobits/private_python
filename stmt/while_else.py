#!/usr/bin/python

i = 0
while i < 2:
    print 'while stmt'
    # break   # will not run else stmt
    i += 1  # when i >= 2, it still run else stmt
else:
    print 'else stmt'
