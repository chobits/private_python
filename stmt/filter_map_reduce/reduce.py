#!/usr/bin/python

def myreduce(func, seq, init=None):
    if init:
        res = init
        lseq = seq
    else:
        res = seq[0]
        lseq = seq[1:]
    for item in lseq:
        res = func(res, item)
    return res

sum = reduce((lambda prevresult, next: prevresult + next), range(2, 10), 1)
print sum
sum = myreduce((lambda prevresult, next: prevresult + next), range(3, 10), 3)
print sum
sum = myreduce((lambda prevresult, next: prevresult + next), range(1, 10))
print sum
