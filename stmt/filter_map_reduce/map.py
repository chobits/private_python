#!/usr/bin/python

def mymap(func, seq):
    mapped_seq = []
    for item in seq:
       mapped_seq.append(func(item)) 
    return mapped_seq

l = map((lambda x: x*x), range(10))
print l
l = mymap((lambda x: x*x), range(10))
print l
