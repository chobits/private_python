#!/usr/bin/python

def myfilter(func, seq):
    filter_seq = []
    for item in seq:
        if func(item):
            filter_seq.append(item)
    return filter_seq

def isprime(x):
    for i in range(2, int(x**0.5) + 1):
        if x % i == 0:
            return False
    return True

l = filter(isprime, range(2, 100))
print l
l = myfilter(isprime, range(2, 100))
print l
