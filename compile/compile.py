#!/usr/bin/python
filetext = open('test.py').read()
code = compile(filetext, '<string>', 'exec')

#hex opcode
print 'opcode hex:'
i = 0
n = len(code.co_code)
while i < n:
    print "%02x" % ord(code.co_code[i]),
    i = i + 1
    if i % 8 == 0:
        print
if i % 8 != 0:
    print

#character opcode
print 'opcode:'
print code.co_code
print 'dis output'
#import dis
#dis.dis(code)
