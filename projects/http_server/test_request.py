import config
from log import setup_log
from test_base import *

# init log
config.log_level = 'INFO'
config.log_file = 'stdout'
config.log_type = ['http', 'sock']
setup_log()

# start test
ip, port = '127.0.0.1', 8080
if len(sys.argv) == 2: port = int(sys.argv[1])

body = '0' + '1' * (10000000 - 1)
data = 'POST /test_post HTTP/1.1\r\n' \
       'Content-Length: %d\r\n' \
       'Connection: close\r\n' \
       'Host: localhost\r\n\r\n' %len(body) + body

# connect & create request
r = test_connect_request(ip, port, data)

# write
tprint('writing request')
r.write()

# read & parse
tprint('reading and parsing response')
err = r.read_parse()
if err not in ['done', 'close']: fatal('read & parse error: %s' %err)

# response
tprint('')
tprint('response:')
print('%s %s %s' %(r.rversion, r.code, r.reason))
for k, v in r.headers.iteritems():
    print('%s: %s' %(k, v))
print('')
if len(r.body) < 4096:
    print(r.body + '[EOF]')
else:
    print(">>> %d bytes response body" %len(r.body))
