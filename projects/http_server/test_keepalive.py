# curl-like client

import tcp
import sys
import time
from test_base import *

def test_keepalive():
    n = 6

    s = test_connect(ip, port)

    for i in range(n - 1):
        test_send(s, 'GET /test_keepalive HTTP/1.1\r\n\r\n')
        test_sleep(0.3)
    test_send(s, 'GET /test_keepalive HTTP/1.0\r\n\r\n')

    tprint('send total data, start reading response')

    data = test_read_all(s)

    for i in range(n):
        pos, version, code, reason, headers, body = test_parse_response(data)
        #tprint('status line: %s %s %s"' %(version, code, reason))
        #tprint('headers: %s' %headers)
        #tprint('body: %s[EOF]' %body)

        assert body == 'keepalive\n'

        data = data[pos:]
        try:
            assert data
        except:
            assert i == n - 1

    assert not data
    tprint('test ok')

def test_pipeline():
    data = 'GET /test_keepalive HTTP/1.1\r\n\r\n' * 5
    data += 'GET /test_keepalive HTTP/1.0\r\n\r\n'
    n = 6

    s = test_connect(ip, port)

    test_send(s, data)

    tprint('send total data, start reading response')

    data = test_read_all(s)

    for i in range(n):
        pos, version, code, reason, headers, body = test_parse_response(data)
        #tprint('status line: %s %s %s"' %(version, code, reason))
        #tprint('headers: %s' %headers)
        #tprint('body: %s[EOF]' %body)

        assert body == 'keepalive\n'

        data = data[pos:]
        try:
            assert data
        except:
            assert i == n - 1

    assert not data
    tprint('test ok')


if __name__ == '__main__':
    ip, port = '127.0.0.1', 8080
    if len(sys.argv) > 1: port = int(sys.argv[1])

    test_pipeline()
    test_keepalive()
