from app import *

# capture 3-digit url, such as '/400'
@app_url('/\d{3,3}', regex=True)
def status_code(r):
    code = int(r.uri[1:4])
    reason = http_status_code.get(code, 'the code is unknown')
    return '%d: %s\n' %(code, reason)
