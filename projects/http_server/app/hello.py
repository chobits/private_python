# descrition: how to write a web application inside this server

# Always write this line. All useful apis are imported.
from app import *


# It's easy to write an helloworld web application.
# You can see it from link 'http://localhost/hello'.
@app_url('/hello')
def hello():
    return 'Hello World\n'


# The function can has 0 or 1 argument. If the function has one argument,
# you can get neccessary information of current http request via this argument (@r)

@app_url('/hello_request')  # << which url do you want to capture?

def hello_request(r):       # << function name handling this request is arbitary
                            #    r is used to descripte current http request

    # get url, http method, http version
    url = r.uri
    method = r.method
    version = r.version

    # get http request headers
    headers_str = ''.join(['%s: %s\n' %(k, v)
                           for k, v in r.headers.iteritems()])

    # get network information about current connection
    src_ip, src_port = r.connection.peername


    # The return value is used as response body.
    # Server will create response and send it for you.

    return 'request from %s:%d is:\n%s %s %s\n%s\n' % \
           (src_ip, src_port, method, url, version, headers_str)


# If you want to capture so many urls starting with '/hello_',
# you can match url with regular expression.
# Just set parameter 'regex' to True.
@app_url('/hello_.*', regex=True)
def hello_what(r):
    what = r.uri.replace('/hello_', '', 1)  # get what is matched.
    return 'Get %s\n' %what


# If you want to create response data and send it by yourself,
# you should set parameter 'hack' to True.
# In such case, server will not do all the dirty work for you.
#
# >> Please ensure you know what you are doing. <<
#
@app_url('/hack', hack=True)
def hack(r):
    # create a raw http response
    body = 'Hack it!\n'
    r.wdata = 'HTTP/1.0 200 OK\r\n' \
              'Connection: close\r\n' \
              'Content-Length: %d\r\n' \
              '\r\n' %len(body)

    r.wdata += body

    # dont keepalive this connection if response has 'Connection: close'
    r.keepalive = False

    # call internal api of server to send this response
    http_send_response(r)

    # Unnecessary to return anything, server ignores return value.


# Let me see what server will return if your app is buggy.
# Aha, it's 503 page!
@app_url('/corrupt')
def corrupt():
    1 / 0

