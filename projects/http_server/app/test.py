# This is used for server test case.
from app import *
from random import randint

@app_url('/test_keepalive')
def test_keepalive():  
    return 'keepalive\n'

@app_url('/test_post')
def test_post(r):
    return r.input_body

@app_url('/test_chunk', hack=True)
def test_chunk_response(r):
    # create chunk response firstly
    r.chunk_create_response()

    # generate random chunk body
    body_len = randint(0, 1024 * 1024)
    while body_len > 0:
        chunk_len = randint(1, body_len)
        body_len -= chunk_len
        r.chunk_add_body('*' * chunk_len)
    # if there is no chunk tailer
    # curl: (18) transfer closed with outstanding read data remaining
    r.chunk_add_body('')

    # send chunk response
    http_send_response(r)

@app_url('/status')
def test_status(r):
    body = 'request: %d\nresponse: %d\n' \
           'accept: %d\nconnect: %d\n' \
           'write: %d\nread: %d\n' % \
           (status.request, status.response,
            status.accept, status.connect,
            status.write, status.read)
    return body
