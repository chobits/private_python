from app import *

@app_url('/.*', regex=True, hack=True)
def http_to_ssl(r):
    r.wdata = 'HTTP/1.0 302 Found\r\n' \
              'Location: https://localhost/\r\n' \
              'Connection: Close\r\n' \
              'Content-Length: 0\r\n\r\n'

    http_send_response(r)

