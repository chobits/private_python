# description: this app is used to test mod_http_copy

from app import *

size = 0

@app_url('/pipeline.*', regex=True, hack=True)
def http_copy_post(r):
    global size

    url = r.uri
    param = url.split('?', 1)[1]
    i = int(param.split('=', 1)[1])
    body = '1' * i
    body_size = len(body)
    size += body_size

    r.chunk_create_response()
    chunk_size = 1000000

    while body_size > 0:
        cz = min(chunk_size, body_size)
        r.chunk_add_body('x' * cz)
        body_size -= cz
    r.chunk_add_body('')

    http_send_response(r)
    
@app_url('/copy_status')
def http_copy_status():
    return 'send %d bytes\n' %size
