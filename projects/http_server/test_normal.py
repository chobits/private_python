import tcp
import sys
from test_base import *

def test_normal_url():
    # app/hello.py
    assert test_get_content('/hello') == 'Hello World\n'
    assert 'GET' in test_get_content('/hello_request')
    assert test_get_content('/hello_world') == 'Get world\n'
    assert test_get_content('/hack') == 'Hack it!\n'
    assert '503' in test_get_content('/corrupt')

    # app/status_code.py
    from http_status_code import http_status_code
    for code, reason in http_status_code.iteritems():
        assert test_get_content('/%d' %code) == ('%d: %s\n' %(code, reason))


def test_get_content(url):
    response = test_get(url)
    pos, version, code, reason, headrs, body = test_parse_response(response)
    assert pos == len(response)
    return body

if __name__ == '__main__':
    test_normal_url()
