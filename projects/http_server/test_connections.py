# test max connections which server can established

import tcp
import sys
import time

ip, port = '127.0.0.1', 8080
if len(sys.argv) > 1:
    port = int(sys.argv[1])

connections = 0

while True:
    s, err = tcp.test_client(ip, port)
    if err:
        print('Cannot connect to server: %s' %err)
        print('max connections: %d' %connections)
        print('waiting 10 min to exiting...')
        time.sleep(600)
        sys.exit(0) 

    connections += 1
    print('established no.%d connection: %s %d' %(connections, s.getsockname()))
