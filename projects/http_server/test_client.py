# curl-like client

import tcp
import sys
import time
from test_base import *

ip, port = '127.0.0.1', 8080
if len(sys.argv) > 1:
    port = int(sys.argv[1])

s = test_connect(ip, port)

body = '1' * 10000000
data = 'POST /post HTTP/1.1\r\nContent-Length: %d\r\nHost: localhost\r\nConnection: close\r\n\r\n' %len(body) + body
#data = 'GET / HTTP/1.0\r\nHost: %s:%d\r\nConnection: close\r\n\r\n' %(ip, port)
test_send(s, data)

response = test_read_all(s)

tprint('read data:')
if len(response) > 256:
    print(response[0:256] + '...total %d bytes[EOF]' %len(response))
else:
    print(response)
tprint('tail data:')
print(response[-256:])
tprint('parse chunk:')
header, body = response.split('\r\n\r\n', 1)
print('body len: %d' %(len(body)))

s.close()
sys.exit(0)
