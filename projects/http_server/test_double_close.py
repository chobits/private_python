# obsolete: fixed,  deleted_event.handler = event_deleted_handler

import tcp
import sys
from time import sleep

ip = '127.0.0.1'
port = int(sys.argv[1])

socks = []

connections = int(sys.argv[2])

for _ in range(connections):
    s, err = tcp.test_client(ip, port)
    assert not err
    socks.append(s)


for _ in range(connections):
    for s in socks:
        n, err = tcp.write(s, 'GET / HTTP/1.0\r\n\r\n')
        assert not err

