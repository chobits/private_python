import http
from http_status_code import http_status_code

# error handle
def http_process_error(r):
    if not r.error_status_code:
        r.close()
        return
    error_response(r)

def error_response(r):
    create_error_response(r)
    http.http_send_response(r)

def create_error_response(r):
    code = r.error_status_code
    description = http_status_code[code]

    body = 'Error: %d %s\n' %(code, description)
    r.wdata = '%s %d %s\r\n' \
              'Connection: close\r\n' \
              'Content-Length: %d\r\n\r\n' %(r.version, code, description, len(body))
    r.wdata += body
    r.keepalive = False

