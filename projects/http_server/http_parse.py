# http parser

# states of http state machine
# http request line
HTTP_REQUEST_LINE_START = 0
HTTP_METHOD = 1
HTTP_URI_START = 2
HTTP_URI = 3
HTTP_VERSION_START = 4
HTTP_VERSION = 5
HTTP_REQUEST_LINE_END_CF = 6

# http status line
HTTP_STATUS_LINE_START = 10
HTTP_STATUS_VERSION = 11
HTTP_STATUS_CODE_START = 12
HTTP_STATUS_CODE = 13
HTTP_REASON_PHRASE_START = 14
HTTP_REASON_PHRASE = 15
HTTP_STATUS_LINE_END_CF = 16

# http headers
HTTP_HEADERS_START = HTTP_HEADER_KEY_START = 7

HTTP_HEADER_KEY = 71
HTTP_HEADER_VALUE_START = 72
HTTP_HEADER_VALUE = 73
HTTP_HEADER_LINE_END_CF = 74

HTTP_HEADERS_END_CF = 75
HTTP_HEADERS_END = 76

# http invalid state
HTTP_INVALID_STATE = 20
HTTP_INVALID_VERSION = 21
HTTP_INVALID_URI = 22
HTTP_INVALID_METHOD = 23
HTTP_INVALID_REQUEST_LINE_END = 24
HTTP_INVALID_HEADER_VALUE = 25
HTTP_INVALID_HEADERS_END = 26
HTTP_INVALID_STATUS_VERSION = 27
HTTP_INVALID_STATUS_CODE = 28
HTTP_INVALID_REASON_PHRASE = 29
HTTP_INVALID_STATE_END = 30

http_invalid_state = range(HTTP_INVALID_STATE, HTTP_INVALID_STATE_END)

# constant value
http_method = ['OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'TRACE', 'CONNECT']
http_method_chars = ''.join(http_method)

http_octet = ''.join([chr(i) for i in range(0, 255)])
http_char = ''.join([chr(i) for i in range(0, 127)])
http_separators = "()<>@,;:\\\"/[]?={} \t"
http_ctl = ''.join([chr(i) for i in range(0, 31)]) + chr(127)
http_token = ''.join([c for c in http_char if c not in http_separators and c not in http_ctl])

http_uri_chars = 'abcdefghijklmnopqrstuvwxyz'\
                 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'\
                 '1234567890'\
                 '~`!@#$%^&*()-_=+{}[]|\\:;"\'<>,.?/'

http_version = ['HTTP/1.1', 'HTTP/1.0', 'HTTP/0.9']
http_version_chars = ''.join(http_version)

http_header_key_chars = http_token
http_header_value_chars = http_octet

http_status_code_chars = '0123456789'
http_reason_phrase_chars = ''.join([c for c in http_octet if c != '\r' and c != '\n'])

# parse start-line of request: request line
def http_parse_request_line(state, (s, e, data)):
    assert e <= len(data)

    # assert data[e - 1] is last char
    pos = s
    prev = s
    prev_state = state

    method = None
    uri = None
    version = None

    while pos < e:
        c = data[pos]
        # method
        if state == HTTP_REQUEST_LINE_START:
            if c in http_method_chars:
                state = HTTP_METHOD
            else:
                return HTTP_INVALID_METHOD, pos, method, uri, version
        elif state == HTTP_METHOD:
            if c == ' ':
                method = data[prev:pos]
                # end of method
                if method not in http_method:
                    return HTTP_INVALID_METHOD, pos, method, uri, version

                state = HTTP_URI_START
                prev = pos + 1
                prev_state = HTTP_URI_START
            elif c not in http_method_chars:
                return HTTP_INVALID_METHOD, pos, method, uri, version
        # uri
        elif state == HTTP_URI_START:
            if c in http_uri_chars:
                state = HTTP_URI
            else:
                return HTTP_INVALID_URI, pos, method, uri, version
        elif state == HTTP_URI:
            if c == ' ':
                uri = data[prev:pos]
                state = HTTP_VERSION_START
                prev = pos + 1
                prev_state = HTTP_VERSION_START
            elif c not in http_uri_chars:
                return HTTP_INVALID_URI, pos, method, uri, version
        # version
        elif state == HTTP_VERSION_START:
            if c in http_version_chars:
                state = HTTP_VERSION
            else:
                return HTTP_INVALID_VERSION, pos, method, uri, version
        elif state == HTTP_VERSION:
            if c == '\r':
                version = data[prev:pos]
                if version not in http_version:
                    return HTTP_INVALID_VERSION, prev, method, uri, version
                state = HTTP_REQUEST_LINE_END_CF
                prev = pos + 1
                prev_state = HTTP_REQUEST_LINE_END_CF
            elif c == '\n':
                version = data[prev:pos]
                if version not in http_version:
                    return HTTP_INVALID_VERSION, prev, method, uri, version
                prev = pos + 1
                prev_state = HTTP_HEADERS_START
                break
            elif c not in http_version_chars:
                return HTTP_INVALID_VERSION, pos, method, uri, version
        # \r\n
        elif state == HTTP_REQUEST_LINE_END_CF:
            if c == '\n':
                prev = pos + 1
                prev_state = HTTP_HEADERS_START
                break
            else:
                return HTTP_INVALID_REQUEST_LINE_END, pos, method, uri, version
        else:
            return HTTP_INVALID_STATE, pos, method, uri, version

        pos += 1

    return (prev_state, prev, method, uri, version)


# parse start-line of response: status line
def http_parse_status_line(state, (s, e, data)):
    assert e <= len(data)

    # assert data[e - 1] is last char
    pos = s
    prev = s
    prev_state = state

    version = None
    code = None         # status code
    reason = None       # reason phrase

    while pos < e:
        c = data[pos]
        # version
        if state == HTTP_STATUS_LINE_START:
            if c in http_version_chars:
                state = HTTP_STATUS_VERSION
            else:
                return HTTP_INVALID_STATUS_VERSION, pos, version, code, reason
        elif state == HTTP_STATUS_VERSION:
            if c == ' ':
                version = data[prev:pos]
                # end of version
                if version not in http_version:
                    return HTTP_INVALID_STATUS_VERSION, pos, version, code, reason

                state = HTTP_STATUS_CODE_START
                prev = pos + 1
                prev_state = HTTP_STATUS_CODE_START
            elif c not in http_version_chars:
                return HTTP_INVALID_STATUS_VERSION, pos, version, code, reason
        # status code
        elif state == HTTP_STATUS_CODE_START:
            if c in http_status_code_chars:
                state = HTTP_STATUS_CODE
            else:
                return HTTP_INVALID_STATUS_CODE, pos, version, code, reason
        elif state == HTTP_STATUS_CODE:
            if c == ' ':
                code = data[prev:pos]
                state = HTTP_REASON_PHRASE_START
                prev = pos + 1
                prev_state = HTTP_REASON_PHRASE_START
            elif c not in http_status_code_chars:
                return HTTP_INVALID_STATUS_CODE, pos, version, code, reason
            elif (pos - prev) >= 3:     # status code must be 3-digit integer
                return HTTP_INVALID_STATUS_CODE, pos, version, code, reason
        # reason phrase
        elif state == HTTP_REASON_PHRASE_START:
            if c == '\r' or c == '\n':  # empty reason phrase
                state = HTTP_REASON_PHRASE
                continue
            if c in http_reason_phrase_chars:
                state = HTTP_REASON_PHRASE
            else:
                return HTTP_INVALID_REASON_PHRASE, pos, version, code, reason
        elif state == HTTP_REASON_PHRASE:
            if c == '\r':
                reason = data[prev:pos]
                state = HTTP_STATUS_LINE_END_CF
                prev = pos + 1
                prev_state = HTTP_STATUS_LINE_END_CF
            elif c == '\n':
                reason = data[prev:pos]
                prev = pos + 1
                prev_state = HTTP_HEADERS_START
                break
            elif c not in http_reason_phrase_chars:
                return HTTP_INVALID_REASON_PHRASE, pos, version, code, reason
        # \r\n
        elif state == HTTP_STATUS_LINE_END_CF:
            if c == '\n':
                prev = pos + 1
                prev_state = HTTP_HEADERS_START
                break
            else:
                return HTTP_INVALID_STATUS_LINE_END, pos, version, code, reason
        else:
            return HTTP_INVALID_STATE, pos, version, code, reason

        pos += 1

    return (prev_state, prev, version, code, reason)


# parse  1. at most one header
#    or  2. headers end CRLF
def http_parse_header_line(state, (s, e, data)):
    # assert data[e - 1] is last char
    pos = s
    prev = s
    prev_state = state

    header_key = None
    header_value = None

    while pos < e:
        c = data[pos]

        # http header key
        if state == HTTP_HEADER_KEY_START:
            if c in http_header_key_chars:
                state = HTTP_HEADER_KEY
            elif c == '\r':
                prev = pos + 1
                prev_state = HTTP_HEADERS_END_CF
                state = HTTP_HEADERS_END_CF
            elif c == '\n':
                prev = pos + 1
                prev_state = HTTP_HEADERS_END
                break
            else:
                return HTTP_INVALID_HEADER_VALUE, pos, header_key, header_value
        elif state == HTTP_HEADER_KEY:
            if c == ':':
                header_key = data[prev:pos]
                state = HTTP_HEADER_VALUE_START
                prev = pos + 1
                prev_state = HTTP_HEADER_VALUE_START
            elif c not in http_header_key_chars:
                return HTTP_INVALID_HEADER_VALUE, pos, header_key, header_value
        # http header key
        elif state == HTTP_HEADER_VALUE_START:
            if c in (' ', '\t'):
                prev += 1
            elif c in http_header_value_chars:
                state = HTTP_HEADER_VALUE
            else:
                return HTTP_INVALID_HEADER_VALUE, pos, header_key, header_value
        elif state == HTTP_HEADER_VALUE:
            if c == '\r':
                header_value = data[prev:pos]
                state = HTTP_HEADER_LINE_END_CF
                prev = pos + 1
                prev_state = HTTP_HEADER_LINE_END_CF
            elif c == '\n':
                header_value = data[prev:pos]
                prev = pos + 1
                prev_state = HTTP_HEADER_KEY_START
                break
            elif c not in http_header_value_chars:
                return HTTP_INVALID_HEADER_VALUE, pos, header_key, header_value
        elif state == HTTP_HEADER_LINE_END_CF:
            if c == '\n':
                prev = pos + 1
                prev_state = HTTP_HEADER_KEY_START
                break
            elif c not in http_header_value_chars:
                return HTTP_INVALID_HEADER_VALUE, pos, header_key, header_value
        elif state == HTTP_HEADERS_END_CF:
            if c == '\n':
                prev = pos + 1
                prev_state = HTTP_HEADERS_END
                break
            else:
                return HTTP_INVALID_HEADERS_END, pos, header_key, header_value
        else:
            return HTTP_INVALID_STATE, pos, header_key, header_value

        pos += 1

    return prev_state, prev, header_key, header_value

# return value:
#  2: has parsed headers end (CRLF or LF)
#  1: has parsed one header key, value and END(CRLF or LF)
#  0: need more data to parse one header
# -1: header is invalid
#
# assert that data[s:] starts from HTTP_HEADER_KEY_START state
def http_parse_one_header((s, e, data)):

    assert e <= len(data)   # assert data[e - 1] is last char

    state, pos, k, v = http_parse_header_line(HTTP_HEADER_KEY_START, (s, e, data))

    if state in http_invalid_state:
        return -1, pos, k, v

    if state == HTTP_HEADER_KEY_START and pos > s:
        assert k and v
        return 1, pos, k, v

    if state == HTTP_HEADERS_END:
        assert not k and not v
        return 2, pos, None, None

    return 0, s, None, None

# parse one complete response
# TODO: remove raise statement
def http_parse_response(data):
    state, pos, version, code, reason = \
        http_parse_status_line(HTTP_STATUS_LINE_START, (0, len(data), data))
    assert state == HTTP_HEADERS_START

    state, pos, n, headers = http_parse_header(state, (pos, len(data), data))
    assert state == HTTP_HEADERS_END

    if headers.get('transfer-encoding') == 'chunked':
        raise Exception('TODO: chunked')

    content_length = headers.get('Content-Length')
    assert content_length

    try: n = int(content_length)
    except: raise Exception('Content-Length is invalid \"%s\"' %content_length)

    body = data[pos:pos + n]
    pos += n

    return pos, version, code, reason, headers, body


# parse at most @n headers
def test_parse_one_header():
    datas = [ "hello: world\r\n"\
              "Connection: xx\r\n"\
              "Content-Length: 10\r\n"\
              "\r\n",

              "need-more-data",

              "need-CR: value\r",

              "header-end-LF: value\n",

              "header-end-CRLF: value\n",

              "\r\n",       # header end CRLF

              "\n",         # header end LF

              # error k-v test
              "key-is-error   : world\n",

              "key-is-error!: world\n",

              "value-error: \r\n" ]
    for data in datas:
        print('== testing data:[%s] ==' %data)
        for i in range(1, len(data) + 1):
            print('testing inc: %d' %i)
            do_test_parse_one_header(data, i)
            print('')
        print('')

def do_test_parse_one_header(data, inc):
    pos = 0
    e = 0
    while True:
        print('parsed data[%d:%d] is [%s]' %(pos, e, data[pos:e]))
        parsed, pos, k, v = http_parse_one_header((pos, e, data))
        print('parsed %d pos %d key: %s value: %s' %(parsed, pos, k, v))

        if parsed == 2:
            print('parsed to end')
            break
        if parsed == 0:
            ee = min(e + inc, len(data))
            if ee == e:
                print('need more data')
                break
            e = ee
        if parsed == -1:
            print('header is invalid: pos %d' %pos)
            break

def http_parse_header(state, (s, e, data), num=0):

    assert e <= len(data)   # assert data[e - 1] is last char

    pos = s
    n = 0
    headers = {}
    pk = None

    while True:
        if num > 0 and n > num:
            break

        #print('state: %s parsing header [%s]' %(state, data[pos:e]))
        prev = pos
        state, pos, k, v = http_parse_header_line(state, (pos, e, data))
        if state == HTTP_HEADERS_END:
            break

        if state in http_invalid_state:
            #print('invalid state: %s data [%s]' %(state, data[pos:e]))
            break

        if pos == prev:
            #print('parse not to continue, need more data')
            break

        #print('state: %s pos: %s header: <%s: %s>' %(state, pos, k, v))
        if not v:
            headers[k] = None
            pk = k
        elif not k:
            headers[pk] = v
        else:
            headers[k] = v
            pk = None

        n += 1

        assert pos <= e     # debug
        if pos == e:
            #print('parse to end %d' %pos)
            break

    #print state, pos, n, headers
    return state, pos, n, headers

# test for this parser
def test_header():
    data = 'Connection: close\r\n'\
           'Content-Length: 123\r\n'
    state = HTTP_HEADERS_START
    pos = 0
    end = len(data)

    state, pos, n, headers = http_parse_header(state, (pos, end, data))

    for k, v in headers.iteritems():
        print('%s: %s' %(k, v))

def test(data, n):
    state = HTTP_REQUEST_LINE_START
    pos = 0
    end = pos + n
    n = min(len(data), n)
    last_parse = 0
    m, u, v = '(empty)', '(empty)', '(empty)'

    while True:
        end = min(end, len(data))

        state, pos, method, uri, version = \
                http_parse_request_line(state, (pos, end, data))

        if state in http_invalid_state:
            print('invalid state: %s\n'
                  '%s%s'
                  '%s^'
                  %(state, data[0:end], '' if '\n' in data[0:end] else '\n', ' ' * pos))
            return

        info = 'state: %s pos: %s ' %(state, pos)
        if method:
            m = method
            info += 'method: %s ' %method
        if uri:
            u = uri
            info += 'uri: %s ' %uri
        if version:
            v = version
            info += 'version: %s' %version
        print(info)

        if state == HTTP_HEADERS_START:
            print('parse Ok\n')
            break

        if pos > end:
            print('stange error pos(%s) > end(%s)' %(pos, end))
            break

        end += n

        if last_parse:
            print('last: data is parsed to end')
            break

        if end > len(data):
            last_parse = 1

    print('method: %s uri: %s version: %s\n' %(m, u, v))


def test_ok():
    http_request1 = 'GET /helloworld HTTP/1.1\r\n'

    for size in range(1, len(http_request1) + 1):
        s = http_request1[0:size]
        print('\n======= [%s] ==========\n' %s)
        for n in range(1, len(s) + 1):
            test(s, n)

def test_err():
    s = 'GET  /heeloworld HTTP/121\r\n'
    for n in range(1, len(s) + 1):
        test(s, n)

if __name__ == '__main__':
    #test_ok()
    #test_err()
    #test_header()
    test_parse_one_header()


# def a(): return 1, 2
# x, y, z = a()
# --> TypeError: 'int' object is not iterable
