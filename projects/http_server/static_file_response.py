from os.path import basename
from http import http_send_response

def static_file_response(r, path):
    try:
        f = file(path)
        body = f.read()
        f.close()
    except:
        raise Exception('FIXME: use 404 page instead. %s cannot be found' %path)
        return

    r.simple_create_response(body)
    http_send_response(r)

def static_func(isfile, path):
    def wrap(r):
        # Error without _path:
        # UnboundLocalError: local variable 'path' referenced before assignment
        _path = path if isfile else path + r.uri
        return static_file_response(r, _path)
    wrap.__name__ = static_file_response.__name__ + ':' + basename(path)
    return wrap

# init
def setup_static_file(conf):
    for url_type, url, path_type, path in conf.static:
        assert path_type in ['file', 'dir']
        assert url_type in ['raw', 'regex']

        if path[0] != '/': path = '%s/%s' %(conf.cwd, path)

        if url_type == 'raw':
            conf.dispatch.add_raw(url, static_func((path_type == 'file'), path))
        else:
            conf.dispatch.add_regex(url, static_func((path_type == 'file'), path))

