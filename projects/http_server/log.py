import sys
import config

# refer linux kernel log level
LOG_EMERG = 0x0000
LOG_ALERT = 0x0001      # unusual
LOG_CRIT = 0x0002       # unusal
LOG_ERROR = 0x0003
LOG_WARN = 0x0004
LOG_NOTICE = 0x0005
LOG_INFO = 0x0006
LOG_DEBUG = 0x0007      # unusual
LOG_NOISE = 0x0008

DEBUG_SOCK = 0x8001
DEBUG_EVENT = 0x8002
DEBUG_CONN = 0x8003
DEBUG_SSL = 0x8004
DEBUG_TCP = 0x8005
DEBUG_DATA = 0x8006
DEBUG_HTTP = 0x8007
DEBUG_SERVER = 0x8008
DEBUG_CONF = 0x8009
DEBUG_DISPATCH = 0x800A

_l = { LOG_EMERG:'EMERG', LOG_ALERT:'ALERT', LOG_CRIT:'CRIT',
       LOG_ERROR:'ERROR', LOG_WARN:'WARN', LOG_NOTICE:'NOTICE',
       LOG_INFO:'INFO', LOG_DEBUG:'DEBUG', LOG_NOISE:'NOISE' }

_ls = dict(((v, k) for k, v in _l.iteritems()))

_dl = { DEBUG_SOCK:'sock', DEBUG_EVENT:'event', DEBUG_CONN:'conn',
        DEBUG_SSL:'ssl', DEBUG_TCP:'tcp', DEBUG_DATA:'data', DEBUG_HTTP:'http',
        DEBUG_SERVER:'server', DEBUG_CONF:'conf', DEBUG_DISPATCH:'dispatch' }

_dls = dict(((v, k) for k, v in _dl.iteritems()))

# log info
log_file = sys.stdout
log_level = _ls[config.log_level]
log_debug_level = [_dls[s] for s in config.log_type]

def log(level, debug, s):
    if level <= log_level and debug in log_debug_level:
        log_file.write('[%s] %s: %s' %(_l[level], _dl[debug], s))
        #log_file.flush()

def log_open_file(path):
    global log_file
    # default
    if path == 'stdout': return

    #log_file = file(path, 'a')
    log_file = file(path, 'w')

def log_close_file():
    global log_file
    if log_file != sys.stdout:
        log_file.close()

def setup_log():
    global log_level, log_debug_level
    log_level = _ls[config.log_level]
    log_debug_level = [_dls[s] for s in config.log_type]

    log_open_file(config.log_file)

#def debug_func(f):
#    def wrap(*args, **kwargs):
#        print(f.__name__)
#        return f(*args, **kwargs)
#    return wrap
