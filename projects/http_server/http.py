import traceback
import async_event_kqueue
import tcp

from log import *
from http_status import status
from http_request import request
from connection import get_connection
from http_status_code import http_status_code
from http_error import http_process_error
from http_ssl import ssl_wrap_connection


def http_accept_handler(e):
    s, conf = e.data
    ec = e.core
    accepts = 0

    # NOTE for kqueue:
    # It must accept all the events here. Otherwise, kqueue with EV_CLEAR
    # flag returns once when state changes and will block on second call until
    # state changes again.

    while True:
        sock, addr, err = tcp.accept(s)
        if err == 'again':
            log(LOG_NOISE, DEBUG_SOCK, 'accept next time...\n')
            break
        # [Errno 24] Too many open files.
        # XXX: When doest it send TCP RESET to client?
        if err == 'nofile':
            log(LOG_WARN, DEBUG_SOCK, 'too many open files, no enough files for accept\n')
            # FIXME: when to enable listening event
            # e.core.disable_event(e)
            break
        if err:
            log(LOG_ERROR, DEBUG_SOCK, '%s for accept()\n' %err)
            break

        accepts += 1
        log(LOG_NOISE, DEBUG_SOCK, 'accept no.%d connection with %s\n' %(accepts, addr))

        # create connection
        c = get_connection(sock)
        if not c:
            sock.close()        # TODO: need check error?
            log(LOG_WARN, DEBUG_SOCK, 'no connections: close sock\n')
            break               # TODO: continue or return

        c.conf = conf
        c.peername = addr
        c.sockname = c.sock.getsockname()
        c.revent = ec.add_read_event(c.sock.fileno(), http_read_handler, c)

        if conf.ssl and ssl_wrap_connection(c) == 'err':
            c.close()
            continue

        if conf.post_handle:
            ec.add_timer(c.revent, http_read_timeout_handler, conf.read_timeout)
        else:
            c.revent.handler(c.revent)

    log(LOG_INFO, DEBUG_SOCK, 'accept %d connections\n' %accepts)
    status.accept += accepts

def http_read_handler(e):
    c = e.data
    if not c.request:
        log(LOG_INFO, DEBUG_HTTP, 'create request for connect %s -> %s\n' %(c.peername, c.sockname))
        c.request = request(c)
    else:
        # TODO: why here?
        if c.request.debug_close:
            print('event: %s' %e)
            print('Traceback (original close(from %s) stack):' %(c.peername,))
            traceback.print_list(c.request.debug_close_stack)
            raise Exception('double close')

    http_init(c.request)

def http_init(r, keepalive=False):
    err = r.read_parse()
    if err == 'done':
        r.connection.revent.handler = http_dummy_read_handler   # block event
        r.connection.revent.core.del_timer(r.connection.revent) # delete timer
        http_process(r)
        return

    if err == 'again':
        # keepalive handler has added read timer
        if not keepalive:
            r.connection.revent.core.add_timer(r.connection.revent,
                                               http_read_timeout_handler,
                                               r.conf.read_timeout)
        return

    # err: other error
    http_process_error(r)

def http_process(r):
    r.dump_request()
    http_dispatch(r)

def http_dispatch(r):
    # dispatch to send response
    handler = r.conf.dispatch.get_handler(r.uri)
    if handler:
        log(LOG_INFO, DEBUG_HTTP, 'dispatch url:%s to handler:%s\n' %(r.uri, handler.__name__))
        handler(r)  # internal logic & http_send_response()
    else:
        r.error_status_code = 404
        http_process_error(r)

def http_send_response(r):
    err = r.simple_send_response()
    if err == 'again':
        # Only when we cannot send data at right, or we dont add write event
        http_prepare_write_event(r)
        return

    r.close(finish=(err == 'done'))

    if r.keepalive == 'k':
        # Post http_init() after current event handler to avoid error:
        # RuntimeError: maximum recursion depth exceeded
        r.connection.revent.core.post_event(http_keepalive_handler)

def http_keepalive_handler(e):

    # reinit connection, request::do_keepalive() dont do this
    c = e.data
    c.revent.handler = http_read_handler
    assert c.revent.timeout == None     # should not have read timer
    c.revent.core.add_timer(c.revent, http_keepalive_timeout_handler, c.conf.keepalive_timeout)
    if c.wevent:
        c.wevent.handler = http_dummy_write_handler
        c.wevent.core.del_timer(c.wevent)

    # init keepalive request
    r = c.request
    r.keepalive = False
    log(LOG_INFO, DEBUG_HTTP, 'reuse %s request %s\n' %
                              ('pipeline' if r.data else 'keepalive', r))
    # NOTE: if not call http_init(), it maybe forget previous read event
    #       because http_dummy_read_handler ignored it.
    # TODO: how to know process request currently or wait read event to handle
    http_init(r, keepalive=True)

def http_dummy_read_handler(e):
    log(LOG_NOISE, DEBUG_HTTP, 'dummy read: %s\n' %e)

def http_dummy_write_handler(e):
    log(LOG_NOISE, DEBUG_HTTP, 'dummy write: %s\n' %e)

def http_simple_write_handler(e):
    c = e.data
    http_send_response(c.request)

def http_prepare_write_event(r, handler=http_simple_write_handler):
    c = r.connection
    ec = c.revent.core
    if not c.wevent:
        c.wevent = ec.add_write_event(c.sock.fileno(), handler, c)
    else:
        # TODO: when to add write event again?
        # Adding event again will trigger next write event.
        # Without this, next write event wont be triggered.
        ec.add_event(c.wevent, again=True)
        c.wevent.handler = handler
    ec.add_timer(c.wevent, http_write_timeout_handler, c.conf.write_timeout)

def http_read_timeout_handler(e):
    c = e.data
    r = c.request
    if r:
        log(LOG_INFO, DEBUG_HTTP, 'read timeout: %s\n' %r)
        r.close()
    else:
        log(LOG_INFO, DEBUG_HTTP, 'read timeout: %s\n' %c)
        c.close()

def http_write_timeout_handler(e):
    r = e.data.request
    log(LOG_INFO, DEBUG_HTTP, 'write timeout: %s\n' %r)
    r.close()

def http_keepalive_timeout_handler(e):
    r = e.data.request
    log(LOG_INFO, DEBUG_HTTP, 'keepalive timeout: %s\n' %r)
    r.close()
