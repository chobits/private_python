import tcp
import sys
from test_base import *

def test_input_body(size):
    input_body = test_random_string(size)
    tprint('input body has %d bytes data' %size)
    
    data = 'POST /test_post HTTP/1.1\r\nHost: localhost\r\n' \
           'Content-Length: %d\r\nConnection: close\r\n\r\n%s' % \
           (len(input_body), input_body)
    
    s = test_connect(ip, port)
    test_send(s, data)
    data = test_read_all(s)
    pos, version, code, reason, headers, body = test_parse_response(data)
    
    # test
    assert input_body == body
    assert pos == len(data)
    tprint('Test Ok')
    
if __name__ == '__main__':

    ip, port = '127.0.0.1', 8080
    if len(sys.argv) > 1:
        port = int(sys.argv[1])

    test_input_body(1)
    test_input_body(2)
    test_input_body(4)
    test_input_body(8)
    test_input_body(16)
    test_input_body(1024)
    test_input_body(1024 * 16)
    test_input_body(1024 * 1024)
