import re
from log import *

class url_dispatch(object):

    def __init__(self):
        self.raw_handler = {}
        self.regex_handler = {}
        self.regex = []     # log order

    def add(self, url, handler, regex):
        if regex:
            self.add_regex(url, handler)
        else:
            self.add_raw(url, handler)

    def add_raw(self, url, handler):
        if url in self.raw_handler:
            log(LOG_WARN, DEBUG_CONF,
                'add duplicate url "%s", handler "%s"\n' %
                (url, handler.__name__))
            return
        self.raw_handler[url] = handler

    def add_regex(self, url, handler):
        regex = re.compile(url)
        if regex in self.regex_handler:
            log(LOG_WARN, DEBUG_CONF,
                'add duplicate regex url "%s", handler "%s"\n' %
                (url, handler.__name__))
            return
        self.regex_handler[regex] = handler
        self.regex.append(regex)

    def get_handler(self, url):
        # direct dispatch
        if url in self.raw_handler:
            return self.raw_handler[url]

        # regex match
        for regex in self.regex:
            m = regex.match(url)
            if m and m.end() == len(url):
                return self.regex_handler[regex]
            log(LOG_NOISE, DEBUG_DISPATCH, 'url:%s doest match pattern: %s\n' %
                                           (url, regex.pattern))

        log(LOG_INFO, DEBUG_DISPATCH, 'cannot find handler for url: %s\n' %url)
        return None

    def debug(self):
        print('regex_handler: %s' %self.regex_handler)
        for regex, handler in self.regex_handler.iteritems():
            print(' %s: %s' %(regex.pattern, handler.__name__))

        print('raw_handler:')
        for url, handler in self.raw_handler.iteritems():
            print(' %s: %s' %(url, handler.__name__))

