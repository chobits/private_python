import ssl
import http
from log import *


def ssl_wrap_connection(c):
    conf = c.conf

    # get ssl version
    if hasattr(ssl, conf.ssl_protocol):
        ssl_protocol = getattr(ssl, conf.ssl_protocol)
    else:
        ssl_protocol = ssl.PROTOCOL_SSLv23

    # wrap socket
    try:
        c.sock = ssl.wrap_socket(c.sock,
                                 server_side=True,
                                 certfile=conf.ssl_certfile,
                                 keyfile=conf.ssl_keyfile,
                                 ciphers=conf.ssl_ciphers,
                                 ssl_version=ssl_protocol,
                                 do_handshake_on_connect=False)
    except ssl.SSLError, err:
        log(LOG_ERROR, DEBUG_SSL, '%s for wrap socket\n' %err)
        return 'err'

    c.sock.settimeout(0)        # set non-blocking fo ssl::do_handshake()
    c.revent.handler = http_ssl_handshake_handler
    return None

def http_ssl_handshake_handler(e):
    c = e.data

    # do_handshake() has no return value, use try-except statement to capture error
    try:
        c.sock.do_handshake()
    except ssl.SSLError, err:
        if err.errno in [ssl.SSL_ERROR_WANT_READ, ssl.SSL_ERROR_WANT_WRITE]:
            log(LOG_NOISE, DEBUG_SSL, 'do handshake next time: %s\n' %err)
            return

        log(LOG_ERROR, DEBUG_SSL, '%s for handshake\n' %err)
        c.close()
        return

    # ssl handshake is done
    log(LOG_INFO, DEBUG_SSL, 'handshake has been finished. SSL connection is established\n')
    e.handler = http.http_read_handler
    e.handler(e)

