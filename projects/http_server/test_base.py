import os
import sys
import time

import tcp
import http_request
import connection
import async_event_kqueue
import http_parse

# test print

def tprint(s): print('+ %s' %s)

def fatal(s):
    print('fatal: %s' %s)
    sys.exit(1)

def test_connect_request(ip='127.0.0.1', port=8080, data='GET / HTTP/1.0\r\n\r\n'):
    s = test_connect(ip, port)
    c = connection.connection(s)
    r = http_request.client_request(c)
    c.request = r

    tprint('creating request')
    err = r.create_request_from_data(data)
    if err: fatal('request error: %s' %err)

    return r

def test_connect(ip='127.0.0.1', port=8080):
    tprint('connecting %s:%d' %(ip, port))
    s, err = tcp.test_client(ip, port, block=1)
    if err: fatal('connect error: %s' %err)
    return s

def test_send(s, data):
    tprint('sending data: %d bytes' %len(data))
    n, err = tcp.write(s, data)
    if 0 <= n < len(data):
        fatal('send error: %s, send %d/%d' %(err, n, len(data)))

def test_read_all(s):
    size = 4096 * 1024
    data = ''
    tprint('reading all data...')
    while True:
        d = test_read(s, size, quiet=True)
        if d == '': break
        data += d
    tprint('read %d bytes data' %len(data))
    return data

def test_read(s, size, quiet=False):
    if not quiet: tprint('reading data: %d bytes' %size)
    d, err = tcp.read(s, size)
    if err == 'close': return ''
    if err: fatal('read error: %s' %err)
    return d

def test_tcp_server(ip, port, handler):
    s = test_listen(ip, port)
    ec = async_event_kqueue.event_core()
    ec.add_read_event(s.fileno(), handler, s)
    ec.loop()

def test_listen(ip, port):
    tprint('listening %s:%d' %(ip, port))
    s, err = tcp.tcp_server(ip, port)
    if err:
        fatal('listen error: %s' %err)
    return s

def test_sleep(t):
    tprint('waiting %ss' %t)
    time.sleep(t)

def test_parse_response(data):
    return http_parse.http_parse_response(data)

def test_random_string(size):
    return os.urandom(size)

def test_get(url='/', ip='127.0.0.1', port=8080):
    host = ip if port == 80 else '%s:%d'%(ip, port)
    data = 'GET %s HTTP/1.1\r\n' \
           'Host: %s\r\n' \
           'Connection: close\r\n\r\n' %(url, host)
    s = test_connect(ip, port)
    test_send(s, data)
    return test_read_all(s)

