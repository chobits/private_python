import traceback

import tcp
from log import *
from http_parse import *
from http_status import status


# used to:
#  1. read & parse request from client
#  2. send response to client
class request(object):
    def __init__(self, c):
        self.connection = c
        self.conf = c.conf

        # request info
        self.data = ''
        self.state = HTTP_REQUEST_LINE_START

        self.method = None
        self.uri = None
        self.version = None

        self.headers = {}

        self.content_length = -1    # request input body
        self.input_body = ''
        self.keepalive = False

        # response info
        self.wdata = ''
        self.error_status_code = 0

        # for debug
        self.debug_close = False

        self.header_handler = {'Content-Length':self.content_length_handler}

    def content_length_handler(self, k, v):
        try: n = int(v)
        except: n = -1

        if n < 0:
            self.error_status_code = 400
            return 'invalid value(%s) of content-length' %v

        self.content_length = n
        return None

    def read_parse(self):
        parsed = 0
        while True:
            # read data
            # if read slow, tengine/1.5.0 will close before all data is read
            err = self.read(4096 * 1024)
            if (err != 'again' and err) or (err == 'again' and not self.data):
                return err
            # err == None

            # Is there not enough data to parse?
            if parsed == len(self.data):
                return 'again'
            parsed = len(self.data)

            # parse data
            err = self.http_parse()
            if not err:
                status.request += 1
                return 'done'

            if err != 'again':
                log(LOG_ERROR, DEBUG_HTTP, '%s for http_parse()\n' %err)
                return err

            # err == 'again'
            log(LOG_NOISE, DEBUG_HTTP, 'parse next time...\n')

    def read(self, size=4096):
        log(LOG_NOISE, DEBUG_SOCK, 'try to read %d bytes data...\n' %size)
        data, err = tcp.read(self.connection.sock, size)
        if err == 'close':
            log(LOG_INFO, DEBUG_SOCK, 'client close its half of connection\n')
        elif err == 'again':
            log(LOG_NOISE, DEBUG_SOCK, 'read next time...\n')
        elif err:
            log(LOG_ERROR, DEBUG_SOCK, '%s for read()\n' %err)
        else:
            log(LOG_NOISE, DEBUG_SOCK, 'read %d bytes data\n' %len(data))
            log(LOG_NOISE, DEBUG_SOCK, 'read data: "%s"\n' %data)
            self.data += data
            status.read += 1
        return err

    def http_parse(self):

        # parse request line
        log(LOG_NOISE, DEBUG_HTTP, 'try to parse request line: "%s"\n' %self.data)

        if HTTP_REQUEST_LINE_START <= self.state <= HTTP_REQUEST_LINE_END_CF:
            state, pos, method, uri, version = \
                http_parse_request_line(self.state, (0, len(self.data), self.data))

            if state in http_invalid_state:
                return 'invalid request line. state:%d data:"%s"' \
                        %(state, self.data[0:pos])

            if state == self.state:
                assert pos == 0
                return 'again'

            self.data = self.data[pos:]
            self.state = state

            if not self.method and method: self.method = method
            if not self.uri and uri: self.uri = uri
            if not self.version and version: self.version = version

            if state != HTTP_HEADERS_START:
                return 'again'

        # continue to parse header
        if HTTP_HEADERS_START <= self.state < HTTP_HEADERS_END:
            assert self.state == HTTP_HEADER_KEY_START
            while True:
                log(LOG_NOISE, DEBUG_HTTP, 'try to parse header: "%s"\n' %self.data)
                parsed, pos, k, v = http_parse_one_header((0, len(self.data), self.data))
                if parsed == -1:
                    return 'invalid header line. data:"%s" key:%s value:%s' \
                           %(self.data[0:pos], k, v)
                if parsed == 0:
                    return 'again'

                self.data = self.data[pos:]
                if parsed == 2:
                    self.state = HTTP_HEADERS_END
                    break

                # parsed == 1
                if k in self.headers:
                    log(LOG_WARN, DEBUG_HTTP, 'duplicate header: "%s"\n' %k)

                self.headers[k] = v
                if k in self.header_handler:
                    err = self.header_handler[k](k, v)
                    if err: return err

            # detect keepalive
            Connection = self.headers.get('Connection')
            if Connection: Connection = Connection.lower()
            if self.version == 'HTTP/1.1':
                self.keepalive = (Connection != 'close')
            elif self.version == 'HTTP/1.0':
                self.keepalive = (Connection == 'keep-alive')

            # there is no input body in request
            if self.content_length <= 0:
                return None

        # FIXME: 2 content_length data will reset body
        # input body
        n = len(self.data)
        if n < self.content_length:
            return 'again'

        self.input_body = self.data[:self.content_length]
        self.data = self.data[self.content_length:]
        return None

    def dump_request(self):
        if self.conf.dump_request == 'off': return

        if self.conf.dump_request == 'only_url':
            log(LOG_INFO, DEBUG_HTTP, '%s %s %s\n' %(self.method, self.uri, self.version))
            return

        log(LOG_INFO, DEBUG_HTTP, '---- dump request ----\n')

        # request line
        log(LOG_INFO, DEBUG_HTTP, '%s %s %s\n' %(self.method, self.uri, self.version))
        if not self.input_body:
            log(LOG_INFO, DEBUG_HTTP, '\n')

        # header
        for k, v in self.headers.iteritems():
            log(LOG_INFO, DEBUG_HTTP, '%s: %s\n' %(k, v))

        # input body
        if self.input_body:
            if len(self.input_body) < 4096:
                log(LOG_INFO, DEBUG_HTTP, '\n')
                log(LOG_INFO, DEBUG_HTTP, '%s\n' %self.input_body)
            else:
                log(LOG_INFO, DEBUG_HTTP, '\n')
                log(LOG_INFO, DEBUG_HTTP, '>>> %d bytes input body\n' %len(self.input_body))

        log(LOG_INFO, DEBUG_HTTP, '----------------------\n')

    # NOTE:
    # add wevent, return, capture wevent
    # add wevent, write, return, capture wevent
    # add wevent, return, capture wevent, (not write), return, hangup!

    def chunk_create_response(self):
        Connection = 'Keep-Alive' if self.keepalive else 'Close'
        self.wdata = '%s 200 OK\r\n' \
                     'Connection: %s\r\n' \
                     'Transfer-Encoding: chunked\r\n' \
                     '\r\n' %(self.version, Connection)

    def chunk_add_body(self, body):
        self.wdata += '%x\r\n%s\r\n' %(len(body), body)

    def simple_send_response(self):
        return self.write()

    def write(self):
        if not self.wdata:
            log(LOG_WARN, DEBUG_SOCK, 'send empty data\n')
            return 'again'

        while self.wdata:
            n, err = tcp.write(self.connection.sock, self.wdata)

            if n == 0:
                # FIXME: re-add write event again
                log(LOG_NOISE, DEBUG_SOCK, 'send next time...\n')
                return 'again'

            if n == -1:
                log(LOG_ERROR, DEBUG_SOCK, '%s for write()\n' %err)
                return err

            # n > 0
            assert n <= len(self.wdata)
            self.wdata = self.wdata[n:]

            status.write += 1

        log(LOG_NOISE, DEBUG_SOCK, 'send complete response\n')
        status.response += 1
        return 'done'

    def simple_create_response(self, body=''):
        Connection = 'Keep-Alive' if self.keepalive else 'Close'
        self.wdata = '%s 200 OK\r\n' \
                     'Connection: %s\r\n' \
                     'Content-Length: %d\r\n' \
                     '\r\n' %(self.version, Connection, len(body))
        self.wdata += body

    # NOTE: request::close() can only be called by users outside
    def close(self, finish=False):

        if self.debug_close:
            print('Traceback (original close(from %s) stack):'
                  %(self.connection.peername,))
            traceback.print_list(self.debug_close_stack)
            raise Exception('double close request %s -> %s'
                            %(self.connection.peername,
                              self.connection.sockname))

        # try keepalive
        if finish and self.keepalive and self.conf.keepalive:
            self.do_keepalive()
            return

        self.connection.close()

        # TODO: self.connection = None
        self.debug_close = True
        self.debug_close_stack = traceback.extract_stack()

    def do_keepalive(self):
        # reinit request
        self.state = HTTP_REQUEST_LINE_START
        self.method = None
        self.uri = None
        self.version = None
        self.headers = {}
        self.content_length = -1    # request input body
        self.input_body = ''
        self.keepalive = 'k'        # notify that this request can be reused
        self.wdata = ''
        self.error_status_code = 0

        # be care: dont reinit connection here

    def __str__(self):
        return '<request %s>' %(self.connection.peername,)

    __repr__ = __str__

# used to:
#  1. send request to server
#  2. read & parse response from server
class client_request(request):

    def __init__(self, c):
        request.__init__(self, c)
        self.state = HTTP_STATUS_LINE_START
        self.rversion = None    # response version
        self.code = None        # response status code
        self.reason = None      # response reason phrase
        self.chunked = False
        self.body = ''

        self.header_handler['Transfer-Encoding'] = self.transfer_encoding_handler

    def transfer_encoding_handler(self, k, v):
        if v == 'chunked':
            self.chunked = True
        return None

    def http_parse(self):

        # parse status line
        log(LOG_NOISE, DEBUG_HTTP, 'try to parse status line: "%s"\n' %self.data)

        if HTTP_STATUS_LINE_START <= self.state <= HTTP_STATUS_LINE_END_CF:
            state, pos, version, code, reason = \
                http_parse_status_line(self.state, (0, len(self.data), self.data))

            if state in http_invalid_state:
                return 'invalid status line. state:%d data:"%s"' \
                        %(state, self.data[0:pos])

            if state == self.state:
                assert pos == 0
                return 'again'

            self.data = self.data[pos:]
            self.state = state

            if not self.rversion and version: self.rversion = version
            if not self.code and code: self.code = int(code)
            if self.reason == None and reason != None: self.reason = reason

            if state != HTTP_HEADERS_START:
                return 'again'

        # continue to parse header
        if HTTP_HEADERS_START <= self.state < HTTP_HEADERS_END:
            assert self.state == HTTP_HEADER_KEY_START
            while True:
                log(LOG_NOISE, DEBUG_HTTP, 'try to parse header: "%s"\n' %self.data)
                parsed, pos, k, v = http_parse_one_header((0, len(self.data), self.data))
                if parsed == -1:
                    return 'invalid header line. data:"%s" key:%s value:%s' \
                           %(self.data[0:pos], k, v)
                if parsed == 0:
                    return 'again'

                self.data = self.data[pos:]
                if parsed == 2:
                    self.state = HTTP_HEADERS_END
                    break

                # parsed == 1
                if k in self.headers:
                    log(LOG_WARN, DEBUG_HTTP, 'duplicate header: "%s"\n' %k)

                self.headers[k] = v
                if k in self.header_handler:
                    err = self.header_handler[k](k, v)
                    if err: return err

            # detect keepalive
            Connection = self.headers.get('Connection')
            if Connection: Connection = Connection.lower()
            if self.version == 'HTTP/1.1':
                self.keepalive = (Connection != 'close')
            elif self.version == 'HTTP/1.0':
                self.keepalive = (Connection == 'keep-alive')

            # detect message length
            self.http_calculate_length()

            if self.content_length == 0:
                return None

        # response entity
        if self.chunked:
            self.http_parse = self.http_parse_chunked_body
            return self.http_parse_chunked_body()

        if self.content_length == -1:
            if False:       # think it is error?
                return 'invalid response body(no content length)'

            log(LOG_WARN, DEBUG_HTTP, 'waiting for server to close the connection\n')

        self.http_parse = self.http_parse_body
        return self.http_parse_body()

    def http_parse_body(self):

        n = len(self.data)
        if n < self.content_length:
            return 'again'

        log(LOG_INFO, DEBUG_HTTP, 'response body is parsed, %d bytes\n' %self.content_length)

        self.body = self.data[:self.content_length]
        self.data = self.data[self.content_length:]
        if self.data:
            log(LOG_WARN, DEBUG_HTTP, 'response has extra data\n')
        return None

    def http_calculate_length(self):

        # RFC 2616: 4.4 Message Length
        if (100 <= self.code < 200 or self.code == 204 or self.code == 304 or
                self.method == 'HEAD'):
            self.content_length = 0
        elif self.chunked:
            self.content_length = -1
        elif self.content_length >= 0:
            pass
        elif False: # TODO: multipart/byteranges
            pass
        else:       # server closes the connection
            pass

    def http_parse_chunked_body(self):
        assert 0

    def create_request(self, method='GET', url='/', version='HTTP/1.0'):
        self.method = method
        self.uri = url
        self.version = version

        Host = 'TODO:Host'
        Connection = 'Close'

        self.wdata = '%s %s %s\r\n' \
                     'Host: %s\r\n' \
                     'Connection: %s\r\n' \
                     '\r\n' %(method, url, version, Host, Connection)

    def create_request_from_data(self, data):
        self.data = data
        self.state = HTTP_REQUEST_LINE_START
        err = request.http_parse(self)
        if err: return err

        parsed = len(data) - len(self.data)
        self.wdata = data[0:parsed]
        self.state = HTTP_STATUS_LINE_START
        self.headers = {}
        self.content_length = 0
        return None
