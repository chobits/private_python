
# default config for one virtual server
class server_config(object):
    def __init__(self):

        # server listen port
        self.ip = '127.0.0.1'
        self.port = 80

        # ssl support
        self.ssl = False
        self.ssl_certfile = 'ssl_cert/server.crt'
        self.ssl_keyfile = 'ssl_cert/server.key'
        # PROTOCOL_SSLv2 / PROTOCOL_SSLv3 / PORTOCOL_SSLv23 / PROTOCOL_TLSv1
        self.ssl_protocol = 'PROTOCOL_SSLv23'
        self.ssl_ciphers = None

        # 'off', 'only_url', 'request'
        self.dump_request = 'only_url'

        # use keepalive connection with client
        self.keepalive = True
        self.keepalive_timeout = 5

        # If set True, it accepts total connections and then handle them
        # (post read event to handle later).
        self.post_handle = True

        # current working directory
        self.cwd = '/Users/tomoyo/work/codes/python/projects/http_server'

        # path of user-defined application
        # It can be file path of app or directory path containing app.
        self.app = ['app/hello.py', 'app/test.py', 'app/status_code.py']

        # static pages
        self.static = []
        self.static.append(('raw', '/', 'file', './html/index.html'))
        self.static.append(('raw', '/favicon.ico', 'dir', './html/'))
        self.static.append(('raw', '/index.html', 'dir', './html/'))
        self.static.append(('raw', '/300x300.jpg', 'dir', './html/'))

        # timeout(s) to read data of request from client every time
        self.read_timeout = 5
        # timeout(s) to send data of response to client every time
        self.write_timeout = 5

# max number of established connections
connections = 1024

# log file path
# 'error.log'
log_file = 'stdout'
# 'ERROR'
log_level = 'ERROR'
# ['server', 'sock', 'http', 'conn', 'event', 'conf', 'dispatch']
log_type = ['server', 'event', 'http']


# http server
http = server_config()
http.port = 80
http.app.append('app/http_to_ssl.py')
http.static = []

# http server 2
http_8080 = server_config()
http_8080.port = 8080

# https server
https = server_config()
https.ssl = True
https.port = 443

# add your virtual server config
#server_configs = [http, http2, https]
server_configs = [http_8080]
