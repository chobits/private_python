# kqueue asynchronous event

import heapq
from time import time
from select import *
from log import *


_rw = { 'r':KQ_FILTER_READ, 'w':KQ_FILTER_WRITE }
_rwf = dict([(v, k) for k, v in _rw.iteritems()])

class event_core(object):
    def __init__(self):
        self.event_type = 'kqueue'
        self.timers = []
        self.events = {}
        self.post_event_handler = None
        self.changelist = []
        self.kq = kqueue()
        # NOTE: dont use EV_ONESHOT
        # EV_ONESHOT - Causes the event to return only the first occurrence of the filter being triggered.
        #              (events returned by kqueue doesnt have two same events )
        #            - After the user retrieves the event from the kqueue, it is deleted.
        #              (should readd listening event again after handling it, which has side effect of performance
        #               delete events from self.events after geting them from kqueue)
        self.kq_flags = KQ_EV_ADD|KQ_EV_CLEAR
        self.time = time()

    def post_event(self, handler):
        self.post_event_handler = handler

    def get_event(self, fd, rw):
        return self.events[(fd, rw)]

    def disable_event(self, e):
        assert (e.fd, e.rw) in self.events
        assert not e.disabled # TODO: delete this line
        if e.disabled:
            log(LOG_WARN, DEBUG_EVENT, '%s has been disabled already\n' %e)
            return

        log(LOG_INFO, DEBUG_EVENT, 'disable %s\n' %e)
        self.kq.control([kevent(e.fd, _rw[e.rw], KQ_EV_DISABLE)], 0, 0)
        e.disabled = True

    def enable_event(self, e):
        assert (e.fd, e.rw) in self.events
        if not e.disabled:
            log(LOG_WARN, DEBUG_EVENT, '%s has been enabled already\n' %e)
            return

        log(LOG_INFO, DEBUG_EVENT, 'enable %s\n' %e)
        self.kq.control([kevent(e.fd, _rw[e.rw], KQ_EV_ENABLE)], 0, 0)
        e.disabled = False

    def del_event(self, e, closed=False):
        assert (e.fd, e.rw) in self.events
        assert not e.closed

        log(LOG_INFO, DEBUG_EVENT, 'delete %s\n' %e)

        del self.events[(e.fd, e.rw)]

        if not closed:
            self.kq.control([kevent(e.fd, _rw[e.rw], KQ_EV_DELETE)], 0, 0)

        # avoid retry this event
        e.closed = True

    def add_event(self, e, again=False):
        assert isinstance(e.fd, int)
        assert again or (e.fd, e.rw) not in self.events
        assert again or e.disabled

        log(LOG_INFO, DEBUG_EVENT, 'add %s\n' %e)

        # event core saves this event
        self.events[(e.fd, e.rw)] = e

        # NOTE: create kevent (isnt a syscall)
        ke = kevent(e.fd, _rw[e.rw], self.kq_flags)  # default edge triggered

        # TODO: suspend to pass this event to kernel (decrease syscalls)
        # pass it to kernel right now
        self.kq.control([ke], 0, 0)         # events, len(events), timeout=0 for not polling now
        e.disabled = False

    def add_read_event(self, fd, handler, data=None):
        e = event(self, fd, handler, data, 'r')
        self.add_event(e)
        return e

    def add_write_event(self, fd, handler, data=None):
        e = event(self, fd, handler, data, 'w')
        self.add_event(e)
        return e

    def add_timer(self, e, handler, timeout):
        assert timeout >= 0
        if timeout == 0: return
        log(LOG_INFO, DEBUG_EVENT, 'add timer: %s %s %fms\n' %(e, handler.__name__, timeout))
        e.timeout = self.time + timeout
        e.timeout_handler = handler

        if e in self.timers:
            heapq.heapify(self.timers)
        else:
            heapq.heappush(self.timers, e)

    def del_timer(self, e):
        if e.timeout == None:
            assert e not in self.timers
            log(LOG_WARN, DEBUG_EVENT, 'delete unexisted timer %s\n' %e)
            return

        log(LOG_INFO, DEBUG_EVENT, 'delete timer: %s\n' %e)
        if e is self.timers[0]:
            heapq.heappop(self.timers)
        else:
            self.timers.remove(e)
            heapq.heapify(self.timers)
        e.timeout = None
        e.timeout_handler = None

    def wait(self):
        # NOTE:
        # 1. kqueue may return same events for one file descriptor without EV_ONESHOT
        # for example: read event from client 1, read event from client 2, read event from client 1.
        # the first event: <select.kevent ident=83 filter=-1 flags=0x21 fflags=0x0 data=0x2176 udata=0x0>
        # the third event: <select.kevent ident=83 filter=-1 flags=0x21 fflags=0x0 data=0x2304 udata=0x0>
        # data means the new water mark for received request data
        #
        # 2. duplicate accept event:
        # kevent: [<select.kevent ident=3 filter=-1 flags=0x21 fflags=0x0 data=0x2 udata=0x0>,
        #          <select.kevent ident=3 filter=-1 flags=0x21 fflags=0x0 data=0x2 udata=0x0>]
        # id: [4425570944, 4425570320]
        # data means accept number
        #
        # If it reads total data(specified by 1st and 3rd event) in 1st event, should take care of 3rd event.
        # Maybe after 1st event was handled, the connection is closed.

        log(LOG_INFO, DEBUG_EVENT, 'waiting kqueue events...\n')

        timeout = (self.timers[0].timeout - self.time) if self.timers else None    # None for polling forever
        kevents = self.kq.control(None, len(self.events), timeout)
        events = [self.events[(ke.ident, _rwf[ke.filter])] for ke in kevents]

        # update time
        t = time()
        delta = t - self.time
        self.time = t

        log(LOG_INFO, DEBUG_EVENT, 'wait %f second for kqueue events\n' %delta)

        # TODO: delete
        if 0:
            try:
                assert len(events) == len(set(events))
            except:
                import pprint
                pprint.pprint(self.events)
                pprint.pprint(kevents)
                pprint.pprint([id(k) for k in kevents])
                assert 0

        return events

    def loop(self):
        while True:
            events = self.wait()
            self.process_events(events)
            self.process_timers()

    def process_events(self, events):
        if not events: return

        log(LOG_INFO, DEBUG_EVENT,
            'kqueue get event[%d]:\n %s\n'
            % (len(events), '\n '.join([str(e) for e in events])))

        for e in events:
            # avoid handling stale event
            if e.closed:
                log(LOG_WARN, DEBUG_EVENT, "stale event: has deleted %s\n" %e)
            elif e.disabled:
                log(LOG_WARN, DEBUG_EVENT, "stale event: has disabled %s\n" %e)
            else:
                log(LOG_INFO, DEBUG_EVENT, 'handle %s\n' %e)
                e.handler(e)

                # handle posted event handler
                while self.post_event_handler:
                    handler = self.post_event_handler
                    self.post_event_handler = None
                    log(LOG_INFO, DEBUG_EVENT, 'post handle %s in %s\n' %(e, handler.__name__))
                    handler(e)

    def process_timers(self):
        log(LOG_INFO, DEBUG_EVENT, 'timers: %s\n' %self.timers)

        while self.timers:
            e = self.timers[0]
            expires = self.time - e.timeout
            if expires < 0: return

            timeout_handler = e.timeout_handler
            self.del_timer(e)

            log(LOG_INFO, DEBUG_EVENT, 'timer handle %s %s. expires %fs\n' %
                                       (e, timeout_handler.__name__, expires))
            timeout_handler(e)


class event(object):
    def __init__(self, ec, fd=None, handler=None, data=None, rw='r'):
        self.core = ec
        self.fd = fd
        self.handler = handler
        self.data = data
        self.rw = rw
        self.closed = False
        self.disabled = True

        self.timeout = None
        self.timeout_handler = None

    # print event
    def __str__(self):
        return '<%sevent at %#x fd: %d handler: %s>' \
               %(self.rw, id(self), self.fd, self.handler.__name__)

    # for sorting timers
    # return 0 only if self is other
    def __cmp__(self, other):
        if self.timeout == other.timeout:
            return cmp(id(self), id(other))
        else:
            return cmp(self.timeout, other.timeout)

    # print [event]
    __repr__ = __str__

