# example: http://docs.python.org/2/library/socket#example

import socket
import errno
import ssl


def tcp_server(ip, port, block=0):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((ip, port))
        s.listen(512)
        s.setblocking(block)        # non-blocking
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    except socket.error, err:
        if err.errno == errno.EADDRINUSE:
            return None, 'used'
        return None, str(err)
    except Exception, err:
        return None, 'Unknown Error'
    return s, None


def read(s, size=1024):
    try:
        data = s.recv(size)
        if not data: return None, 'close'
    except ssl.SSLError, err:       # support ssl socket
        # NOTE: SSLError inherits socket.error,
        #       it should capture SSLError before socket.error.
        if err.errno == ssl.SSL_ERROR_WANT_READ:
            return None, 'again'
        return None, str(err)
    except socket.error, err:
        if err.errno in [errno.EWOULDBLOCK, errno.EAGAIN]:
            return None, 'again'
        return None, str(err)
    except Exception, err:
        return None, 'unknown error for recv(): %s' %err

    return data, None


def write(s, data):
    try:
        n = s.send(data)
    except ssl.SSLError, err:       # support ssl socket
        if err.errno == ssl.SSL_ERROR_WANT_WRITE:
            return 0, 'again'
        return -1, str(err)
    except socket.error, err:
        # Is this check ok??
        # check EINTR?
        if err.errno in [errno.EWOULDBLOCK, errno.EAGAIN]:
            return 0, 'again'
        return -1, str(err)
    except Exception, err:
        return -1, 'unknown error for send()'
    return n, None


def accept(s, block=0):
    try:
        sock, addr = s.accept()
        sock.setblocking(block)         # non-blocking
    except socket.error, err:
        if err.errno in [errno.EWOULDBLOCK, errno.EAGAIN]:
            return None, None, 'again'
        # kernel has established tcp connection,
        # but fd table of process or system is full
        if err.errno == errno.EMFILE:   # Too many open files
            return None, None, 'nofile'
        if err.errno == errno.ENFILE:   # Too many open files in system
            return None, None, 'nofile'
        return None, None, str(err)
    except Exception, err:
        return None, None, 'unknown error for accept()'

    return sock, addr, None


def test_client(ip, port, block=0):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setblocking(block)        # non-blocking for IO
        s.connect((ip, port))       # blocked connect
    except socket.error, err:
        # TODO:
        #if err.errno == errno.EADDRINUSE:
        #    return None, 'used'
        return None, str(err)
    except Exception, err:
        return None, 'Unknown Error: %s' %err
    return s, None

