import conf
conf.log_level = 'ERROR'
conf.log_file = 'stdout'
conf.log_type = ['event']

from test_base import *
import sys

def client(ip, port):
    s = test_connect(ip, port)

    while True:
        test_send(s, 'ping')
        d = test_read(s, 4)
        tprint('read data: \n%s[EOF]\n' %d)

        test_sleep(1)

def server(ip, port):
    test_tcp_server(ip, port, accept_handler)

def accept_handler(e):
    s = e.data 
    while True:
        sock, addr, err = tcp.accept(s)
        if err == 'again': return
        if err: fatal('accept error: %s' %err)

        e.core.add_read_event(sock.fileno(), read_handler, (sock, None))

def read_handler(e):
    s, we = e.data
    d = test_read(s, 1024)
    tprint('read data: [%s]' %d)

    if not we:
        we = e.core.add_write_event(s.fileno(), write_handler, (s, "pong"))
        e.data = (s, we)
        return

    if False:
        # using write to trigger next write event
        tprint('write 1 byte ahead')
        ws = we.data[0]
        test_send(ws, "p")
        we.data = (ws, "ong")
    elif False:
        # using add_event to trigger next write event
        tprint('add write event again')
        we.data = (we.data[0], "pong")
        e.core.add_event(we, again=True)
    else:
        # this will hangup write event
        tprint('do nothing with write event')


def write_handler(e):
    s, data = e.data
    if not data:
        tprint('has write total data')
        tprint('write handler exit without calling sys_write()')
        tprint('In which case, the write event will not be reported again\n')
        return
    tprint('write handler get notification: %s' %data)
    test_send(s, data[0:1])
    data = data[1:]
    e.data = (s, data)

if __name__ == '__main__':
    ip, port = '127.0.0.1', 8080

    if len(sys.argv) == 3:
        port = int(sys.argv[2])

    if sys.argv[1] == 'server':
        server(ip, port)
    elif sys.argv[1] == 'client':
        client(ip, port)
    else:
        fatal('argument error: %s' %sys.argv[1])
