import sys
import tcp
import random
import socket       # TODO: delete this line
import traceback
import signal
import config

from log import *
from http import *
from random import randint
from url_dispatch import url_dispatch
from app import setup_app
from static_file_response import setup_static_file
from async_event_kqueue import event_core


def server_start():
    setup_log()
    server_setup_signal()

    server_check_config()
    ec = event_core()
    # setup every virtual server
    for conf in config.server_configs:
        server_setup_dispatch(conf)

        sock = server_setup_net(conf.ip, conf.port)
        if not sock: return

        ec.add_read_event(sock.fileno(), http_accept_handler, (sock, conf))

    ec.loop()

def server_check_config():
    for conf in config.server_configs:
        assert conf.keepalive_timeout >= 0
        assert conf.read_timeout >= 0
        assert conf.write_timeout >= 0

def signal_int_handler(signum, frame):
    print('Traceback (in interrupt(%d) signal handler):' %signum)
    traceback.print_stack(frame)
    log_close_file()
    sys.exit(1)

def server_setup_net(ip, port):
    # create socket
    s, err = tcp.tcp_server(ip, port)
    if err:
        log(LOG_ERROR, DEBUG_SERVER, '%s for tcp_server()\n' %err)
        return None
    log(LOG_NOTICE, DEBUG_SERVER, 'tcp create server %s:%s\n' %(ip, port))

    return s

def server_setup_signal():
    log(LOG_INFO, DEBUG_SERVER, 'install interrupt signal\n')
    signal.signal(signal.SIGINT, signal_int_handler)

def server_setup_dispatch(conf):
    conf.dispatch = url_dispatch()
    setup_static_file(conf)
    setup_app(conf)

def trace_server():
    import http, http_request, connection, async_event_kqueue
    __module__ = __import__(__name__)   # only python 3.0+ supports '__module__'
    import imp
    calltrace = imp.load_source('calltrace', '/Users/tomoyo/work/codes/python/projects/calltrace/calltrace.py')

    print('tracing server...')
    calltrace.trace_module(http)
    calltrace.trace_module(http_request)
    calltrace.trace_module(connection)
    calltrace.trace_module(async_event_kqueue)
    calltrace.trace_module(__module__)

    print('running traced server...')
    server_start()

if __name__ == '__main__':
    if len(sys.argv) == 2 and sys.argv[1] == 'trace':
        trace_server()
    else:
        server_start()
