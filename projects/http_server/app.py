import traceback
import sys
from log import *
from imp import load_source
from http import http_send_response
from http_error import http_process_error
from http_status import status
from http_status_code import http_status_code

# APIs used by application
__all__ = ['http_send_response', 'http_status_code', 'app_url', 'status']

def app_url(url, regex=False, hack=False):
    def wrap_wrap(f):
        # just add application, dont change f
        app_add(url, regex, hack, f)
        return f
    return wrap_wrap

def app_add(url, regex, hack, f):

    # prepare argument of f()
    argcount = f.func_code.co_argcount
    if argcount > 1:
        raise Exception('app function only has 0 or 1 argument')

    # call f()
    def app_handler_response(r):
        args = [r] if argcount == 1 else []

        try:
            ret = f(*args)
        except Exception, err:

            info = traceback.format_exc()
            log(LOG_ERROR, DEBUG_HTTP, 'user-written app is corrupt:\n' + info)

            r.error_status_code = 503
            http_process_error(r)
            return

        if hack: return

        if not isinstance(ret, basestring):
            log(LOG_ERROR, DEBUG_HTTP, 'user-written app generates invalid response body: %s\n' %ret)
            # TODO: error handling
            # r.error_status_code = 503
            # http_process_error(r)
            return

        r.simple_create_response(ret)
        http_send_response(r)

    # add app_handler_response to url dispatch system
    app_handler_response.__name__ += ':' + f.__name__
    app_dispatch.add(url, app_handler_response, regex)   # [*]


# Used by server
def setup_app(conf):

    # let [*] work well
    global app_dispatch
    app_dispatch = conf.dispatch

    for path in conf.app:
        if path[0] != '/': path = '%s/%s' %(conf.cwd, path)
        module_name = path.rsplit('/', 1)[-1].replace('.py', '')
	# import module sepcified by path
        module = load_source(module_name, path)
        app_modules.append(module)

app_dispatch = None
app_modules = []

