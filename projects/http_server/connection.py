import config
from log import *
from http_status import status

# TODO: add connection status log

def get_connection(sock):
    if connection.number >= config.connections:
        log(LOG_WARN, DEBUG_CONN, 'Cannot get connection, too many established connections\n')
        return None
    status.connect += 1
    c = connection(sock)
    return c


class connection(object):

    number = 0

    def __init__(self, sock):
        self.sock = sock
        self.request = None
        self.peername = None
        self.sockname = None
        # self.event = None  # current event
        self.revent = None
        self.wevent = None
        self.read_handler = None
        self.write_handler = None
        self.conf = None

        connection.number += 1
        log(LOG_INFO, DEBUG_CONN, 'has created connections: %d\n' %connection.number)

    def close(self):
        # NOTE for getpeername():
        # [EINVAL] socket has been shut down by peer.
        # So you should not use getpeername() here to get peer address.
        #
        # NOTE for send():
        # [EPIPE] The socket is shut down for writing or the socket is connection-mode and is no
        #         longer connected.  In the latter case, and if the socket is of type
        #         SOCK_STREAM, the SIGPIPE signal is generated to the calling thread.
        log(LOG_INFO, DEBUG_CONN, 'close connect %s -> %s\n' %(self.peername, self.sockname))
        self.sock.close()

        # Calling close() on a file descriptor will remove any kevents that
        # reference the descriptor.
        ec = self.revent.core
        if self.revent:
            ec.del_event(self.revent, closed=True)
            ec.del_timer(self.revent)

        if self.wevent:
            ec.del_event(self.wevent, closed=True)
            ec.del_timer(self.wevent)

        # TODO: self.request = None
        connection.number -= 1
        log(LOG_INFO, DEBUG_CONN, 'left connections: %d\n' %connection.number)

