import time

moon = '\xf0\x9f\x8c\x99'
suns = ['\xf0\x9f\x8c\x9e',
        '\xf0\x9f\x94\x85',
        '\xf0\x9f\x94\x86']

h = int(time.strftime('%H'))
m = int(time.strftime('%M'))

sun = suns[m % 3]

t = time.strftime('%A %m-%d %H:%M')
p = moon if (h < 6 or 18 < h < 24) else sun

print(p + '  ' + t)
