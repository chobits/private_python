
def xx(f):
    def wrap():
        return f()
    return wrap

@xx
def func():
    func_args(1,2,3,4)

def func_args(*args):
    func_kwargs(a=2, b=3, c=4)

def func_kwargs(**kwargs):
    pass
