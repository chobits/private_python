import inspect
import types

type_wrapper_descriptor = type(object.__str__)
type_method_wrapper = type(object().__str__)
debug = False
call_depth = 0

def trace_module(m, skip_import=True):
    for k in dir(m):
        try:
            v =  getattr(m, k)

            # skip object imported from other modules
            if skip_import and v.__module__ != m.__name__:
                continue
        except:
            continue

        if inspect.isfunction(v):
            trace_function(m, k, v)
        elif inspect.isclass(v):
            trace_class(v)

def trace_class(c):
    for k in dir(c):
        try:
            v =  getattr(c, k)
        except Exception:
            continue
        if inspect.ismethod(v) and not v.__name__.startswith('__'):
            trace_function(c, k, v)

def trace_function(o, k, f):

    if debug: print('trace: %s %s %s' %(o, k, f))

    def call_trace_func(*args, **kwargs):
        global call_depth

        # generate one call entry of call map
        make_call_graph(o, f, args, kwargs)

        # run orignally
        call_depth += 1
        retval = f(*args, **kwargs)
        call_depth -= 1

        return retval

    # avoid duplicated tracing
    if f.__name__ != call_trace_func.__name__:
        setattr(o, k, call_trace_func)

def make_call_graph(o, f, args, kwargs):
        print(trace_function_string(o, f, args, kwargs))

def trace_function_string(o, f, args, kwargs):

    str_trace = call_depth * '  '

    if inspect.isclass(o):
        str_trace += '%s::%s' %(o.__name__, f.__name__)
    else:
        str_trace += f.__name__

    str_trace += '('
    if args:
        str_trace += ', '.join([strarg(i)[0:64] for i in args])
    if kwargs:
        if args: str_trace += ', '
        str_trace += ', '.join(['%s=%s' %(k, strarg(v)[0:64]) for k, v in kwargs.iteritems()])
    str_trace += ')'

    return str_trace

def strarg(arg):
    if isinstance(arg, basestring):
        return '"%s"' %arg
    s = str(arg)
    if (not hasattr(arg, '__str__') or
            isinstance(arg.__str__, type_wrapper_descriptor) or
            isinstance(arg.__str__, type_method_wrapper)) and \
            (s[0] == '<' and s[-1] == '>'):
        try:
            return s[1:].split(' ', 1)[0]        # ??object
        except:
            return s.replace('object at ', '')   # <??object 0x????????>
    return s

if __name__ == '__main__':
    import test
    trace_module(test)
    test.func()

    import socket
    trace_module(socket)
    s = socket.socket()
    s.bind(('127.0.0.1', 7890))

