from socket import gethostbyname

REQ_NODATA = 0
REQ_STARTLINE = 1
REQ_PARSING_HEADER = 2
REQ_HEADERS = 3
REQ_ENTITYBODY = 4
REQ_COMPLETED = 5

# http request packet:
#   startline:       GET / HTTP/1.0\r\n
# [ headers:         Host: www.baidu.com\r\n
#                    Cookie: r=1\r\n
#                    \r\n
#   entitybody:      k=2\0                   ]

class http_request_parser:
    def __init__(self):
        self.state = REQ_NODATA

        self.data = ''
        self.headers = {}

        self.startline = ''     # end without '\r\n'
        self.istunnel = False
        self.iscompleted = False

        self.entitybody = ''

        self.errstr = ''

    def get_request(self):
        return self.data if self.state == REQ_COMPLETED else None

    def parse(self, data, off=0):

        parsed_size = 0
        data_size = len(data)

        while off < data_size:
            if self.state == REQ_NODATA:
                n = self.parse_startline(data, off)
            elif self.state in [REQ_STARTLINE, REQ_PARSING_HEADER]:
                n = self.parse_headers(data, off)
            elif self.state == REQ_HEADERS:
                n = self.parse_entitybody(data, off)
            elif self.state == REQ_COMPLETED:
                break

            if n == 0:      # not parsed
                break
            elif n < 0:     # error handling
                raise Exception(self.errstr)

            parsed_size += n
            off += n

        # give a chance to make state to REQ_COMPLETED
        if off == data_size and self.state == REQ_HEADERS:
            self.parse_entitybody(data, off)

        return parsed_size

    def parse_startline(self, data, off):
        # find flag
        startline_end = data.find('\r\n', off)
        if startline_end == -1:
            return 0

        # try parse
        startline = data[off:startline_end]
        try:
            method, url, version = startline.split(' ')
        except ValueError:
            self.errstr = 'Invalid startline format'
            return -1
        if method not in ['GET', 'PUT', 'POST', 'CONNECT', 'HEAD']:
            self.errstr = 'Unknown http request method: ' + method
            return -1
        if version not in ['HTTP/1.0', 'HTTP/1.1']:
            self.errstr = 'Invalid http version: ' + version
            return -1

        self.headers['method'] = method
        self.headers['url'] = url
        self.headers['version'] = version
        self.istunnel = (method == 'CONNECT')

        # completed
        self.startline = startline
        self.data = data[off:startline_end + 2]
        self.state = REQ_STARTLINE

        return startline_end + 2 - off


    def parse_headers(self, data, off):
        # no headers and no entity body
        if self.state == REQ_STARTLINE and data[off:off+2] == '\r\n':
            self.data += '\r\n'
            self.state = REQ_COMPLETED
            self.iscompleted = True
            return 2
        elif self.state == REQ_PARSING_HEADER and data[off:off+2] == '\r\n':
            self.data += '\r\n'
            self.state = REQ_HEADERS
            return 2

        # find flag
        header_end = data.find('\r\n', off)
        if header_end == -1:
            return 0

        header_line = data[off:header_end]
        try:
            key, value = header_line.split(':', 1)
        except:
            pass # ignore invalid http header
        else:
            self.headers[key.strip().lower()] = value.strip()
            self.data += data[off:header_end+2]
        self.state = REQ_PARSING_HEADER

        return header_end + 2 - off


    def parse_entitybody(self, data, off):
        if self.headers['method'] == 'POST':
            entitybody_size = int(self.headers['content-length'])
            # Only has whole entity body, it eats it.
            if (len(data) - off) < entitybody_size:
                return 0
            self.entitybody = data[off:off+entitybody_size]
            self.data += self.entitybody
            self.state = REQ_COMPLETED
            self.iscompleted = True
            return entitybody_size

        else:
            self.state = REQ_COMPLETED
            self.iscompleted = True
            return 0


    def get_server_address(self):
        def get_url_address(url):
            host = url
            port = 0
            if 'http://' == host[0:7]:
                host = host[7:]
                port = 80
            elif 'https://' == host[0:8]:
                host = host[8:]
                port = 443
            host = host.split('/')[0]
            if not host:
                return '', 0
            if ':' in host:
                host, port = host.split(':')
                port = int(port)
            return host, port

        def get_host_address(host):
            port = 0
            if ':' in host:
                host, port = host.split(':')
                port = int(port)
            return host, port

        if 'host' in self.headers:
            host, port = get_host_address(self.headers['host'])
        else:
            host, port = '', 0

        url_host, url_port = get_url_address(self.headers['url'])
        host = host if host else url_host
        port = port if port else (url_port if url_port else 80)

        ip = gethostbyname(host)
        return ip, port

    def display(self):
        if self.state != REQ_COMPLETED:
            return

        print 'request: ' + self.startline
        if self.headers['method'] == 'POST':
            print 'request: POST %s bytes' %self.headers['content-length']
        print

def test(data):
    mparser = http_request_parser()
    mparser.parse(data)

    for i in range(396):
        parser = http_request_parser()
        n = parser.parse(data[0:i])
        n2 = parser.parse(data[n:])
        assert n + n2 == 396
        if parser.headers != mparser.headers:
            print i
            print n
            print n2
            print parser.headers
            print
            print mparser.headers
            break
        assert parser.state == mparser.state
        assert parser.startline == mparser.startline


if __name__ == '__main__':
    data = """GET http://mail.njupt.edu.cn/ HTTP/1.1
Host: mail.njupt.edu.cn
Connection: keep-alive
User-Agent: Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Encoding: gzip,deflate,sdch
Accept-Language: en-US,en;q=0.8
Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3

<<<<<<<<<<<<<<<<<<<<< unused >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
"""
    data = data.replace('\n', '\r\n')

    test(data)    


