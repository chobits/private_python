import asyncore
import socket
import threading
import Queue
import errno
import sys
from http_parser import http_request_parser


class Session:
    def __init__(self, reqsock):
        self.request = Request(self, reqsock)
        self.response = None
        self.destip = ''
        self.destport = 0
        self.connected = False

    def connect_server(self, ip, port):
        if not self.response and not self.connected:
            self.destip = ip
            self.destport = port
            self.response = Response(self)
            self.connected = True


class Response(asyncore.dispatcher_with_send):
    def __init__(self, session):
        asyncore.dispatcher_with_send.__init__(self)

        self.session = session
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect((session.destip, session.destport))
        self.addr = (session.destip, session.destport)
        self.clear_response()

        print 'proxy: connect server(%s:%d)' %(session.destip, session.destport)

    def handle_connect(self):
        self.handle_read()

    def handle_read(self):
        try:
            data = self.recv(1024)
            if not data: return # dispatcher has called handle_close()
            self.data += data
        except Exception, err:
            if err.args[0] not in [errno.EWOULDBLOCK, errno.EAGAIN]:
                raise err
            return
        self.send_response()
        self.clear_response()

    def send_response(self):
        # send it
        if self.session.request and len(self.data) > self.data_send:
            self.session.request.send_delay(self.data[self.data_send:])
            self.data_send = len(self.data)

    def clear_response(self):
        self.data = ''
        self.data_send = 0

    def handle_close(self):
        notify_request_close = self.session.request.notify_close if self.session.request else None

        print 'proxy: server(%s) close the connection' %repr(self.addr)
        # if still have response, send it
        self.send_response()
        self.clear_response()
        self.close()
        self.session.response = None

        # cannot close client at right, client will receive this response
        if notify_request_close:
            notify_request_close()

    def notify_close(self):
        self.clear_response()
        self.close()
        self.session.response = None

    def send_delay(self, data):
        self.out_buffer += data


class Request(asyncore.dispatcher_with_send):
    def __init__(self, session, sock):
        asyncore.dispatcher_with_send.__init__(self, sock)
        self.session = session
        self.clear_request()
        self.times = 0
        self.closed = False
        self.send_data_end = 0

    def notify_close(self):
        if self.parser.iscompleted:
            self.clear_request()
            self.closed = True
            self.send_error_response()
        else:
            self.handle_close()

    def send_error_response(self):
        self.send_delay('HTTP/1.1 503 Service Unavailable\r\nInfo: remote server close connection\r\n\r\n')

    def send_ok_response(self):
        self.send_delay('HTTP/1.1 200 Connection established\r\n\r\n')

    def handle_close(self):
        print 'proxy: client(%s) close the connection, has received %d requests' %(repr(self.addr), self.times)
        if self.session.response:
            self.session.response.notify_close()
        self.close()
        self.session.request = None

    def send_request(self):
        if self.session.connected and self.session.response:
            try:
                self.parser.display()
                request_data = self.parser.get_request()
                request_data = request_data.replace('\r\nProxy-Connection:', '\r\nConnection:')
                self.session.response.send(request_data)
            except socket.error, e:
                # cannot send to server, maybe server close the connection or network down
                self.send_error_response()
                return

    def next_request(self):
        self.parser = http_request_parser()
        self.times += 1

    def clear_request(self):
        self.data = ''
        self.parser = http_request_parser()

    def handle_direct_connect(self):
        if not self.parser.iscompleted:
            return

        if not self.session.connected:
            self.connect_server()

        if self.session.connected and self.session.response:
            try:
                self.session.response.send_delay(self.data)
                self.data = ''
            except socket.error, e:
                # cannot send to server, maybe server close the connection or network down
                print 'proxy: cannot send msg via http tunnel'
                self.send_error_response()
                return

    def read_data(self):
        # read data
        try:
            data = self.recv(1024)
            # local host client close the connection
            if not data: return False # dispatcher has called handle_close()
            self.data += data
            return True
        except Exception, err:
            if err.args[0] not in [errno.EWOULDBLOCK, errno.EAGAIN]:
                raise err
            return False

    def handle_read(self):
        if not self.read_data():
            return

        if self.closed:
            self.notify_close()

        # parse http request
        n = self.parser.parse(self.data)
        if n > 0:
            self.data = self.data[n:]
        elif not self.parser.istunnel:
            return

        # handle CONNECT
        if self.parser.istunnel:
            self.handle_direct_connect()
            return

        if self.parser.iscompleted and not self.closed:
            self.connect_server()
            self.send_request()
            self.next_request()
            return

    # NOTE: change orignal initiate_send() logic
    def initiate_send(self):
        asyncore.dispatcher_with_send.initiate_send(self)
        if not self.out_buffer and self.closed:
            self.handle_close()

    def send_delay(self, data):
        self.out_buffer += data

    def connect_server(self):
        if not self.parser.iscompleted or self.session.connected:
            return
        ip, port = self.parser.get_server_address()
        # real connect
        self.session.connect_server(ip, port)
        if self.parser.istunnel:
            self.send_ok_response()

class server(asyncore.dispatcher):
    def __init__(self, host, port):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(5)

    def handle_accept(self):
        try:
            accept_sock, accept_addr = self.accept()
        except:
            self.accept_error_times += 1
            return
        if accept_sock:
            Session(accept_sock)

    def handle_close(self):
        self.close()

if __name__ == '__main__':
    server('127.0.0.1', 8080)
    while True:
        try:
            asyncore.loop()
        except Exception, err:
            print 'proxy: ' + err


