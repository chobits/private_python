#include <python2.6/Python.h>

int main(int argc, char **argv)
{
	Py_SetProgramName(argv[0]);
	Py_Initialize();
	PySys_SetArgv(argc, argv);
	PyRun_SimpleString("import sys\n");
	PyRun_SimpleString("print sys.argv\n");
	Py_Exit(0);
}
