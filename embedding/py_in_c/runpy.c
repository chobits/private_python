#include <python2.6/Python.h>

int main(int argc, char **argv)
{
	if (argc < 3)
		errx(-1, "Usage: %s module func [func_args]", argv[0]);

	PyObject *module, *function;
	int i;

	/* init python intepreter */
	Py_SetProgramName(argv[0]);
	Py_Initialize();

	/* add current path */
	PySys_SetArgv(argc, argv);	/* will set sys.path[0] to '' */
//	PyRun_SimpleString("import sys\n");
//	PyRun_SimpleString("sys.path.append('')\n");

	/* load module(.py) */
	module = PyImport_ImportModule(argv[1]);
	if (!module) {
		PyErr_Print();
		return 1;
	}
	/* load module.function */
	function = PyObject_GetAttrString(module, argv[2]);
	if (!function) {
		Py_DECREF(module);
		PyErr_Print();
		return 1;
	}
	/* call function */
	PyObject *args, *value;
	/* load function arguments */
	args = PyTuple_New(argc - 3);
	for (i = 0; i < argc - 3; i++) {
		value = PyInt_FromString(argv[3 + i], NULL, 0);
		if (!value) {
			Py_DECREF(args);
			Py_DECREF(module);
			PyErr_Print();
			return 1;
		}
		PyTuple_SetItem(args, i, value);
	}
	/* call function(args...) */
	value = PyObject_CallObject(function, args);
	if (value) {
		printf("function returns %d\n", PyInt_AsLong(value));
		Py_DECREF(value);
	} else {
		Py_DECREF(args);
		Py_DECREF(function);
		Py_DECREF(module);
		PyErr_Print();
		return 1;
	}

	Py_Finalize();
	return 0;
}
