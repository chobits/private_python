from distutils.core import setup, Extension

demo_module = Extension('demo',
                   sources = ['demomodule.c'])

setup(name='PackageName',
      version='1,0',
      description='This is demo package',
      ext_modules=[demo_module])
