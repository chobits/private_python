#include <Python.h>

static PyObject *demo_error;

static PyObject *demo_system(PyObject *self, PyObject *args)
{
	const char *command;
	int status;
	/* Parse will handle the error */
	if (!PyArg_ParseTuple(args, "s", &command))
		return NULL;
	status = system(command);
	if (status < 0) {
		PyErr_SetString(demo_error, """System command failed");
		return NULL;
	}
	return Py_BuildValue("i", status);
}

PyDoc_STRVAR(system_doc,
"system() -> (args...)\n\
\n\
Run system command.");

static PyMethodDef demo_methods[] = {
	{"system",	(PyCFunction)demo_system,	METH_VARARGS,
			system_doc},
	{NULL,		NULL}		/* sentinel */
};

/*
 * Function must be initdemo().
 * init_demo() is error, python cannot import it
 */
PyMODINIT_FUNC initdemo(void)
{
	PyObject *m;
	m = Py_InitModule("demo", demo_methods);
	if (m == NULL)
		return;

	demo_error = PyErr_NewException("demo.error", NULL, NULL);
	if (demo_error == NULL)
		return;
	Py_INCREF(demo_error);
	PyModule_AddObject(m, "error", demo_error);
}
